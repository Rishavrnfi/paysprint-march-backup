import { Component, Input, OnInit } from '@angular/core';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {
 // @Input() _permissionlist: any;
  list: any;
  public details: any;
  constructor() {
    const encode:any = EncodeDecode('n',localStorage.getItem('LoginDetails'));
    const _permissionlist:any = JSON.parse(encode);
    this.list = _permissionlist;  
    this.details = {
      allowfundrequest:  _permissionlist.allowfundrequest, 
      funding:_permissionlist.permission.funding,
      is_admin:_permissionlist.permission.is_admin,
      is_approver : _permissionlist.permission.is_approver
    } 
   }

  ngOnInit(): void { 

  }
}
