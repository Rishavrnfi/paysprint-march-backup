import { Component, OnInit } from '@angular/core';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';

@Component({
  selector: 'app-common-sidebar',
  templateUrl: './common-sidebar.component.html',
  styleUrls: ['./common-sidebar.component.css']
})
export class CommonSidebarComponent implements OnInit {
  public details: any;
  constructor() { 
    const encode:any = EncodeDecode('n',localStorage.getItem('LoginDetails'));
    const _permissionlist:any = JSON.parse(encode); 
    ////console.log(_permissionlist);
    
    this.details = {
      allowfundrequest:  _permissionlist.allowfundrequest, 
      firmname:_permissionlist.firmname,
      name : _permissionlist.name,
      show_usertype:_permissionlist.show_usertype
    }
  }

  ngOnInit(): void {
  }

}
