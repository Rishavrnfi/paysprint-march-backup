import { Component, OnInit } from '@angular/core';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';
@Component({
  selector: 'app-super-admin',
  templateUrl: './super-admin.component.html',
  styleUrls: ['./super-admin.component.css']
})
export class SuperAdminComponent implements OnInit {
  version: string = require( '../../../../package.json').version;
  list: any;
  public details: any;
  constructor() { 
    const encode:any = EncodeDecode('n',localStorage.getItem('LoginDetails'));
    const _permissionlist:any = JSON.parse(encode);
    this.list = _permissionlist['permission'];
    ////console.log(this.list)
    this.details = {
      permissionmanager:  _permissionlist['permission'].permissionmanager,
      controlmanager: _permissionlist['permission'].controlmanager,
      is_superadmin:_permissionlist['permission'].is_superadmin,
      usermanager:_permissionlist['permission'].usermanager,
      is_approver : _permissionlist['permission'].is_approver
    }
  }
  ngOnInit(): void {
  }
}
