import { Component, OnInit } from '@angular/core';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';

@Component({
  selector: 'app-retailer-header',
  templateUrl: './retailer-header.component.html',
  styleUrls: ['./retailer-header.component.css']
})
export class RetailerHeaderComponent implements OnInit {
  list: any;
  public details: any;
  constructor() { 
    const encode:any = EncodeDecode('n',localStorage.getItem('LoginDetails'));
    const _permissionlist:any = JSON.parse(encode);
    this.list = _permissionlist; 
    
    this.details = {
      allowfundrequest:  _permissionlist.allowfundrequest, 
      name:_permissionlist.firmname,
      aadharpay:_permissionlist.permission.aadharpay, 
      aeps: _permissionlist.permission.aeps,
      bill: _permissionlist.permission.bill,
      cash_deposit: _permissionlist.permission.cash_deposit,
      dmt: _permissionlist.permission.dmt,
      matm: _permissionlist.permission.matm,
      pg: _permissionlist.permission.pg,
      recharge: _permissionlist.permission.recharge,
      settlement: _permissionlist.permission.settlement,
      wallet:_permissionlist.permission.wallet
    } 
     
  } 

  ngOnInit(): void { 
  }

}
