import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-get-prefrence',
    templateUrl: './get-prefrence.component.html',
    styleUrls: ['./get-prefrence.component.css']
})
export class GetPrefrenceComponent implements OnInit {
    prefrence: any;
    form: FormArray = this.fb.array([]);
    mainDt: any;
    comHeading: any;
    viewType: any;
    parkCom: any;
    dmtSlabCom: any;
    bill_sms_bCom: any = [];
    viewComOrVal: any;
    orpList: any;
    rechCom: any = [];
    constructor(
        private fb: FormBuilder,
        private _auth: ApiService,
        private modal: CustomeModalService
    ) { }

    ngOnInit(): void {
        this.get_Prefrence();
    }
    get formCon() {
        return this.form as FormArray;
    }
    getArr(arr: any, i: any): any {
        return (<FormArray>arr.controls['arr'])
    }
    getFormArr(innerFormArr: any, i: any): any {

        return (<FormArray>innerFormArr.controls['innerArDt'])
    }

    sub(val: FormGroup) {
        const currentVal = val.value;
        let newVal: any;
        setTimeout(() => {
            newVal = val.value
        }, 10);
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            icon: 'warning',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            denyButtonText: `Don't save`,
            confirmButtonText: 'Yes, Submit it!',
            allowOutsideClick: false,
            allowEscapeKey: false,

        }).then((result) => {
            if (result.isConfirmed) {
                const formdata = new FormData();
                formdata.append('token', config.tokenauth);
                formdata.append('key', newVal.key);
                formdata.append('value', newVal.key_current_value);
                this._auth.postdata(formdata, config.updateprefrence).subscribe((res: any) => {
                    if (res.statuscode == 200) {
                        Swal.fire({
                            icon: 'success',
                            title: res.message
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: res.message
                        })
                    }
                })
            } else if (result.isDenied) {
                val.patchValue({
                    'key_current_value': currentVal.key_current_value
                })
            }
        })
    }
    get_Prefrence() {
        const formdata = new FormData();
        formdata.append('token', config.tokenauth);
        this._auth.postdata(formdata, config.getprefrence).subscribe((res: any) => {
            this.prefrence = res.data;
            let main: any[] = [];
            for (const key1 in this.prefrence) {
                const formType = this.prefrence[key1];

                if (key1 == 'dmt') {
                    let innerFormArr = this.fb.array([]);
                    // const formType = this.prefrence[key1];
                    let formDtAr: any[] = []
                    let step2: any = {
                        name: key1,
                        data: formDtAr
                    }
                    for (const formTypeKey in formType) {
                        let innerFormData = this.fb.array([]);
                        let formTypeDtAr: any[] = []
                        let step3: any = {
                            name: formTypeKey,
                            data: formTypeDtAr
                        }
                        const eleAc = formType[formTypeKey];
                        for (const eleAcKey in eleAc) {
                            const eleAcData: any = eleAc[eleAcKey];
                            const eleAcDataVal: any = eleAcData.value;

                            let group: FormGroup = this.fb.group({
                                head_key: this.fb.control(key1),
                                title_key: this.fb.control(formTypeKey),
                                d_key: this.fb.control(eleAcData.d_key),
                                key_current_value: this.fb.control(eleAcData.current_value),
                                key: this.fb.control(eleAcData.key),
                            })

                            for (const valKy in eleAcDataVal) {
                                const eleVal = eleAcDataVal[valKy];
                                group.addControl('optName' + (valKy), this.fb.control(eleVal));
                                group.addControl('optValue' + (valKy), this.fb.control(valKy));
                                // group.addControl(valKy, new FormControl(eleVal));
                            }
                            formTypeDtAr.push(eleAcData);
                            innerFormData.push(group)
                        }
                        formDtAr.push(step3)
                        innerFormArr.push(this.fb.group({
                            title: this.fb.control(formTypeKey),
                            innerArDt: innerFormData
                        }))
                    }
                    main.push(step2)
                    this.form.push(this.fb.group({
                        heading: this.fb.control(key1),
                        arr: innerFormArr
                    }));
                }


                {
                    if (key1 != 'dmt') {
                        let current_val = formType.current_value;
                        let innerFormArr = this.fb.array([]);
                        for (const formTypeKey in formType) {
                            if (formTypeKey != 'operator' && formTypeKey != 'current_value') {

                                let innerFormData = this.fb.array([]);
                                const eleAc = formType[formTypeKey];
                                let group: FormGroup = this.fb.group({
                                    head_key: this.fb.control(key1),
                                    title_key: this.fb.control(formTypeKey),
                                    selected_key: this.fb.control(formTypeKey),
                                    key: this.fb.control(key1),
                                    // currentVal: this.fb.control(current_val),
                                    key_current_value: this.fb.control(current_val),
                                })
                                let count = 0
                                for (const eleAcKey in eleAc) {

                                    const eleAcData: any = eleAc[eleAcKey];
                                    const eleAcDataVal: any = eleAcData.value;


                                    // for (const valKy in eleAcDataVal) {

                                    // const eleVal = eleAcDataVal[valKy];
                                    group.addControl('optName' + (count), this.fb.control(eleAcData.d_key));
                                    group.addControl('optValue' + (count), this.fb.control(eleAcData.value));
                                    count++;
                                    // }


                                }
                                innerFormData.push(group)

                                innerFormArr.push(this.fb.group({
                                    title: this.fb.control(formTypeKey),
                                    innerArDt: innerFormData
                                }))

                            }

                        }
                        this.form.push(this.fb.group({
                            heading: this.fb.control(key1),
                            arr: innerFormArr
                        }));
                    }
                }

                {
                    if (key1 != 'dmt') {
                        const formType = this.prefrence[key1];
                        let formDtAr: any[] = []
                        let step2: any = {
                            name: key1,
                            data: formDtAr
                        }
                        for (const formTypeKey in formType) {
                            if (formTypeKey != 'current_value' && formTypeKey != 'operator') {
                                let formTypeDtAr: any[] = []
                                let step3: any = {
                                    name: formTypeKey,
                                    data: formTypeDtAr
                                }
                                const eleAc = formType[formTypeKey];
                                for (const eleAcKey in eleAc) {
                                    const eleAcData: any = eleAc[eleAcKey];
                                    formTypeDtAr.push(eleAcData);
                                }
                                formDtAr.push(step3)
                            }
                            if (formTypeKey == 'operator') {
                                const eleAc = formType[formTypeKey];
                                this.orpList = eleAc

                            }
                        }
                        main.push(step2)
                    }

                }




            }
            this.mainDt = main;
        })

    }
    showComm(data: any, head: any, subhead: any, itel?: any) {
        this.viewType = null;
        let modalSize = '';

        if (itel == undefined) {
            this.comHeading = this.mainDt[head].name;
            modalSize = 'xl';
            if (this.comHeading == 'recharge') {
                this.viewType = 4;
                this.mainDt[head].data[subhead].data.map((m: any) => {
                })
                this.rechCom = this.mainDt[head].data[subhead].data
                // this.parkCom = this.mainDt[head].data[subhead].data[itel].comm;
            } else {
                this.viewComOrVal = this.comHeading == 'bill' ? 'Value :' : ''
                this.viewType = 3;
                modalSize = this.comHeading == 'sms' ? 'lg' : 'md';
                this.bill_sms_bCom = this.mainDt[head].data[subhead].data;

            }
        } else {
            this.comHeading = this.mainDt[head].data[subhead].data[itel].d_key;
            if (this.comHeading == 'Park of Transaction') {
                this.viewType = 1;
                modalSize = 'sm';
                this.parkCom = this.mainDt[head].data[subhead].data[itel].comm;
            } else {
                this.viewType = 2;
                modalSize = 'md';
                this.dmtSlabCom = this.mainDt[head].data[subhead].data[itel].comm;

            }
        }
        this.modal.open(data, modalSize);
    }
    getOprName(id: any) {
        let retVal = '';
        this.orpList.filter((dt: any) => {
            if (dt.id == id) {
                retVal = dt.name;
            }
        })

        return retVal;
    }
    checkType(val: any) {
        //console.log(val);
        //console.log(typeof val);
        return typeof val;
    }
}