import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetPrefrenceComponent } from './get-prefrence.component';

describe('GetPrefrenceComponent', () => {
  let component: GetPrefrenceComponent;
  let fixture: ComponentFixture<GetPrefrenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetPrefrenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetPrefrenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
