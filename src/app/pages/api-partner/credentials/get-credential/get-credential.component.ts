import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';

@Component({
  selector: 'app-get-credential',
  templateUrl: './get-credential.component.html',
  styleUrls: ['./get-credential.component.css']
})
export class GetCredentialComponent implements OnInit {
  credentialData : any=[];
  constructor(private _auth: ApiService) { }


  ngOnInit(): void {
    this.getUsercredentials();
  }

  getUsercredentials(){ 
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      this._auth.postdata(formdata, config.getusercredential).subscribe((res: any) => {
       //this.credentialData =  res.data; 
        let udData = res.data;
        for (const key in udData) {
          const element = udData[key];
          //console.log(key);
          let cust = {
            'name': key,
            'value': element
          }
          this.credentialData.push(cust)
        } 
        
      })
    } 
}
