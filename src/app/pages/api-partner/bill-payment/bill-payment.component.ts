import { getLocaleDateTimeFormat } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-bill-payment',
  templateUrl: './bill-payment.component.html',
  styleUrls: ['./bill-payment.component.css']
})
export class BillPaymentComponent implements OnInit, AfterViewInit {
  calLst: any;
  data: any = [];
  filterData: any;
  oprObj: any = null;
  oprNg = '';
  disValueNg = '';
  objData = {
    "category": null,
    "displayname": null,
    "id": null,
    "name": null,
    "regex": null,
    "viewbill": null,
  };
  viewFetchBillDtl: boolean = false;
  viewBillDtl: any;
  longitute: any;
  latitute: any;
  now: any;

  constructor(private fb: FormBuilder, private _auth: ApiService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getLocation()
    let formdata: any = new FormData();
    // formdata.append('token', config.tokenauth);
    // formdata.append('Authkey', config.Authkey);
    this._auth.postdata(formdata, config.getBillOperator).subscribe((res: any) => {
      //console.log(res.data);
      this.data = res.data
      //console.log('dd');

      // Declare a new array 
      let newArray: any = [];

      // Declare an empty object 
      let uniqueObject: any = {};

      // Loop for the array elements 
      for (let i in this.data) {

        // Extract the title 
        let objTitle = this.data[i]['category'];

        // Use the title as the index 
        uniqueObject[objTitle] = this.data[i];
      }
      // //console.log(uniqueObject);

      // Loop to push unique object into array 
      for (let j in uniqueObject) {
        newArray.push({ 'catName': uniqueObject[j]['category'] });
      }
      //console.log(newArray);
      this.calLst = newArray;
    });
    setInterval(() => {
      this.now = Date.now();

    }, 1);

  }

  ngAfterViewInit() {
    // this.message = 'all done loading :)'
    this.cdr.detectChanges();
  }
  getCatData(catName: any) {
    this.oprNg = '';
    this.objData.displayname = null;
    this.objData.viewbill = null;
    this.disValueNg = '';
    this.viewFetchBillDtl = false;
    this.viewBillDtl = null;
    this.objData.category = catName;
    //console.log(catName);
    this.filterData = this.data.filter((person: any) => {
      return person.name === "Tata Power Delhi Distribution Ltd" || person.name === "BSES Rajdhani Power Limited"
      // return person.category === catName
    });
    // //console.log(this.filterData);
  }
  oprDtl(opr: any) {
    this.disValueNg = '';
    this.viewFetchBillDtl = false;
    this.viewBillDtl = null;
    //console.log(this.filterData[opr]);
    this.objData.displayname = this.filterData[opr].displayname;
    this.objData.viewbill = this.filterData[opr].viewbill;
  }
  payBill(oprIndex: any, disValue: any) {
    //console.log(oprIndex);
    //console.log(disValue);
    const oprDtl = this.getMainData(oprIndex);
    let formdata: any = new FormData();
    formdata.append('operator', oprDtl.id);
    formdata.append('canumber', disValue);
    formdata.append('amount', '');
    formdata.append('referenceid', '');
    formdata.append('latitude', '');
    formdata.append('longitude', '');
    formdata.append('bill_fetch', '');
    this._auth.postdata(formdata, config.paybillOperator).subscribe((res: any) => {
      if (res.statuscode == 200) {
        // Swal.fire(res.message);
        Swal.fire(res.message).then((result) => {
          if (result.isConfirmed) {
            //console.log(res.data);
            this.data = res.data
          }
        })
      }
      if (res.response_code == 13) {
        Swal.fire(res.message);
        // Swal.fire(res.message).then((result) => {
        //   // if (result.isConfirmed) {
        //   //   this.goBakc();
        //   // }
        // })
      }
      //console.log(res.data);
      this.data = res.data
    })
  }
  getLocation() {
    this._auth.getLocationService().then(resp => {
      this.longitute = resp.lng;
      this.latitute = resp.lat;

    })
  }
  payBills(oprIndex: any, disValue: any) {
    //console.log(oprIndex);
    //console.log(disValue);
    const oprDtl = this.getMainData(oprIndex);
    let formdata: any = new FormData();
    formdata.append('operator', oprDtl.id);
    formdata.append('canumber', disValue);
    formdata.append('amount', this.viewBillDtl.amount);
    formdata.append('referenceid', this.now);
    formdata.append('latitude', this.latitute);
    formdata.append('longitude', this.longitute);
    formdata.append('bill_fetch', JSON.stringify(this.viewBillDtl.bill_fetch));
    this._auth.postdata(formdata, config.paybillOperator).subscribe((res: any) => {
      if (res.statuscode == 200) {
        // Swal.fire(res.message);
        Swal.fire(res.message).then((result) => {
          if (result.isConfirmed) {
            //console.log(res.data);
            this.data = res.data
          }
        })
      }
      if (res.response_code == 2 || res.response_code == 3 || res.response_code == 13 || res.response_code == 16 || res.response_code == 18) {
        Swal.fire(res.message);

      }
      //console.log(res.data);
      this.data = res.data
    })
  }
  viewBill(oprIndex: any, disValue: any) {
    //console.log(oprIndex);
    //console.log(disValue);
    const oprDtl = this.getMainData(oprIndex);
    let formdata: any = new FormData();
    formdata.append('operator', oprDtl.id);
    formdata.append('canumber', disValue);
    this._auth.postdata(formdata, config.fetchbillOperator).subscribe((res: any) => {
      if (res.response_code == 1) {
        // Swal.fire(res.message);
        Swal.fire(res.message).then((result) => {
          if (result.isConfirmed) {
            // this.data = res.data
            this.viewBillDtl = res;
            //console.log(this.viewBillDtl.bill_fetch);

            this.viewFetchBillDtl = true;
          }
        })
      }
      if (res.response_code == 2 || res.response_code == 3 || res.response_code == 13 || res.response_code == 16 || res.response_code == 18) {
        Swal.fire(res.message);

      }

    })
  }
  getMainData(i: number) {
    return this.filterData[i];
  }
}

