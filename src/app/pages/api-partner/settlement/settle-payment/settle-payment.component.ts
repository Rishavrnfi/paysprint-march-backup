import { Component, Directive, HostListener, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { LoaderService } from 'src/app/_helpers/common/loader.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-settle-payment',
  templateUrl: './settle-payment.component.html',
  styleUrls: ['./settle-payment.component.css']
})
export class SettlePaymentComponent implements OnInit { 
  @ViewChild('amount') amount: any;

  addedBankList: any = [];
  radioSelected: boolean = false;
  radioVal: any = '';
  unicode: any;
  constructor(private _auth: ApiService, private loader: LoaderService) { }

  ngOnInit(): void {
    this.listAddedBank()

  }

  listAddedBank() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getAddedBankLst).subscribe((res: any) => {
      if (res.statuscode == 1) {
        this.addedBankList = res.bankdetails;
        //console.log(res.bankdetails);
        this.unicode = res.unicode;
      } else {
      }
    });
  }

  onSubmit(amount: any, id: any) { 
    Swal.fire({
      title: 'Are you sure?', 
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {

      if (result.isConfirmed) {
        const formdata = new FormData();
        formdata.append('token', config.tokenauth);
        formdata.append('id', id);
        formdata.append('amount', amount);
        formdata.append('unicode', this.unicode);
        this._auth.postdata(formdata, config.transact).subscribe((res: any) => {
          if (res.statuscode == 2001 || res.statuscode === 1) {
            Swal.fire(res.message);
            this.amount.nativeElement.value = null;
            this.radioVal = null;
            this.listAddedBank();
            this.loader.loaderEvent.emit(false)
            // Swal.fire(res.message);
            Swal.fire( res.message
            )
          } else {
            Swal.fire(res.message, '', 'error');
          }
        });

      }
    })
  }
  rowClick(obj: any, i: any) {
    //console.log(obj, i);

  }


}
