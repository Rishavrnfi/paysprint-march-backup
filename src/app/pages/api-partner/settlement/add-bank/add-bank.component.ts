import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng-select2';
import { Options } from 'select2';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import { MustMatch } from 'src/app/_helpers/must.match.validator';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-bank',
  templateUrl: './add-bank.component.html',
  styleUrls: ['./add-bank.component.css']
})
export class AddBankComponent implements OnInit {
  exampleData: Array<Select2OptionData> = [];
  options: Options = {}
  tranLst: any = [];
  form = new FormGroup({});
  bankList: any;
  bankidFin: any;
  constructor(private _CommonService: CommonService, private _auth: ApiService) {
  }

  ngOnInit(): void {
    this.listBank();

    this.form = new FormGroup({
      bankid: new FormControl('', [Validators.required]),
      benename: new FormControl(null, [Validators.required]),
      accno: new FormControl(null, [Validators.required]),
      confirmaccno: new FormControl(null, [Validators.required]),
      ifsccode: new FormControl('', [Validators.required]),
    }, {
      validators: MustMatch('accno', 'confirmaccno')
    })
    this.bankId?.valueChanges.subscribe((val: any) => {
      this.bankidFin = val.id;
      let fltr = this.bankList.filter((dt: any) => {
        return dt.id == val;
      })
      for (const key in fltr[0]) {
        const element = fltr[0][key];
        if (key === 'ifsc') {
          this.form.patchValue({ "ifsccode": element })
        }
      }
    })
  }
  listBank() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getBankLst).subscribe((res: any) => {
      if (res.statuscode == 1) {
        this.bankList = res.banklist;
        let newArray: any = [];
        res.banklist.forEach((element: any) => {
          newArray.push({ id: element.id, text: element.bankname });
        });
        this.exampleData = newArray;
      } else {
      }
    });
  }
  onSubmit() {
    //console.log(this.form.value);

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('bankid', this.form.get('bankid')?.value);
    formdata.append('benename', this.form.get('benename')?.value);
    formdata.append('accno', this.form.get('accno')?.value);
    formdata.append('confirmaccno', this.form.get('confirmaccno')?.value);
    formdata.append('ifsccode', this.form.get('ifsccode')?.value);
    this._auth.postdata(formdata, config.registerBank).subscribe((res: any) => {
      if (res.statuscode == 200 || res.statuscode === 1) {
        Swal.fire(res.message);
        this.form.reset();
      } else {
        Swal.fire(res.message);
      }
    });

  }

  get bankId() {
    return this.form.controls['bankid'];
  }

}
