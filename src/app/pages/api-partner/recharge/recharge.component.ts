import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { element } from 'protractor';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.css']
})
export class RechargeComponent implements OnInit {
  RechargeForm: any = [];
  categorydata : any;
  catName : any;
  searchdata:any;
  CurrentTime: any;
  showData = false;
  showD=false;
  constructor(private _auth:ApiService ,
    private fb: FormBuilder) { 
      setInterval(() => {
    this.CurrentTime = Math.floor(Date.now()/1000); 
      },200);
   }
  
  ngOnInit(): void { 
    this.RechargeForm = this.fb.group({
      cateogry: ['', [Validators.required]],
      Operator: ['', [Validators.required]],
      mobile: ['', [Validators.pattern('[6789][0-9]{9}')]],
      amount: ['', [Validators.required]] 

    })
    let newArray: any = [];  
    let uniqueObject: any = {}; 
    for (let i in this.categorydata) { 
      let objTitle = this.categorydata[i]['category']; 
      uniqueObject[objTitle] = this.categorydata[i];
    }  
    for (let j in uniqueObject) {
      newArray.push({ 'catName': uniqueObject[j]['category']});
    }
    //console.log(newArray);
    const formdata = new FormData();
    this._auth.postdata(formdata,config.rechargeGetoperator).subscribe((res: any) => {  
      this.categorydata =res.data;
      
    let newArray: any = []; 
    let uniqueObject: any = {}; 
    for (let i in res.data) { 
      let objTitle = res.data[i]['category']; 
      uniqueObject[objTitle] = res.data[i];
    } 
    for (let j in uniqueObject) {
      newArray.push({ 'catName': uniqueObject[j]['category'] });
    }
    this.catName =newArray    
    }); 
  } 
  getOprator(val: any) { 
   var  marvelHeroes =  this.categorydata.filter(function(res : any) { 
    return res.category == val;
   }); 
   this.searchdata =marvelHeroes;
   this.showData =true;
  
  }
  getmobile(val: any){
    this.showD=true;
  }

  doRecharge(){
    const formdata = new FormData();
    formdata.append('operator', this.RechargeForm.get('Operator').value);
    formdata.append('canumber', this.RechargeForm.get('mobile').value);
    formdata.append('amount', this.RechargeForm.get('amount').value);
    formdata.append('referenceid', this.CurrentTime);
    this._auth.postdata(formdata, config.rechargedorecharge)
      .subscribe((res: any) => {
        if (res.response_code == 1) {
          Swal.fire(res.message).then((result) => {
            if (result.isConfirmed) {
              this.RechargeForm.reset();
            }
          }) 
        } else {
          Swal.fire(res.message);
        }
      });
  }

  get f() {
    return this.RechargeForm.controls
  }
}
