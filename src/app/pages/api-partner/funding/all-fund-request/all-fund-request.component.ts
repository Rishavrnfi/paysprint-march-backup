import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-all-fund-request',
  templateUrl: './all-fund-request.component.html',
  styleUrls: ['./all-fund-request.component.css']
})
export class AllFundRequestComponent implements OnInit {
  fundrequestList: any = [];
  startDt: any;
  endDt: any;
  showError = false;
  fundrequest :any;
  fromDt: any = null;
  toDt: any = null;
  minDate: Date;
  maxDate: Date;
  fromDta:any;
  

  constructor(private _CommonService: CommonService, private _auth: ApiService, private datePipe: DatePipe, private fb: FormBuilder) {
    const DateValue = new Date();
    this.minDate = new Date(2017, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate())
  }

  ngOnInit(): void {
    this.fundrequest = this.fb.group({
      fromDt:['',[Validators.required]],
      toDt : ['',[Validators.required]]

    }); 
  }
  getFundrequestList() {
    const fromDt = this.datePipe.transform(this.fromDt, 'yyyy-MM-dd');
    const toDt = this.datePipe.transform(this.toDt, 'yyyy-MM-dd');

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('startdate', fromDt === null ? '' : fromDt);
    formdata.append('enddate', toDt === null ? '' : toDt);
    this._auth.postdata(formdata, config.getfundrequest).subscribe((res: any) => {

      if (res.statuscode == 200) {
        // Swal.fire(res.message); 
        this.fundrequestList = res.data;
        if (this.fundrequestList.length === 0) {
          this.showError = true;
        }
        ////console.log(res.data);
      } else {
        Swal.fire({
          icon: 'error',
          title: res.message,
          text: 'Something went wrong!'
        })

      }
    });
  }
  getStatus(status: any) {
    const st: any = this._CommonService.getStatus()
    return st[status] === undefined ? 'N/A' : st[status];
  }
  getRequest(status: any) {
    const rt: any = this._CommonService.getRequestType()
    return rt[status] === undefined ? 'N/A' : rt[status];
  }
  get f() {
    return this.fundrequest.controls
  }
}
