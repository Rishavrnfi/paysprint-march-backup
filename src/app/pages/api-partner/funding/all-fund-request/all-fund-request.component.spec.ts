import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllFundRequestComponent } from './all-fund-request.component';

describe('AllFundRequestComponent', () => {
  let component: AllFundRequestComponent;
  let fixture: ComponentFixture<AllFundRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllFundRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllFundRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
