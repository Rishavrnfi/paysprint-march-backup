import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import Swal from 'sweetalert2';

export function ValidateMin(c: AbstractControl): { [key: string]: any } | null {
  if (c.value < 1000 || isNaN(c.value)) {
    return { 'amountInvalid': true };
  }
  return null;
}
@Component({
  selector: 'app-exceptioal-request',
  templateUrl: './exceptioal-request.component.html',
  styleUrls: ['./exceptioal-request.component.css']
})
export class ExceptioalRequestComponent implements OnInit {

  @ViewChild('fileUploader', { static: true }) fileUploader!: ElementRef;


  imagePath: any;
  form = new FormGroup({});
  amount: any;
  isshow: boolean = false;
  constructor(private _auth: ApiService, private http: HttpClient, private _CommonService: CommonService,) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      requesttype: new FormControl('5', [Validators.required]),
      amount: new FormControl(null, [Validators.required, ValidateMin]),
      transactionid: new FormControl('', [Validators.required]),
      depositeddate: new FormControl(''),
      bankid: new FormControl(''),
      profile: new FormControl('', [Validators.required]),
    })
  }

  onSubmit() {
    Swal.fire({
      title: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.isConfirmed) {
        const formdata = new FormData();
        //5
        formdata.append('token', config.tokenauth);
        formdata.append('requesttype', this.form.get('requesttype')?.value);
        formdata.append('amount', this.form.get('amount')?.value);
        formdata.append('transactionid', this.form.get('transactionid')?.value);
        formdata.append('depositeddate', this.form.get('depositeddate')?.value);
        formdata.append('bankid', this.form.get('bankid')?.value);
        this._auth.postdata(formdata, config.createfundrequest).subscribe((res: any) => {
          //Fund Request Successfully raised.
          if (res.statuscode == 200) {
            Swal.fire({
              title: 'Hurray!!',
              text: 'Fund Request Successfully raised.',
              icon: 'success'
            });
            this.resetForm();
            this.isshow = false;
          } else {
            Swal.fire({
              icon: 'error',
              title: res.message,
              text: 'Something went wrong!'
            })
          }
        });

      }
    })
  }
  get f() {
    return this.form.controls;
  }
  onFileSelect(event: any) {
    if (event.target.files.length > 0) {
      const HttpUploadOptions = {
        headers: new HttpHeaders({ "Content-Type": "multipart/form-data" })
      }
      const file = event.target.files[0];
      this.form.get('profile')?.setValue(file);
      const endpoint = 'https://docs.paysprint.in/upload/uploadimage';
      const formdata: FormData = new FormData();
      formdata.append('token', '27faf26c87d99ae520ade3ce7cd88684');
      formdata.append('image', this.form.get('profile')?.value);
      this.http.post(endpoint, formdata, HttpUploadOptions).subscribe((res: any) => {
        if (res.response == 1) {
          this.imagePath = res.pan_proof;

        } else {
          Swal.fire({
            icon: 'error',
            title: res.message,
            text: 'Something went wrong!'
          })
        }
      });
    }
  }
  resetForm() {
    this.form.reset('');
    this.form.patchValue({
      "amount": null,
      "transactionid": '',
      "profile": '', 
    })
    this.fileUploader.nativeElement.value = null;
  }
  onKey(val: any) {
    this.amount = this._CommonService.convertNumberToWords(val.target.value);
    if (!this.amount) {
      this.isshow = false;
    } else {
      this.isshow = true;
    }

    ////console.log(this.amount)
  }
}
