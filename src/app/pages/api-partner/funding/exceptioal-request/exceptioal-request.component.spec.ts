import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceptioalRequestComponent } from './exceptioal-request.component';

describe('ExceptioalRequestComponent', () => {
  let component: ExceptioalRequestComponent;
  let fixture: ComponentFixture<ExceptioalRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExceptioalRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceptioalRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
