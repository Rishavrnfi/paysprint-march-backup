import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineTransferComponent } from './online-transfer.component';

describe('OnlineTransferComponent', () => {
  let component: OnlineTransferComponent;
  let fixture: ComponentFixture<OnlineTransferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineTransferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
