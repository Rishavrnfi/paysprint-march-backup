import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import Swal from 'sweetalert2';
import { NgbDateStruct, NgbCalendar, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Select2OptionData } from 'ng-select2';

export function ValidateMin(c: AbstractControl): {[key: string]: any} | null  {
  if (c.value < 1000 || isNaN(c.value)) {
    return { 'amountInvalid': true };
  }
  return null;
}

@Component({
  selector: 'app-online-transfer',
  templateUrl: './online-transfer.component.html',
  styleUrls: ['./online-transfer.component.css']
})
export class OnlineTransferComponent implements OnInit {
  @ViewChild('fileUploader', { static: true }) fileUploader!: ElementRef;
  exampleData: Array<Select2OptionData> = [];
  depositeddt: any = null;
  minDate!: Date;
  maxDate!: Date;
  tranLst: any;
  form = new FormGroup({});
  amount:any;
  isshow:boolean=false;
  CompanyBankList: any;
  imagePath : any; 
  constructor(private _CommonService: CommonService, private _auth: ApiService, private calendar: NgbCalendar, config: NgbDatepickerConfig, private http: HttpClient, private datePipe: DatePipe) {
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate())
  }

  ngOnInit(): void {
    this.listCompanyBank();
    this.tranLst = this._CommonService.getRequestType()
    delete this.tranLst[0];
    delete this.tranLst[4];
    delete this.tranLst[5];

    this.form = new FormGroup({
      requesttype: new FormControl('', [Validators.required]),
      amount: new FormControl(null, [Validators.required,ValidateMin]),
      transactionid: new FormControl(null, [Validators.required]),
      depositeddate: new FormControl(null, [Validators.required]),
      bankid: new FormControl('', [Validators.required]),
      profile: new FormControl('', [Validators.required]),
    })
  }
 
  listCompanyBank() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
      if (res.statuscode == 200) {  
        this.CompanyBankList = res.data.filter((dt: any) => {
          return dt.status == 1;
        }) 
        let newArray: any = [];
        this.CompanyBankList.forEach((element: any) => {
          var name = element.name;
          var accn = element.accno;
          var ifsc = element.ifsc;
          newArray.push({ id: element.id, text: name +' | '+ accn +' | '+ ifsc});
        });
        this.exampleData = newArray;
      } else {
      }
    });
  }
  onSubmit() {
    Swal.fire({
      title: 'Are you sure?', 
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {

      if (result.isConfirmed) {
        const formdata = new FormData();
        formdata.append('token', config.tokenauth);
        formdata.append('requesttype', this.form.get('requesttype')?.value);
        formdata.append('amount', this.form.get('amount')?.value);
        formdata.append('transactionid', this.form.get('transactionid')?.value);
        formdata.append('depositeddate', this.form.get('depositeddate')?.value);
        formdata.append('bankid', this.form.get('bankid')?.value);
        formdata.append('image', this.imagePath);
        this._auth.postdata(formdata, config.createfundrequest).subscribe((res: any) => {
          if (res.statuscode == 200) {
            Swal.fire({
              title: 'Hurray!!',
              text: 'Fund Request Successfully raised.',
              icon: 'success'
            });
            this.resetForm();
            this.isshow = false;
          } else {
            Swal.fire({
              icon: 'error',
              title: res.message,
              text: 'Something went wrong!'
            })
          }
        });

      }
    })


  }
  get f() {
    return this.form.controls;
  }
  onFileSelect(event: any) {
    if (event.target.files.length > 0) {
      const HttpUploadOptions = {
        headers: new HttpHeaders({ "Content-Type": "multipart/form-data" })
      }
      const file = event.target.files[0];
      this.form.get('profile')?.setValue(file);
      const endpoint = 'https://docs.paysprint.in/upload/uploadimage';
      const formdata: FormData = new FormData();
      formdata.append('token', '27faf26c87d99ae520ade3ce7cd88684');
      formdata.append('image', this.form.get('profile')?.value);
      this.http.post(endpoint, formdata, HttpUploadOptions).subscribe((res: any) => {
        if (res.response == 1) {
          this.imagePath = res.pan_proof; 
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message,
            text: 'Something went wrong!'
          })
        }
      });
    }
  }
  resetForm() {
    this.form.reset('');
    this.form.patchValue({
      "requesttype": '',
      "bankid": '',
      "profile":''
    })
    this.fileUploader.nativeElement.value = null;
  }
  onKey(val:any){  
    this.amount = this._CommonService.convertNumberToWords(val.target.value);
    if(!this.amount){
      this.isshow = false;
    }else{
      this.isshow = true;
    }
   
    ////console.log(this.amount)
  }
}
