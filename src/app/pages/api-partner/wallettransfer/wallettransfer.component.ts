import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { LoaderService } from 'src/app/_helpers/common/loader.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-wallettransfer',
  templateUrl: './wallettransfer.component.html',
  styleUrls: ['./wallettransfer.component.css']
})
export class WallettransferComponent implements OnInit {
  @ViewChild('amt') amt: any;
  constructor(private _auth: ApiService,private loader:LoaderService) { }

  ngOnInit(): void {
  }

  walletTfr(val: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Add Amount Of Rs.' + val,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Add it!'
    }).then((result) => {

      if (result.isConfirmed) {
        const formdata = new FormData();
        formdata.append('token', config.tokenauth);
        formdata.append('amount', val);
        this._auth.postdata(formdata, config.wallettransfer).subscribe((res: any) => {
          if (res.statuscode == 200) {
            Swal.fire(res.message);
            this.amt.nativeElement.value = '';
            this.loader.loaderEvent.emit(false)
            // Swal.fire(res.message);
            Swal.fire(
              'Done!',
              'Rs. ' + val + ' has been Added.',
              'success'
            )
          } else {
            Swal.fire(res.message, '', 'error');
          }
        });

      }
    })
  }
}
