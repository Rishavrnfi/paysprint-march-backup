import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { LoaderService } from 'src/app/_helpers/common/loader.service';
import Swal from 'sweetalert2';

export function ValidateMin(c: AbstractControl): {[key: string]: any} | null  {
  if (c.value < 100 || isNaN(c.value)) {
    return { 'amountInvalid': true };
  }
  return null;
}
@Component({
  selector: 'app-gateway-pg',
  templateUrl: './gateway-pg.component.html',
  styleUrls: ['./gateway-pg.component.css']
})
export class GatewayPgComponent implements OnInit {
  addPg :any = FormGroup;
  submitted = false;
  is_active: boolean = true;
  is_value: string = "DC"
  private timedata: any;
  private isPgDetail: boolean = true;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private router:Router,
    private loader:LoaderService
  ) {
    this.addPg = this.fb.group({
      mode: ['', [Validators.required]],
      amount: ['', [Validators.required,ValidateMin]],
      mobile :['',[Validators.required,Validators.pattern('[6789][0-9]{9}')]],
      email :['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      card :['',[Validators.required,Validators.pattern('[0-9]{4}'),Validators.maxLength(4)]],
      name: ['',[Validators.required]],
    });
  }

  ngOnInit(): void {
    localStorage.removeItem('Gatewaypg');
  }

  submitPg(){
    this.submitted = true;
    if (this.addPg.invalid){ 
      return;
    }else{
      this.timedata = Math.floor(Date.now()/1000);
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('mode',this.addPg.get('mode').value);
      formdata.append('name',this.addPg.get('name').value);
      formdata.append('amount',this.addPg.get('amount').value);
      formdata.append('mobile',this.addPg.get('mobile').value);
      formdata.append('email',this.addPg.get('email').value);
      formdata.append('card',this.addPg.get('card').value);
      formdata.append('refid',this.timedata);
      formdata.append('redirect_url','https://paysprint.in/front/#/gateway-pg-response');
      //formdata.append('redirect_url','http://localhost:4200/#/gateway-pg-response');
      this._auth.postdata(formdata,config.gatewayPg).subscribe((res: any) => {
        if (res.statuscode == 1) {
          this.loader.loaderEvent.emit(true);
          this.openWindow(res);
        } else {
          Swal.fire(res.message);
        }
      })
    }
  }

  handleChange(e:any){
    if(e.target.value =='NB'){
      this.is_active = false;
      this.addPg.get('card').clearValidators();
      this.addPg.get('name').clearValidators(); 
      this.addPg.get('card').updateValueAndValidity();
      this.addPg.get('name').updateValueAndValidity();
    }else{
      this.is_active = true;
      this.addPg.get('card').setValidators([Validators.required,Validators.pattern('[0-9]{4}'),Validators.maxLength(4)]);
      this.addPg.get('name').setValidators([Validators.required]);
      this.addPg.get(['card','name']).updateValueAndValidity();
    }
  }

  openWindow(res:any){
    let form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", res.data.url);
    let element1:any = document.createElement("INPUT");
    element1.name="encdata"
    element1.value = res.data.encdata;
    element1.type = 'hidden'
    form.appendChild(element1);
    let newwindow:any = window.open('', 'name', 'width=800,height=600');
    function popitup() {
      newwindow.document.body.appendChild(form);
      newwindow.document.forms[0].submit();
      return false;
    }
    popitup();
    var timer = setInterval(()=> {
      if(this.isPgDetail){
        if(newwindow.closed) {
          this._clearInput();
          clearInterval(timer);
          this.loader.loaderEvent.emit(false);
        }
        this.getDetails();
      }else{
        newwindow.close();
        this._clearInput();
        clearInterval(timer);
        this.loader.loaderEvent.emit(false);
        this.router.navigate(['/pg-recepit']);
      }
    }, 1000);
  }

  getDetails(){
    if ("Gatewaypg" in localStorage) {
      this.isPgDetail = false;
    } else {
      this.isPgDetail = true;
    }
  }

  _clearInput(){
    this.addPg.patchValue({
      mode: '',
      amount: '',
      mobile :'',
      email :'',
      card :'',
      name: '',
    });
  }

  get f() { return this.addPg.controls;}

}
