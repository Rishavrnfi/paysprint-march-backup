import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayPgComponent } from './gateway-pg.component';

describe('GatewayPgComponent', () => {
  let component: GatewayPgComponent;
  let fixture: ComponentFixture<GatewayPgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GatewayPgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayPgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
