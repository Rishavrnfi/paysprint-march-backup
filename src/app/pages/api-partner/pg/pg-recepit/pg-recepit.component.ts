import { Component, OnInit } from '@angular/core';
import { EncodeDecode } from 'src/app/_helpers/encode-decode'

@Component({
  selector: 'app-pg-recepit',
  templateUrl: './pg-recepit.component.html',
  styleUrls: ['./pg-recepit.component.css']
})
export class PgRecepitComponent implements OnInit {
  public recepitDetails: any ={};
  constructor() { }

  ngOnInit(): void {
    let encode:any = EncodeDecode('n',localStorage.getItem('Gatewaypg'))
    let data = decodeURI(encode);
    let detail = data.split('=')[1];
    //console.log(data.split('='));
    this.recepitDetails = JSON.parse(detail);
    this._removeObject(['name','mode','refid','card']);
   
  }

  _removeObject(remove:any){
    for(let i in this.recepitDetails){
        for(let j in remove){
          if(i == remove[j]){
          delete this.recepitDetails[remove[j]]
          }
        }
     }
  }
}
