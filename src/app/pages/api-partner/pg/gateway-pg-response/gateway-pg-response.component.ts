import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EncodeDecode } from 'src/app/_helpers/encode-decode'

@Component({
  selector: 'app-gateway-pg-response',
  templateUrl: './gateway-pg-response.component.html',
  styleUrls: ['./gateway-pg-response.component.css']
})
export class GatewayPgResponseComponent implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    let href = this.router.url;
    let url = href.split("?")[1]
    if(typeof(url) !== 'undefined'){
      let encryptData:any = EncodeDecode(url,'n');
      localStorage.setItem('Gatewaypg', encryptData);
    }else{
      localStorage.removeItem('LoginDetails');
      localStorage.clear();
      this.router.navigate(['/login']);
    }
  }

}
