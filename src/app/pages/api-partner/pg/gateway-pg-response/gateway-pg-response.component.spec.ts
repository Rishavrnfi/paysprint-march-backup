import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayPgResponseComponent } from './gateway-pg-response.component';

describe('GatewayPgResponseComponent', () => {
  let component: GatewayPgResponseComponent;
  let fixture: ComponentFixture<GatewayPgResponseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GatewayPgResponseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayPgResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
