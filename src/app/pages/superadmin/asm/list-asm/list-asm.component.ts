import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config'; 
import { CommonService } from 'src/app/_helpers/common/common.service';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-list-asm',
  templateUrl: './list-asm.component.html',
  styleUrls: ['./list-asm.component.css']
})
export class ListAsmComponent implements OnInit {
  public data:any;
  public pageOption: Object = {pageCount: 5};
  AsmList: any = [];
  updateasm :any = FormGroup; 
  asmID:any;
  activestatus:any; 
  remark :any;
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal : CustomeModalService ,
    private convrtAmt : CommonService
  ) {
    
    this.updateasm = this.fb.group({
      name:   ['', [Validators.required]],   
      email      :['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo    :['',[Validators.required,Validators.pattern('[6789][0-9]{9}')]], 
      remarks    :['',[Validators.required]],
      activestatus:['',[Validators.required]]
    });
  }

  ngOnInit(): void {
    this.listAsm(); 
    // let val = this.convrtAmt.convertNumberToWords(200);
    // alert(val)
  }
  listAsm() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.asmList).subscribe((res: any) => {
      this.AsmList = res.data;
     // //console.log(res)
      setTimeout(() => {
        $('#asmDatatable').DataTable({
          pagingType: 'full_numbers',
          language: {
            paginate: { "first": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
            "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
            "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
            "last": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'},
            
            search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            searchPlaceholder: "Search...",
            lengthMenu: "Results :  _MENU_",
          },
          lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
          processing: true,
        });
      }, 1); 
    })
  }

  editasm(data :any,details : any){ 
    this.modal.open(data);
    this.updateasm.reset(); 
    const formdata = new FormData();
    formdata.append('userid',details.id);
    formdata.append('token', config.tokenauth); 
    this._auth.postdata(formdata,config.getasm).subscribe((res: any) => {  
       this.asmID = res.data.userid; 
       this.updateasm.controls["name"].setValue(res.data.name); 
       this.updateasm.controls["email"].setValue(res.data.email);  
       this.updateasm.controls["phoneNo"].setValue(res.data.phone); 
       this.activestatus = res.data.status;
       this.remark=res.data.remarks;
    }) 
  }
  
  async edit_asm(){
    if (!this.updateasm.valid){
      return;
    }else{
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name',this.updateasm.get('name').value);  
      formdata.append('phone',this.updateasm.get('phoneNo').value);  
      formdata.append('email',this.updateasm.get('email').value); 
      formdata.append('remarks',this.updateasm.get('remarks').value); 
      formdata.append('userid', this.asmID); 
      formdata.append('status',this.updateasm.get('activestatus').value);
      this._auth.postdata(formdata,config.editasm).subscribe((res: any) => { 
        if (res.statuscode == 200) {
          this.modal.close();
          this.updateasm.reset(); 
          const formdata = new FormData();
          formdata.append('token', config.tokenauth);
          this._auth.postdata(formdata,config.asmList).subscribe((res: any) => { 
            this.listAsm = res.data;  
          });
          Swal.fire({ 
            title:   res.message,
            icon: 'success'
          }); 
        }else{
          Swal.fire({
            icon: 'error',
            title: res.message, 
          }) 
        }
      })
    }
  } 
  
   
  get f() { return this.updateasm.controls;}
}
