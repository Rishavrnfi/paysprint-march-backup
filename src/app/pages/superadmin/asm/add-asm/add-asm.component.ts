import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-asm',
  templateUrl:  './add-asm.component.html',
  styleUrls: ['./add-asm.component.css']
})
export class AddAsmComponent implements OnInit {
  addAsm :any = FormGroup; 
  activestatus:any; 
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
  ){ 
    this.addAsm = this.fb.group({
      name: ['', [Validators.required]], 
      email      :['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo    :['',[Validators.required,Validators.pattern('[6789][0-9]{9}')]], 
      activestatus:['',[Validators.required]]
    });
  }

  ngOnInit(): void {
  }
  async add_asm(){
    if (!this.addAsm.valid){
      return;
    }else{
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name',this.addAsm.get('name').value);  
      formdata.append('phone',this.addAsm.get('phoneNo').value);  
      formdata.append('email',this.addAsm.get('email').value);  
      formdata.append('status',this.addAsm.get('activestatus').value);  
      this._auth.postdata(formdata,config.addasm).subscribe((res: any) => { 
        if (res.statuscode == 200) {
          Swal.fire({
            title: 'Hurray!!',
            text:   res.message,
            icon: 'success'
          });  
          this.addAsm.reset();
        }else{
          Swal.fire({
            icon: 'error',
            title: res.message,
            text: 'Something went wrong!' 
          }) 
        }
      })
    }
  }

   
  get f() { return this.addAsm.controls;}
}
