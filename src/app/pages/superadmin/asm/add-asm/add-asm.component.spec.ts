import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAsmComponent } from './add-asm.component';

describe('AddAsmComponent', () => {
  let component: AddAsmComponent;
  let fixture: ComponentFixture<AddAsmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAsmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAsmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
