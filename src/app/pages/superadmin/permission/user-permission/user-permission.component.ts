import { Component, OnInit, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-permission',
  templateUrl: './user-permission.component.html',
  styleUrls: ['./user-permission.component.css']
})
export class UserPermissionComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any = [];


  @Input() userId: any = '';

  updatedData: any = [];
  dynamicForm: FormGroup;
  upUserId: any;
  showData = false;
  isAnyChanges: boolean = false;
  showErrorMesg: any = null;
  userLst: any;
  isShow: boolean = false;
  newUpdatedData: any = [];

  constructor(
    private fb: FormBuilder,
    private _auth: ApiService
  ) {
    this.dynamicForm = this.fb.group({});
    // this.userId = '3399198201767469569760';
  }

  ngOnInit() {

    ////console.log(this.userId);

    if (this.userId !== '') {
      this.selectEvent(this.userId);
    }

  }
  selectEvent(event: any) {
    this.showData = false;

    if (event !== undefined) {
      this.updatedData = [];
      this.newUpdatedData = [];
      this.upUserId = event.userid;
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('userid', this.upUserId);
      // formdata.append('userid', '3399198201767469569760');
      this._auth.postdata(formdata, config.getuserpermission).subscribe((res: any) => {
        if (res.statuscode == 200) {
          this.data = res.data;
          for (const key in this.data) {
            let sudDtArr: any = [];
            for (const key1 in this.data[key]) {
              let newSubObj = {
                name: key1,
                value: this.data[key][key1]
              }
              sudDtArr.push(newSubObj)
            }
            let newObj = {
              heading: key,
              items: sudDtArr
            }
            this.updatedData.push(newObj);
            this.newUpdatedData.push(newObj);
            ////console.log(this.updatedData);
          }

          this.showData = true;
        }
        if (res.statuscode == 404) {

          this.showErrorMesg = res.message;
        }
      });
    }

  }


  handleChange(name: any, event: any, i: any, j: any) {
    this.isAnyChanges = true;
    this.newUpdatedData[i].items[j].value = event.target.value;
  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }

  onSubmit(val: any) {
    let finalData: any[] = [];
    const formdata = new FormData();

    this.newUpdatedData.map((element: any) => {
      if (element.heading !== 'userid' && element.heading !== 'is_editable') {
        for (const key in element.items) {
          let ele: any = element.items[key];
          formdata.append(ele['name'], ele['value']);
          finalData.push({ [ele['name']]: ele['value'] });
        }
      }
    });

    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.upUserId);


    //console.log(finalData);
    this._auth.postdata(formdata, config.changepermission).subscribe((res: any) => {
      if (res.statuscode == 200) {
        Swal.fire(res.message);
      } else {
        Swal.fire(res.message);
      }
    })

  }

}
