import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Select2OptionData } from 'ng-select2';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-staff-permission',
  templateUrl: './staff-permission.component.html',
  styleUrls: ['./staff-permission.component.css']
})
export class StaffPermissionComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any = [];

  updatedData: any = [];
  dynamicForm: FormGroup;
  upUserId: any;
  showData = false;
  isShow = false;
  showErrorMesg: any = null;
  staffLst: any[] = [];
  constructor(private fb: FormBuilder, private _auth: ApiService) { this.dynamicForm = this.fb.group({}); }

  ngOnInit(): void {

  }
  selectEvent(event: any) {

    this.upUserId = event.userid;
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.upUserId);
    // formdata.append('userid', '3399198201767469569760');
    this._auth.postdata(formdata, config.getpermissionbyuser).subscribe((res: any) => {
      this.dynamicForm = this.fb.group({});
      this.updatedData = [];
      if (res.statuscode == 200) {
        this.data = res.data;
        for (const key in this.data) {

          if (key === 'is_editable') {
            this.isShow = this.data[key] === '0' ? false : true;
          }
          if (key !== 'userid' && key !== 'is_editable' && key !== 'loginusertype') {
            this.updatedData.push({ name: key, value: this.data[key] });

          } else {
            // this.upUserId = this.data[key];

          }
        }

        this.updatedData.forEach((question: any) => {
          this.dynamicForm.addControl(question.name, this.fb.control(question.value));
        })

        this.showData = true;
        this.showErrorMesg = null;
      }
      if (res.statuscode == 404) {
        this.showData = false;
        this.showErrorMesg = res.message;
      }

    });

  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  onSubmit() {

    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.upUserId);

    for (const key in this.dynamicForm.value) {
      if (key !== '__proto__') {
        formdata.append(key, this.dynamicForm.value[key]);
      }
    }
    // formdata.forEach((value: any, key: any) => {
    // });
    this._auth.postdata(formdata, config.changeStaffpermission).subscribe((res: any) => {
      // this.data = res.data;
      // if (res.statuscode == 200) {
      Swal.fire(res.message).then((result) => {
        if (result.isConfirmed) {
          // this.apiPartner.reset();
          this.selectEvent(this.upUserId)
        }
      })
      // }
    });
  }


}
