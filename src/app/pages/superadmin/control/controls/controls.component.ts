import { Component, Directive, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})
export class ControlsComponent implements OnInit {
  data: any = [];
  updatedData: any = [];
  constructor(private _auth: ApiService,) { }

  ngOnInit(): void {
    this.getControl();
  }
  testFR(name: any, event: any) {

  }
  getControl() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.control).subscribe((res: any) => {
      this.data = res.data;
      for (const key in this.data) {
        let sudDtArr: any = [];
        for (const key1 in this.data[key]) {
          let newSubObj = {
            name: key1,
            value: this.data[key][key1]
          }
          sudDtArr.push(newSubObj)
        }
        let newObj = {
          heading: key,
          items: sudDtArr
        }
        this.updatedData.push(newObj)
      }
    });


  }
 
  handleChange(name: any, event: any) {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('key', name);
    formdata.append('value', event.target.value);
    this._auth.postdata(formdata, config.controlupdate).subscribe((res: any) => {
      if (res.statuscode == 200) {
        Swal.fire(res.message);
      } else {
        Swal.fire(res.message);
      }
    })
  }
}