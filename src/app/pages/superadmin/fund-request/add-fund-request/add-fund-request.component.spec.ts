import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFundRequestComponent } from './add-fund-request.component';

describe('AddFundRequestComponent', () => {
  let component: AddFundRequestComponent;
  let fixture: ComponentFixture<AddFundRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFundRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFundRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
