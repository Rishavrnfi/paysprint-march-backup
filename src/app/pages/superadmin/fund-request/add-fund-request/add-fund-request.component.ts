import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import { LoaderService } from 'src/app/_helpers/common/loader.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-fund-request',
  templateUrl: './add-fund-request.component.html',
  styleUrls: ['./add-fund-request.component.css']
})
export class AddFundRequestComponent implements OnInit {
  addFunding :any = FormGroup; 
  submitted = false;  
  amount:any;
  isshow:boolean=false;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route:Router,
    private loader:LoaderService,
    private convrtAmt : CommonService
  ) { 
    this.addFunding = this.fb.group({
      amount : ['', [Validators.required,Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/),Validators.max(10000000), Validators.min(0)]],
      otp    :['',[Validators.required,Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
  }

  ngOnInit(): void {
  }
  OnAddFunding(){
    this.submitted = true;
    if (this.addFunding.invalid){
      return;
    }else{
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('amount',this.addFunding.get('amount').value);
      formdata.append('otp',this.addFunding.get('otp').value);
      Swal.fire({
        title: 'Are you sure you want to transfer RS. ' +this.addFunding.get('amount').value+ ' amount?',
        showDenyButton: true,
        icon: 'warning', 
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        denyButtonText: `Don't save`,
        confirmButtonText: 'Yes, Submit it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this._auth.postdata(formdata,config.addFundAdmin).subscribe((res: any) => { 
            if (res.statuscode == 200) {
              Swal.fire(res.message).then((result) => {
                if (result.isConfirmed) {
                  this.addFunding.reset();
                  this.loader.loaderEvent.emit(false);
                  this.isshow = false;
                }
              }) 
            } else {
              Swal.fire({
                icon: 'error',
                title: res.message, 
              }) 
            }
          }) 
        }else if (result.isDenied) {
          this.addFunding.reset();
        }
      })



     
    }
  }
  onKey(val:any){  
    this.amount = this.convrtAmt.convertNumberToWords(val.target.value);
    if(!this.amount){
      this.isshow = false;
    }else{
      this.isshow = true;
    }
   
    ////console.log(this.amount)
  }
  get f() { return this.addFunding.controls;}
}
