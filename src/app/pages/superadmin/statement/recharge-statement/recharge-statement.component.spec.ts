import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RechargeStatementComponent } from './recharge-statement.component';

describe('RechargeStatementComponent', () => {
  let component: RechargeStatementComponent;
  let fixture: ComponentFixture<RechargeStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RechargeStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RechargeStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
