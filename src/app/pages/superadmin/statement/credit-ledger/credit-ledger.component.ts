import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CustConfg } from 'src/app/_helpers/common/custom-datepicker/ngx-datePicker-CustConfg';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-credit-ledger',
  templateUrl: './credit-ledger.component.html',
  styleUrls: ['./credit-ledger.component.css']
})
export class CreditLedgerComponent implements OnInit {
  @ViewChild('rangePicker') rangePicker: any;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  dtOptionscr: DataTables.Settings = {};
  form: FormGroup = new FormGroup({});
  minDate: Date;
  maxDate: Date;
  fromDt: any = null;
  toDt: any = null;
  morefilter: boolean = false;
  viewtable: boolean = false;
  details: any;
  billpayment: any;
  downloadcredit: any;
  csvcredit: any;
  formdata: any;
  testCon: boolean = false;
  userAutoComData: any;
  userKeyword = 'userdetails';
  isLoadingResult: boolean = false;
  click: number = 0;
  prevVal: any;
  userdt: any;
  showUserDtl: boolean = false;
  showActionType: any;
  usertype: any;
  bsCustConfg = CustConfg;
  IsDownloadDisabled: boolean = true;
  constructor(private http: HttpClient, private _auth: ApiService, private datePipe: DatePipe) {
    const DateValue = new Date();
    this.minDate = new Date(2017, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate())
    this.formdata = {
      'token': config.tokenauth,
      'startdate': '',
      'enddate': '',
      'searchby': '',
      'serachvalue': '',
      'status': '',
      'frombank': ''
    }
  }
  get f() {
    return this.form.controls
  }
  ngOnInit(): void {
    this.form = new FormGroup({
      // startdate: new FormControl('', [Validators.required]),
      // enddate: new FormControl('', [Validators.required]),
      selectdate: new FormControl([new Date(), new Date()], [Validators.required]),
      searchbyradio: new FormControl(''),
      searchvalue: new FormControl(''),
      status: new FormControl(''),
      frombank: new FormControl('')
    });
    let headers = new HttpHeaders({
      'Authkey': config.Authkey,
      'Authtoken': this._auth.isLoggedIn(),
      "Content-Type": "application/json"
    })

    const that = this;
    let options = { headers: headers };
    this.dtOptionscr = {
      autoWidth: false,
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "first": 'GHF', "last": "GHFH"
        },
        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results : _MENU_",
      },
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
      serverSide: true,
      processing: true,
      columnDefs: [
        {
          "targets": [1, 2, 3, 5, 6, 7, 8, 9, 10, 11],
          "orderable": false,
        },
      ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.testCon) {
          that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.getCreditRecords, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
            if (res.statuscode == 200) {
              ////console.log(res.data);
              that.billpayment = res.data;
              this.IsDownloadDisabled = res['data'].length > 0 ? false : true;
              this.viewtable = true;
              //console.log(res.data);
              callback({
                recordsTotal: res.recordsTotal,
                recordsFiltered: res.recordsFiltered,
                data: []
              });
            }
          });

        }
      },
    };
    this.usertype = this._auth.Getsessiondata().usertype;

  }

  moreFilter() {
    this.morefilter = true;
  }
  radiocheck(event: any) {
    if (event.target.value) {
      this.form.get('searchvalue')?.enable();
    } else {
      this.form.get('searchvalue')?.disable();
    }
  }
  ngOnDestroy(): void {
    $.fn['dataTable'].ext.search.pop();
  }
  filterAepsSettlement() {
    this.testCon = true;
    const startdate = this.datePipe.transform(this.form.get('selectdate')?.value[0], 'yyyy-MM-dd');
    const enddate = this.datePipe.transform(this.form.get('selectdate')?.value[1], 'yyyy-MM-dd');
    const currentdate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': (startdate === null ? '' : startdate),
      'enddate': (enddate === null ? '' : enddate),
      'searchby': this.form.get('searchbyradio')?.value,
      'serachvalue': this.form.get('searchvalue')?.value,
      'status': this.form.get('status')?.value,
      'userby': (this.prevVal === null ? '' : this.prevVal)
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  getServerResponse(val: any) {
    if (val != this.prevVal) {
      this.prevVal = val;
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      this.isLoadingResult = true;
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        this.isLoadingResult = false;
        if (res['data'] == undefined) {
          this.userAutoComData = [];
        } else {
          this.userAutoComData = res['data'];
        }
      });
    }

  }
  selectEvent(item: any) {
    // do something with selected item
    //console.log(item);
    this.showUserDtl = true;
    this.prevVal = item.userid
  }
  searchCleared() {
    //console.log('searchCleared');
    this.prevVal = null;
  }
  // download() {
  // let format;
  // const startdate = this.datePipe.transform(this.form.get('selectdate')?.value[0], 'yyyy-MM-dd');
  // const enddate = this.datePipe.transform(this.form.get('selectdate')?.value[1], 'yyyy-MM-dd');
  // let formdata: any = new FormData();
  // formdata.append('token', config.tokenauth);
  // formdata.append('startdate', (startdate === null ? '' : startdate));
  // formdata.append('enddate', (startdate === null ? '' : enddate));
  // formdata.append('searchby', this.form.get('searchbyradio')?.value);
  // formdata.append('serachvalue', this.form.get('serachvalue')?.value);
  // formdata.append('status', this.form.get('status')?.value);
  // formdata.append('userby', (this.prevVal === null ? '' : this.prevVal));
  // this._auth.postdata(formdata, config.downloadcredit).subscribe((res: any) => {
  //   if (res.statuscode == 200) {
  //     format = ["id", "uid", "amount", "comm", "opening", "closing", "utype", "status", "transactiontype", "dateadded", "tds", "username", "gst", "narration"];
  //     this._auth.downloadFile(res['data'], format, "Credit-Ledger-Stmt");
  //   }
  // });
  // }
  download($event: any) {
    const startdate = this.datePipe.transform(this.form.get('selectdate')?.value[0], 'yyyy-MM-dd');
    const enddate = this.datePipe.transform(this.form.get('selectdate')?.value[1], 'yyyy-MM-dd');
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('startdate', (startdate === null ? '' : startdate));
    formdata.append('enddate', (startdate === null ? '' : enddate));
    formdata.append('searchby', this.form.get('searchbyradio')?.value);
    formdata.append('serachvalue', this.form.get('serachvalue')?.value);
    formdata.append('status', this.form.get('status')?.value);
    formdata.append('userby', (this.prevVal === null ? '' : this.prevVal));
    this._auth.postdata(formdata, config.downloadcredit).subscribe((res: any) => {
      if (res.statuscode == 200) {
        const fileName = 'Credit-ledger.xlsx';
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res['data']);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Credit-ledger');
        XLSX.writeFile(wb, fileName);
      }else{
        Swal.fire({
          icon: 'error',
          title: res.message 
        }) 
      }
    });
  }
  onDateRangePickerShow() {
    // This is a workaround to show previous month
    var prevMonth = new Date();
    prevMonth.setMonth(prevMonth.getMonth() - 1);
    this.rangePicker._datepicker.instance.monthSelectHandler({ date: prevMonth });
  }
}