import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashDepositStatementComponent } from './cash-deposit-statement.component';

describe('CashDepositStatementComponent', () => {
  let component: CashDepositStatementComponent;
  let fixture: ComponentFixture<CashDepositStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashDepositStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CashDepositStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
