import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config'; 
import { CustConfg } from 'src/app/_helpers/common/custom-datepicker/ngx-datePicker-CustConfg';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';
import { environment } from 'src/environments/environment.prod';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-cash-deposit-statement',
  templateUrl: './cash-deposit-statement.component.html',
  styleUrls: ['./cash-deposit-statement.component.css']
})
export class CashDepositStatementComponent implements OnInit {
  @ViewChild('rangePicker') rangePicker:any;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  minDate!: Date;
  maxDate!: Date;
  details: any;
  username: any;
  dtOptions: DataTables.Settings = {};
  aeps_mini_stmt: any;
  public morefilter: boolean = false;
  public viewtable: boolean = false;
  public viewtbl: boolean = false;
  min: any = '';
  max: any = '';
  formdata: any;
  form: FormGroup = new FormGroup({});
  cashdepositst: any;
  //---View details variable--
  _cashdepositrefid: any;
  _commission: any;
  _profit: any;
  _ackno: any;
  _lat: any;
  _longt: any;
  _mobile: any;
  _accountno: any;
  _ipaddress: any;
  _tds: any;
  _bankiiin: any;
  IsDownloadDisabled: boolean = false;

public adddate:any;
  //---End---
  userAutoComData: any;
  userKeyword = 'userdetails';
  usertype: any;
  prevVal: any;
  bsCustConfg = CustConfg;
  constructor(
    private http: HttpClient,
    private auth: ApiService,
    private datepipe: DatePipe,
    private modal: CustomeModalService
  ) {
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': this.min,
      'enddate': this.max,
      'searchby': '',
      'serachvalue': '',
      'status': '',
    }
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate());
  }
  ngOnInit(): void {
    const encode: any = EncodeDecode('n', localStorage.getItem('LoginDetails'));
    const _permissionlist: any = JSON.parse(encode);
    this.details = _permissionlist.usertype;
    this.username = _permissionlist.username;
    ////console.log(this.details);
    this.form = new FormGroup({
      selectdate: new FormControl([new Date(),new Date()], [Validators.required]),
      // minDt: new FormControl('', Validators.required),
      // maxDt: new FormControl('', Validators.required),
      status: new FormControl(''),
      searchbyradio: new FormControl(''),
      searchvalue: new FormControl(''),
    })
    let headers = new HttpHeaders({
      'Authkey': 'MWQyMmUzNWY4YjhlNjY2NWJjM2EzZjY0NjNhZWM0ZTk=',
      'Authtoken': this.auth.isLoggedIn(),
      "Content-Type": "application/json"
    })

    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "first": 'GHF', "last": "GHFH"
        },

        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      "lengthMenu": [[100, 200,300, -1], [100, 200,300, "All"]],
      serverSide: true,
      processing: true,
      columnDefs: [
        {
          "targets": [1, 2, 3, 4, 5, 6,7, 8, 9, 10, 11, 12, 13,14,15,16,17,18,19,20,21,22,23,24,25],
          "orderable": false,
        },
      ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.viewtbl) {
          that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.cashdepositStmt, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
            if (res.statuscode == 200) {
              that.cashdepositst = res.data;
              this.viewtable = true;
              this.IsDownloadDisabled = res['data'].length > 0 ? false : true;
              callback({
                recordsTotal: res.recordsTotal,
                recordsFiltered: res.recordsFiltered,
                data: []
              });
            }
          });
        }
      },
      //columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
    };
    this.usertype = this.auth.Getsessiondata().usertype;

  }

  radiocheck(event: any) {
    if (event.target.value) {
      this.form.get('searchvalue')?.enable();
    }
  }
  ngOnDestroy(): void {
    // We remove the last function in the global ext search array so we do not add the fn each time the component is drawn
    // /!\ This is not the ideal solution as other components may add other search function in this array, so be careful when
    // handling this global variable
    $.fn['dataTable'].ext.search.pop();
  }

  OnCallApesCashWithdrawal(): void {
    this.viewtbl = true;
    let startdate = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate = this.transform(this.form.get('selectdate')?.value[1]);
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': startdate,
      'enddate': enddate,
      'searchby': this.form.get('searchbyradio')?.value,
      'serachvalue': this.form.get('searchvalue')?.value,
      'status': this.form.get('status')?.value,
      'userby': (this.prevVal === null ? '' : this.prevVal)
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  transform(date: any) {
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }
  getServerResponse(val: any) {
    if (val != this.prevVal) {
      this.prevVal = val;
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this.auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        if (res['data'] == undefined) {
          this.userAutoComData = [];
        } else {
          this.userAutoComData = res['data'];
        }
      });
    }

  }
  selectEvent(item: any) {
    // do something with selected item
    //console.log(item);
    this.prevVal = item.userid
  }
  searchCleared() {
    //console.log('searchCleared');
    this.prevVal = null;
  }
  getCashDepositDetails(data: any, id: any) {
    this.modal.open(data);
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('id', id);
    this.auth.postdata(formdata, config.cashdepositedetails).subscribe((response: any) => {
      this._cashdepositrefid = response.data.cashdepositrefid;
      this._commission = response.data.commission;
      this._profit = response.data.profit;
      this._ackno = response.data.ackno;
      this._lat = response.data.lat;
      this._longt = response.data.longt;
      this._mobile = response.data.mobile;
      this._accountno = response.data.accountno;
      this._ipaddress = response.data.ipaddress;
      this._tds = response.data.tds;
      this._bankiiin = response.data.bankiiin;
      this.adddate = response.data.dateadded;
    });
  }
  get f() {
    return this.form.controls
  }
  onDateRangePickerShow() {
    // This is a workaround to show previous month
    var prevMonth = new Date();
    prevMonth.setMonth(prevMonth.getMonth() - 1);
    this.rangePicker._datepicker.instance.monthSelectHandler({ date: prevMonth });
  }
  download($event:any) {
    const startdate = this.transform(this.form.get('selectdate')?.value[0]);
    const enddate = this.transform(this.form.get('selectdate')?.value[1]);
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('startdate', (startdate === null ? '' : startdate));
    formdata.append('enddate', (startdate === null ? '' : enddate));
    formdata.append('searchby', this.form.get('searchbyradio')?.value);
    formdata.append('serachvalue', this.form.get('serachvalue')?.value);
    formdata.append('status', this.form.get('status')?.value);
    formdata.append('userby', (this.prevVal === null ? '' : this.prevVal));
    this.auth.postdata(formdata, config.downloadcashdepositStmt).subscribe((res: any) => {
      if (res.statuscode == 200) {
	  const fileName = 'Cash-deposite.xlsx';
		const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res['data']);
		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, 'Cash-deposite');
		XLSX.writeFile(wb, fileName);
  }else{
    Swal.fire({
      icon: 'error',
      title: res.message 
    }) 
  }
});
  }
}
