import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BillPaymentStatementComponent } from './bill-payment-statement.component';

describe('BillPaymentStatementComponent', () => {
  let component: BillPaymentStatementComponent;
  let fixture: ComponentFixture<BillPaymentStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BillPaymentStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BillPaymentStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
