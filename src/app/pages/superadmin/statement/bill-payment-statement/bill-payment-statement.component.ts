import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { environment } from 'src/environments/environment';
import * as XLSX from 'xlsx';
import { CustConfg } from 'src/app/_helpers/common/custom-datepicker/ngx-datePicker-CustConfg';
import Swal from 'sweetalert2';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-bill-payment-statement',
  templateUrl: './bill-payment-statement.component.html',
  styleUrls: ['./bill-payment-statement.component.css']
})
export class BillPaymentStatementComponent implements OnInit {
  @ViewChild('rangePicker') rangePicker:any;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  form: FormGroup = new FormGroup({});
  minDate: Date;
  maxDate: Date;
  fromDt: any = null;
  toDt: any = null;
  morefilter: boolean = false;
  viewtable: boolean = false;
  public viewtb: boolean = false;
  details: any;
  billpayment: any;
  formdata: any;
  userAutoComData: any;
  userKeyword = 'userdetails';
  usertype: any;
  prevVal: any;
  bsCustConfg = CustConfg;
  IsDownloadDisabled: boolean = true;
  constructor(private http: HttpClient, private _auth: ApiService, private datePipe: DatePipe) {
    const DateValue = new Date();
    this.minDate = new Date(2017, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate())

  }
  get f() {
    return this.form.controls
  }
  ngOnInit(): void {
    this.form = new FormGroup({
      selectdate: new FormControl([new Date(),new Date()], [Validators.required]),
      searchbyradio: new FormControl(''),
      searchvalue: new FormControl(''),
      status: new FormControl(''),
      frombank: new FormControl('')
    });

    this.usertype = this._auth.Getsessiondata().usertype;

  }
  ngDoCheck() {
    let headers = new HttpHeaders({
      'Authkey': 'MWQyMmUzNWY4YjhlNjY2NWJjM2EzZjY0NjNhZWM0ZTk=',
      'Authtoken': this._auth.isLoggedIn(),
      "Content-Type": "application/json"
    })

    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "first": 'GHF', "last": "GHFH"
        },

        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
      serverSide: true,
      processing: false,
      columnDefs: [
        {
          "targets": [2, 3, 4, 5, 7, 8, 9, 10, 11],
          "orderable": false,
        },
      ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.viewtb) {
          that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.billpaymentst, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
            if (res.statuscode == 200) {
              ////console.log(res.data);
              that.billpayment = res.data;
              this.viewtable = true;
              this.IsDownloadDisabled = res['data'].length > 0 ? false : true;
              ////console.log(res.data);
              callback({
                recordsTotal: res.recordsTotal,
                recordsFiltered: res.recordsFiltered,
                data: []
              });
            }
          });
        }
      },
      //columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
    };
  }

  radiocheck(event: any) {
    if (event.target.value) {
      this.form.get('searchvalue')?.enable();
    } else {
      this.form.get('searchvalue')?.disable();
    }
  }
  ngOnDestroy(): void {
    $.fn['dataTable'].ext.search.pop();
  }
  getServerResponse(val: any) {
    if (val != this.prevVal) {
      this.prevVal = val;
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        if (res['data'] == undefined) {
          this.userAutoComData = [];
        } else {
          this.userAutoComData = res['data'];
        }
      });
    }

  }
  selectEvent(item: any) {
    // do something with selected item
    //console.log(item);
    this.prevVal = item.userid
  }
  searchCleared() {
    //console.log('searchCleared');
    this.prevVal = null;
  }
  filterAepsSettlement() {
    this.viewtb = true; 
    let startdate = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate = this.transform(this.form.get('selectdate')?.value[1]);
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': (startdate === null ? '' : startdate),
      'enddate': (enddate === null ? '' : enddate),
      'searchby': this.form.get('searchbyradio')?.value,
      'serachvalue': this.form.get('searchvalue')?.value,
      'status': this.form.get('status')?.value,
      'api': this.form.get('frombank')?.value,
      'userby': (this.prevVal === null ? '' : this.prevVal)
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  download($event: any) {
    let startdate = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate = this.transform(this.form.get('selectdate')?.value[1]);
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('startdate', (startdate === null ? '' : startdate));
    formdata.append('enddate', (startdate === null ? '' : enddate));
    formdata.append('searchby', this.form.get('searchbyradio')?.value);
    formdata.append('serachvalue', this.form.get('serachvalue')?.value);
    formdata.append('status', this.form.get('status')?.value);
    formdata.append('userby', (this.prevVal === null ? '' : this.prevVal));
    this._auth.postdata(formdata, config.downloadbillpaymentst).subscribe((res: any) => {
      if (res.statuscode == 200) {
        const fileName = 'Bill-Payment-Statement.xlsx';
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res['data']);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Bill-Payment-Statement');
        XLSX.writeFile(wb, fileName);
      }else{
        Swal.fire({
          icon: 'error',
          title: res.message 
        }) 
      }
    });
  }
  transform(date: any) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  onDateRangePickerShow() {
    // This is a workaround to show previous month
    var prevMonth = new Date();
    prevMonth.setMonth(prevMonth.getMonth() - 1);
    this.rangePicker._datepicker.instance.monthSelectHandler({ date: prevMonth });
  }
}
