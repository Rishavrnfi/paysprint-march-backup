import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
@Component({
  selector: 'app-hold',
  templateUrl: './hold.component.html',
  styleUrls: ['./hold.component.css']
})
export class HoldComponent implements OnInit {
  tsfBank: any;
  constructor(private _auth: ApiService) { }
  ngOnInit(): void {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('status', '4');
    this._auth.postdata(formdata, config.dmtCheck).subscribe((res: any) => {
      this.tsfBank = res.data;
    });
  }


}
