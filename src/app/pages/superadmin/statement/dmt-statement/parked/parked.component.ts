import { Component, OnInit } from '@angular/core'; 
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config'; 

@Component({
  selector: 'app-parked',
  templateUrl: './parked.component.html',
  styleUrls: ['./parked.component.css']
})
export class ParkedComponent implements OnInit {  
  parkedList:any; 
  constructor(private _auth: ApiService) {
   }

  ngOnInit(): void { 
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('status','2');
    this._auth.postdata(formdata,config.dmtCheck).subscribe((res: any) => {  
      this.parkedList = res.data;   
      console.log(res.data);
      
    }); 
  }

}
