import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params, RoutesRecognized } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { LoaderService } from 'src/app/_helpers/common/loader.service';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent implements OnInit {
  sno: any; 
  myTextarea:any;
  queryList:any;
 
  constructor(private route: ActivatedRoute,  private _auth: ApiService,private loader: LoaderService) { 
    this.loader.loaderEvent.emit(true);
    this.route.params.subscribe( params =>  
      this.sno = params.id
      );  
  }

  ngOnInit(): void {
    var that = this;
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('id',this.sno);
      this._auth.postdata(formdata,config.QueryDmt).subscribe((res: any) => {  
        this.queryList = res.data; 
        this.myTextarea = JSON.stringify(res.bank, undefined, 4);
        this.loader.loaderEvent.emit(false);
      }); 
      
       
  }

}
