import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';

@Component({
  selector: 'app-transfer-to-bank',
  templateUrl: './transfer-to-bank.component.html',
  styleUrls: ['./transfer-to-bank.component.css']
})
export class TransferToBankComponent implements OnInit {
 tsfBank:any;
  constructor(private _auth: ApiService) { }

  ngOnInit(): void {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('status','3');
    this._auth.postdata(formdata,config.dmtCheck).subscribe((res: any) => {  
      this.tsfBank = res.data;  
      
    }); 
  }

}
