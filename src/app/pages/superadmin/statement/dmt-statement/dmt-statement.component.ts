import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';  
import { EncodeDecode } from 'src/app/_helpers/encode-decode';
import { environment } from 'src/environments/environment.prod';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { CustConfg } from 'src/app/_helpers/common/custom-datepicker/ngx-datePicker-CustConfg';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-dmt-statement',
  templateUrl: './dmt-statement.component.html',
  styleUrls: ['./dmt-statement.component.css']
})
export class DmtStatementComponent implements OnInit {
  @ViewChild('rangePicker') rangePicker:any;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  minDate!: Date;
  maxDate!: Date;
  details: any;
  username: any;
  dtOptions: DataTables.Settings = {};
  dmt_stmt: any;
  userAutoComData: any;
  userKeyword = 'userdetails';
  refund :any = FormGroup; 
  public morefilter: boolean = false;
  public viewtable: boolean = false;
  public viewtb: boolean = false;
  public txtbox: boolean = false;
  min: any = '';
  max: any = '';
  formdata: any;
  form: FormGroup = new FormGroup({});
  usertype: any;
  prevVal: any;
  targetData:any=[];  
  IsDownloadDisabled: boolean = true;
  bsCustConfg = CustConfg;
  refundid:any;
  constructor(
    private http: HttpClient,
    private auth: ApiService,
    private datepipe: DatePipe,
    private modal: CustomeModalService
  ){
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': this.min,
      'enddate': this.max,
      'searchby': '',
      'serachvalue': '',
      'status': '',
      'transfertype': '',
      'frombank':'',
    }
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate());
  }
  ngOnInit(): void {
    this.refund = new FormGroup({ 
      authcode: new FormControl('',[Validators.required,Validators.pattern(/^[0-9]\d*$/)]), 
    });
    
    const encode: any = EncodeDecode('n', localStorage.getItem('LoginDetails'));
    const _permissionlist: any = JSON.parse(encode);
    this.details = _permissionlist.usertype;
    this.username = _permissionlist.username;
    // //console.log(this.details);
    this.form = new FormGroup({
      selectdate: new FormControl([new Date(),new Date()], [Validators.required]), 
      status: new FormControl(''),
      transfertype: new FormControl(''),
      searchbyradio: new FormControl(''),
      searchvalue: new FormControl(''),
      frombank: new FormControl('')
      
    })
    let headers = new HttpHeaders({
      'Authkey': 'MWQyMmUzNWY4YjhlNjY2NWJjM2EzZjY0NjNhZWM0ZTk=',
      'Authtoken': this.auth.isLoggedIn(),
      "Content-Type": "application/json"
    })
    const that = this;
    let options = { headers: headers };
   
    this.usertype = this.auth.Getsessiondata().usertype;
    if (this.usertype == 0) {
      this.dtOptions = {
        pagingType: 'full_numbers',
        language: {
          paginate: {
            "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
            "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
            "first": 'GHF', "last": "GHFH"
          },
  
          search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
          searchPlaceholder: "Search...",
          lengthMenu: "Results :  _MENU_",
        },
        "lengthMenu": [[100, 200,300, -1], [100, 200,300, "All"]],
        serverSide: true,
        processing: true,
        // autoWidth: false,
        columnDefs: [
          {
            
            "targets":[1, 2, 3, 4, 5, 6,7, 8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20,21,22,23],
            "orderable": false,
          },
          // { "width": "20%", "targets": 3 },
          // { "width": "5%", "targets": 5 },
          { "width": "5%", "targets": 2 },
          // { "width": "5%", "targets": 14 },
        ],
        ajax: (dataTablesParameters: any, callback) => {
          if (this.viewtb) {
            that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.dmt, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
              if (res.statuscode == 200) {
                that.dmt_stmt = res.data;
                this.viewtable = true;
                  this.IsDownloadDisabled = res['data'].length > 0 ? false : true;
                callback({
                  recordsTotal: res.recordsTotal,
                  recordsFiltered: res.recordsFiltered,
                  data: []
                });
              } else {
                Swal.fire({
                  icon: 'error',
                  title: res.message
                })
              }
            });
          }
        },
        //columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
      };
    }else{
      this.dtOptions = {
        pagingType: 'full_numbers',
        language: {
          paginate: {
            "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
            "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
            "first": 'GHF', "last": "GHFH"
          },
  
          search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
          searchPlaceholder: "Search...",
          lengthMenu: "Results :  _MENU_",
        },
        "lengthMenu": [[100, 200,300, -1], [100, 200,300, "All"]],
        serverSide: true,
        processing: true,
        // autoWidth: false,
        columnDefs: [
          {
            
            "targets":[1, 2, 3, 4, 5, 6,7,8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20,21,22],
            "orderable": false,
          },
          // { "width": "20%", "targets": 3 },
          // { "width": "5%", "targets": 5 },
          { "width": "5%", "targets": 2 },
          // { "width": "5%", "targets": 14 },
        ],
        ajax: (dataTablesParameters: any, callback) => {
          if (this.viewtb) {
            that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.dmt, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
              if (res.statuscode == 200) {
                that.dmt_stmt = res.data;
                this.viewtable = true;
                  this.IsDownloadDisabled = res['data'].length > 0 ? false : true;
                callback({
                  recordsTotal: res.recordsTotal,
                  recordsFiltered: res.recordsFiltered,
                  data: []
                });
              } else {
                Swal.fire({
                  icon: 'error',
                  title: res.message
                })
              }
            });
          }
        },
        //columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
      }; 
    }
  // console.log(this.targetData);
  }
  radiocheck(event: any) {
    if (event.target.value) {
      this.txtbox = true;
      this.form.get('searchvalue')?.enable();
    }
  }
  ngOnDestroy(): void {
    // We remove the last function in the global ext search array so we do not add the fn each time the component is drawn
    // /!\ This is not the ideal solution as other components may add other search function in this array, so be careful when
    // handling this global variable
    $.fn['dataTable'].ext.search.pop();
  }
  dmtSearch(): void {
    this.viewtb = true;
    let startdate = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate = this.transform(this.form.get('selectdate')?.value[1]);
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': (startdate === null ? '' : startdate),
      'enddate': (enddate === null ? '' : enddate),
      'searchby': this.form.get('searchbyradio')?.value,
      'serachvalue': this.form.get('searchvalue')?.value,
      'status': this.form.get('status')?.value,
      'transfertype': this.form.get('transfertype')?.value,
      'userby': (this.prevVal === null ? '' : this.prevVal),
      'frombank': this.form.get('frombank')?.value,
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  } 
  transform(date: any) {
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }
  getServerResponse(val: any) {
    if (val != this.prevVal) {
      this.prevVal = val;
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this.auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        if (res['data'] == undefined) {
          this.userAutoComData = [];
        } else {
          this.userAutoComData = res['data'];
        }
      });
    }
  }
  selectEvent(item: any) {
    // do something with selected item
    //console.log(item);
    this.prevVal = item.userid
  }
  searchCleared() {
    //console.log('searchCleared');
    this.prevVal = null;
  }
  get f() {
    return this.form.controls
  }
  get r() {
    return this.refund.controls
  }
  showrefund(data: any,id:any){
    let modalSize = 'md'; 
    this.modal.open(data, modalSize);
    this.refundid=id;
    }

    request_refund(){ 
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth); 
    formdata.append('id',this.refundid);  
    formdata.append('authcode', this.refund.get('authcode')?.value);
    this.auth.postdata(formdata, config.refundDmt).subscribe((res: any) => {
      if (res.response_code == 200) { 
        this.modal.close(); 
        Swal.fire({ 
          title: res.message,
          icon: 'success'
        });
        this.refund.reset(); 
      } else {
        this.refund.reset(); 
        Swal.fire({
          icon: 'error',
          title: res.message  
        }) 
      }
    }) 
    }
  download($event:any) { 
    let startdate = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate = this.transform(this.form.get('selectdate')?.value[1]);
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('startdate', (startdate === null ? '' : startdate));
    formdata.append('enddate', (startdate === null ? '' : enddate));
    formdata.append('searchby', this.form.get('searchbyradio')?.value);
    formdata.append('serachvalue', this.form.get('serachvalue')?.value);
    formdata.append('status', this.form.get('status')?.value);
    formdata.append('userby', (this.prevVal === null ? '' : this.prevVal));
    this.auth.postdata(formdata, config.downloaddmtstmt).subscribe((res: any) => {
      if (res.statuscode == 200) {
        const fileName = 'DMT-Statement.xlsx';
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res['data']);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'DMT-Statement');
        XLSX.writeFile(wb, fileName);
      }else{
        Swal.fire({
          icon: 'error',
          title: res.message 
        }) 
      }
    });
  }
  onDateRangePickerShow() {
    // This is a workaround to show previous month
    var prevMonth = new Date();
    prevMonth.setMonth(prevMonth.getMonth() - 1);
    this.rangePicker._datepicker.instance.monthSelectHandler({ date: prevMonth });
  }
}
