import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DmtStatementComponent } from './dmt-statement.component';

describe('DmtStatementComponent', () => {
  let component: DmtStatementComponent;
  let fixture: ComponentFixture<DmtStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DmtStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DmtStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
