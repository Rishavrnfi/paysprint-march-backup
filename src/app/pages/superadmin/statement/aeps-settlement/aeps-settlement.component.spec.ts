import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AepsSettlementComponent } from './aeps-settlement.component';

describe('AepsSettlementComponent', () => {
  let component: AepsSettlementComponent;
  let fixture: ComponentFixture<AepsSettlementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AepsSettlementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AepsSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
