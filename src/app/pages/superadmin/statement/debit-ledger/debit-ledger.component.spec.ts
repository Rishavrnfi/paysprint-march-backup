import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebitLedgerComponent } from './debit-ledger.component';

describe('DebitLedgerComponent', () => {
  let component: DebitLedgerComponent;
  let fixture: ComponentFixture<DebitLedgerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebitLedgerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebitLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
