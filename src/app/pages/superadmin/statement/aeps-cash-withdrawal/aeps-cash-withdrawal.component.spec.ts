import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AepsCashWithdrawalComponent } from './aeps-cash-withdrawal.component';

describe('AepsCashWithdrawalComponent', () => {
  let component: AepsCashWithdrawalComponent;
  let fixture: ComponentFixture<AepsCashWithdrawalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AepsCashWithdrawalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AepsCashWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
