import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantOnboardStatementComponent } from './merchant-onboard-statement.component';

describe('MerchantOnboardStatementComponent', () => {
  let component: MerchantOnboardStatementComponent;
  let fixture: ComponentFixture<MerchantOnboardStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MerchantOnboardStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantOnboardStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
