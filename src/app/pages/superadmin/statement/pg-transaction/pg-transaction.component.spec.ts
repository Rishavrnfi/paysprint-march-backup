import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PgTransactionComponent } from './pg-transaction.component';

describe('PgTransactionComponent', () => {
  let component: PgTransactionComponent;
  let fixture: ComponentFixture<PgTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PgTransactionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PgTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
