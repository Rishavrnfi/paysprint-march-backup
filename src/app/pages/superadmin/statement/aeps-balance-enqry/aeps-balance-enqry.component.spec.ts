import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AepsBalanceEnqryComponent } from './aeps-balance-enqry.component';

describe('AepsBalanceEnqryComponent', () => {
  let component: AepsBalanceEnqryComponent;
  let fixture: ComponentFixture<AepsBalanceEnqryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AepsBalanceEnqryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AepsBalanceEnqryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
