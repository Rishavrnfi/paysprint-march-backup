import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AepsMiniStatementComponent } from './aeps-mini-statement.component';

describe('AepsMiniStatementComponent', () => {
  let component: AepsMiniStatementComponent;
  let fixture: ComponentFixture<AepsMiniStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AepsMiniStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AepsMiniStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
