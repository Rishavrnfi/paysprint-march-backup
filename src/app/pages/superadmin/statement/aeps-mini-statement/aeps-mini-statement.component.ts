import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config'; 
import { CustConfg } from 'src/app/_helpers/common/custom-datepicker/ngx-datePicker-CustConfg';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-aeps-mini-statement',
  templateUrl: './aeps-mini-statement.component.html',
  styleUrls: ['./aeps-mini-statement.component.css']
})
export class AepsMiniStatementComponent implements OnInit {
  @ViewChild('rangePicker') rangePicker:any;
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  minDate!: Date;
  maxDate!: Date;
  details: any;
  username: any;
  dtOptions: DataTables.Settings = {};
  aeps_mini_stmt: any;
  public viewtb: boolean = false;
  public morefilter: boolean = false;
  public viewtable: boolean = false;
  min: any = '';
  max: any = '';
  formdata: any;
  form: FormGroup = new FormGroup({});


  ministmt: any;

  public accessmode: any;
  public aepsrefId: any;
  public commission: any;
  public tds: any;
  public mobile: any;
  public lat: any;
  public longt: any;
  public last_aadhar: any;
  public bankiiin: any;
  public bankName: any;
  public remarks: any;
  public ipaddress: any;
  public dateadded: any;
  public deviceimei: any;
  public devicemi: any;
  public adddate: any;
  userAutoComData: any;
  userKeyword = 'userdetails';
  usertype: any;
  prevVal: any;
  bsCustConfg = CustConfg;
  IsDownloadDisabled: boolean = false;

  constructor(
    private http: HttpClient,
    private auth: ApiService,
    private datepipe: DatePipe,
    private modal: CustomeModalService
  ) {
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate());
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      selectdate: new FormControl([new Date(),new Date()], [Validators.required]),
      // minDt: new FormControl('', [Validators.required]),
      // maxDt: new FormControl('', [Validators.required]),
      status: new FormControl(''),
      transfertype: new FormControl(''),
      searchbyradio: new FormControl(''),
      searchvalue: new FormControl(''),
    })
    let headers = new HttpHeaders({
      'Authkey': 'MWQyMmUzNWY4YjhlNjY2NWJjM2EzZjY0NjNhZWM0ZTk=',
      'Authtoken': this.auth.isLoggedIn(),
      "Content-Type": "application/json"
    })
    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "first": 'GHF', "last": "GHFH"
        },

        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
      serverSide: true,
      processing: true,
      columnDefs: [
        {
          "targets": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
          "orderable": false,
        },
      ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.viewtb) {
          that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.aepsministmt, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
            if (res.statuscode == 200) {
              that.aeps_mini_stmt = res.data;
              this.viewtable = true;
              this.IsDownloadDisabled = res['data'].length > 0 ? false : true;
              callback({
                recordsTotal: res.recordsTotal,
                recordsFiltered: res.recordsFiltered,
                data: []
              });
            }
          });
        }
      },
      //columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
    };
    this.usertype = this.auth.Getsessiondata().usertype;
  }

  radiocheck(event: any) {
    if (event.target.value) {
      this.form.get('searchvalue')?.enable();
    }
  }

  ngOnDestroy(): void {
    // We remove the last function in the global ext search array so we do not add the fn each time the component is drawn
    // /!\ This is not the ideal solution as other components may add other search function in this array, so be careful when
    // handling this global variable
    $.fn['dataTable'].ext.search.pop();
  }


  filterById(): void {
    this.viewtb = true;
    let startdate = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate = this.transform(this.form.get('selectdate')?.value[1]);
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': (startdate === null ? '' : startdate),
      'enddate': (enddate === null ? '' : enddate),
      'searchby': this.form.get('searchbyradio')?.value,
      'serachvalue': this.form.get('searchvalue')?.value,
      'status': this.form.get('status')?.value,
      'transfertype': this.form.get('transfertype')?.value,
      'userby': (this.prevVal === null ? '' : this.prevVal)
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  transform(date: any) {
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }
  getServerResponse(val: any) {
    if (val != this.prevVal) {
      this.prevVal = val;
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this.auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        if (res['data'] == undefined) {
          this.userAutoComData = [];
        } else {
          this.userAutoComData = res['data'];
        }
      });
    }

  }
  selectEvent(item: any) {
    // do something with selected item
    //console.log(item);
    this.prevVal = item.userid
  }
  searchCleared() {
    //console.log('searchCleared');
    this.prevVal = null;
  }
  get f() {
    return this.form.controls
  }
  viewdetail_Model(data: any, details: any) {
    ////console.log(details);
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('id', details.id);
    this.auth.postdata(formdata, config.getstmtDetails).subscribe((res: any) => {
      //this.ministmt = res.data;  
      this.modal.open(data);
      this.aepsrefId = res.data.aepsrefId;
      this.accessmode = res.data.accessmode;
      this.commission = res.data.commission;
      this.tds = res.data.tds;
      this.mobile = res.data.mobile;
      this.lat = res.data.lat;
      this.longt = res.data.longt;
      this.last_aadhar = res.data.last_aadhar;
      this.bankiiin = res.data.bankiiin;
      this.bankName = res.data.bankName;
      this.remarks = res.data.remarks;
      this.ipaddress = res.data.ipaddress;
      this.dateadded = res.data.dateadded;
      this.deviceimei = res.data.deviceimei;
      this.devicemi = res.data.devicemi;
      this.adddate = res.data.dateadded;


      // this.editStaff.controls["gender"].setValue(res.data.gender);
      // this.editStaff.controls["activestatus"].setValue(res.data.status);
    })
  }
  onDateRangePickerShow() {
    // This is a workaround to show previous month
    var prevMonth = new Date();
    prevMonth.setMonth(prevMonth.getMonth() - 1);
    this.rangePicker._datepicker.instance.monthSelectHandler({ date: prevMonth });
  }
  download($event:any) {
    const startdate = this.transform(this.form.get('selectdate')?.value[0]);
    const enddate = this.transform(this.form.get('selectdate')?.value[1]);
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('startdate', (startdate === null ? '' : startdate));
    formdata.append('enddate', (startdate === null ? '' : enddate));
    formdata.append('searchby', this.form.get('searchbyradio')?.value);
    formdata.append('serachvalue', this.form.get('serachvalue')?.value);
    formdata.append('status', this.form.get('status')?.value);
    formdata.append('userby', (this.prevVal === null ? '' : this.prevVal));
    this.auth.postdata(formdata, config.downloadaepsministmt).subscribe((res: any) => {
      if (res.statuscode == 200) {
	  const fileName = 'Aeps-Mini-Stmt.xlsx';
		const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(res['data']);
		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, 'Aeps-Mini-Stmt');
		XLSX.writeFile(wb, fileName);
  }else{
    Swal.fire({
      icon: 'error',
      title: res.message 
    }) 
  }
});
  }
}
