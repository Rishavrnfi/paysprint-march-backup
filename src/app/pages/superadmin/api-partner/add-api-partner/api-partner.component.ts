import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-api-partner',
  templateUrl: './api-partner.component.html',
  styleUrls: ['./api-partner.component.css']
})
export class ApiPartnerComponent implements OnInit {
  state :any;
  values = '';
  apiPartner :any = FormGroup;
  public isSubmit:boolean=false;
  userid :any;
  activestatus:any; 
  minDate!: Date;
  maxDate!: Date;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route:Router,
    private datepipe:DatePipe
  ) {
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate()); 
    this.apiPartner = this.fb.group({
      name: ['', [Validators.required]],
      firmname :['',[Validators.required]],
      dob        :['',[Validators.required]],  
      email      :['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo    :['',[Validators.required,Validators.minLength(10),Validators.pattern('[6789][0-9]{9}')]],  
      pannumber   :['',[Validators.required,Validators.minLength(10), Validators.pattern('[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}')]],
      gstnumber   :['',[Validators.required,Validators.minLength(15),Validators.pattern('^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$')]], 
      pincode :['',[Validators.required,Validators.minLength(6),Validators.pattern('[1-9][0-9]{5}')]],
      state :['',[Validators.required]],
      address :['',[Validators.required]],
      activestatus :['',[Validators.required]],
    });
    //^(\d)(?!\1+$)\d{5}$
  }

  ngOnInit(): void {
  }

  transform(date:any){
    return this.datepipe.transform(date,'dd-MM-yyyy');
  }
  OnaddapiPartner(){
    if (!this.apiPartner.valid){
      return;
    }else{
      const date:any = this.transform(this.apiPartner.get('dob').value);
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name',this.apiPartner.get('name').value);
      formdata.append('firmname',this.apiPartner.get('firmname').value);
      formdata.append('dob',date); 
      formdata.append('email',this.apiPartner.get('email').value);
      formdata.append('phone',this.apiPartner.get('phoneNo').value);  
      formdata.append('pannumber',this.apiPartner.get('pannumber').value);
      formdata.append('gstnumber',this.apiPartner.get('gstnumber').value); 
      formdata.append('status',this.apiPartner.get('activestatus').value);
      formdata.append('state',this.apiPartner.get('state').value); 
      formdata.append('pincode',this.apiPartner.get('pincode').value); 
      formdata.append('address',this.apiPartner.get('address').value); 

      this._auth.postdata(formdata,config.apipartner).subscribe((res: any) => { 
        if (res.statuscode == 200) { 
          this.userid= res.userid; 
          Swal.fire({
            title: res.message, 
            icon: 'success'
          });
              this.apiPartner.reset(); 
        }else{
          Swal.fire({
            icon: 'error',
            title: res.message
          }) 
        }
      })
    }
  }
  getpincode(event: any){ 
    this.values = event.target.value;
    if (this.values.length == 6) {
      const formdata = new FormData(); 
      formdata.append('pincode',this.values);
      this._auth.postState(formdata).subscribe((res: any) => { 
        this.state =res.result.state;
        ////console.log(res.result.state)
        // this.apiPartner.setValue({
        //   "state" : res.result.state
        // });

      })
    } 
  }
  get f() { return this.apiPartner.controls; }
}
