import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiPartnerComponent } from './api-partner.component';

describe('ApiPartnerComponent', () => {
  let component: ApiPartnerComponent;
  let fixture: ComponentFixture<ApiPartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApiPartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
