import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewApiPartnerComponent } from './view-api-partner.component';

describe('ViewApiPartnerComponent', () => {
  let component: ViewApiPartnerComponent;
  let fixture: ComponentFixture<ViewApiPartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewApiPartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewApiPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
