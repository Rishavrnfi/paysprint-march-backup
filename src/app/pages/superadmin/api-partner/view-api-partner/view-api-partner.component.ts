import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { from, Subject } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2'; 
import { DataTableDirective } from 'angular-datatables';
import{ DatatableServiceService } from 'src/app/_helpers/common/custome-datatable/datatable-service.service';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
declare let $: any;
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-view-api-partner',
  templateUrl: './view-api-partner.component.html',
  styleUrls: ['./view-api-partner.component.css']
})
export class ViewApiPartnerComponent implements OnInit {
  state :any;
  formdata:any;
  values = '';
  apiPartnerList: any;
  editApiAccount: any = FormGroup;
  private apiPartnerID: any;
  activestatus: any;
  gender: any;
  remark: any;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: any =  DataTableDirective;
  dtOptions: DataTables.Settings = {}; 
  isDtInitialized:boolean = false; 
  minDate!: Date;
  maxDate!: Date;
  usertype:any;
  dtTrigger:any =  new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal: CustomeModalService, 
    private datat: DatatableServiceService,
    private datepipe:DatePipe,
    private http: HttpClient
  ) { 
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate()); 
    this.editApiAccount = this.fb.group({
      name: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      remarks: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo: ['', [Validators.required, Validators.pattern('[6789][0-9]{9}')]],
      firmname: ['', [Validators.required]],
      pannumber: ['', [Validators.required]],
      gstnumber: ['', [Validators.required]],
      pincode :['',[Validators.required,Validators.minLength(6),Validators.pattern('[1-9][0-9]{5}')]],
      state :['',[Validators.required]],
      address :['',[Validators.required]],
      activestatus: ['', [Validators.required]]
    });
    this.formdata = {
      'token': config.tokenauth,
    }
  }

  ngOnInit(): void {
    let headers = new HttpHeaders({
      'Authkey': config.Authkey,
      'Authtoken': this._auth.isLoggedIn(),
      "Content-Type": "application/json"
    }) 
    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      //"lengthMenu": [[10, 20,30, -1], [10, 20,30, "All"]], 
      "lengthMenu": [[100, 200,300, -1], [100, 200,300, "All"]], 
      language: {
        paginate: {
          "first": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "last": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
        },

        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      }, 
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.apiPartnerList, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
          if (res.statuscode == 200) {
            that.apiPartnerList = res.data;
            callback({
              recordsTotal: res.recordsTotal,
              recordsFiltered: res.recordsFiltered,
              data: []
            });
          }
        });
      },
    };
    this.usertype = this._auth.Getsessiondata().usertype;
  }

  editapi_Model(data: any, details: any) {
    const formdata = new FormData();
    formdata.append('userid', details.id);
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getapiPartner).subscribe((res: any) => {
      this.apiPartnerID = res.data.userid;
      this.modal.open(data);
      this.editApiAccount.reset();
      this.editApiAccount.controls["name"].setValue(res.data.name); 
      this.editApiAccount.controls["dob"].setValue(this.convertDate(res.data.dob));
      this.editApiAccount.controls["email"].setValue(res.data.email);
      this.editApiAccount.controls["phoneNo"].setValue(res.data.phone);
      this.editApiAccount.controls["firmname"].setValue(res.data.firmname);
      this.editApiAccount.controls["pannumber"].setValue(res.data.pannumber);
      this.editApiAccount.controls["gstnumber"].setValue(res.data.gstnumber);
      this.editApiAccount.controls["pincode"].setValue(res.data.pincode); 
      this.editApiAccount.controls["address"].setValue(res.data.address);  
      this.state = res.data.state;
      this.activestatus = res.data.status;
      this.remark = res.data.remarks;

    })
  }
  convertDate(getdate:any){
    if(typeof(getdate)== 'undefined' && getdate== '' && getdate== null){
      return '' ;
    }
    const myDateValue = new Date(getdate);
    return  new Date(myDateValue.getFullYear(),myDateValue.getMonth(),myDateValue.getDate())
  }
  OnupdateApiAccount() {
    if (!this.editApiAccount.valid) {
      return;
    } else {
      const formdata = new FormData();
      const dob:any = this.transform(this.editApiAccount.get('dob').value);
      formdata.append('token', config.tokenauth);
      formdata.append('name', this.editApiAccount.get('name').value); 
      formdata.append('dob', dob);  
      formdata.append('email', this.editApiAccount.get('email').value);
      formdata.append('phone', this.editApiAccount.get('phoneNo').value);
      formdata.append('firmname', this.editApiAccount.get('firmname').value);
      formdata.append('pannumber', this.editApiAccount.get('pannumber').value);
      formdata.append('gstnumber', this.editApiAccount.get('gstnumber').value);
      formdata.append('remarks', this.editApiAccount.get('remarks').value);
      formdata.append('userid', this.apiPartnerID);
      formdata.append('status', this.editApiAccount.get('activestatus').value);
      formdata.append('state',this.editApiAccount.get('state').value); 
      formdata.append('pincode',this.editApiAccount.get('pincode').value); 
      formdata.append('address',this.editApiAccount.get('address').value); 
      this._auth.postdata(formdata, config.editapipartner).subscribe((res: any) => {
        if (res.statuscode == 200) {
          this.modal.close();
          this.editApiAccount.reset(); 
          Swal.fire({
            title: 'Hurray!!',
            text: res.message,
            icon: 'success'
          });
          this.rerender();
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message,
            text: 'Something went wrong!'
          })
        }
      })
    }
  }
  transform(date:any){
    return this.datepipe.transform(date,'dd-MM-yyyy');
  }
  getpincode(event: any){ 
    this.values = event.target.value;
    if (this.values.length == 6) {
      const formdata = new FormData(); 
      formdata.append('pincode',this.values);
      this._auth.postState(formdata).subscribe((res: any) => { 
        this.state =res.result.state;
        //console.log(res.result.state)
        // this.apiPartner.setValue({
        //   "state" : res.result.state
        // });

      })
    } 
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => { 
      dtInstance.draw(); 
      this.dtTrigger.next();
    });
  }
  get f() { return this.editApiAccount.controls; }
}
