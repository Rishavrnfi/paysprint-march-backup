import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-payment-gateway',
  templateUrl: './payment-gateway.component.html',
  styleUrls: ['./payment-gateway.component.css']
})
export class PaymentGatewayComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any;

  showData: boolean = false;

  userId: any;

  mainForm: FormGroup;
  paymentTypeLst: any;


  constructor(private fb: FormBuilder, private _auth: ApiService, private _CommonService: CommonService) {
    this.mainForm = this.fb.group({
      mainArr: new FormArray([])
    });
  }
  get getMainArr() {
    // return this.mainForm.get('mainArr') as FormArray;
    return ((this.mainForm as FormGroup).controls['mainArr'] as FormArray)
  }

  getArr(arr: any, i: any): any {
    return (<FormArray>arr.controls['arr'])
  }

  ngOnInit(): void {
    this.paymentTypeLst = this._CommonService.getPaymentType();
  }

  selectEvent(event: any) {
    if (event !== undefined) {
      this.userId = event.userid;
      // //console.log(this.userId);
      let newArray: any = [];
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('userid', this.userId);
      formdata.append('type', '10');
      this._auth.postdata(formdata, config.getcommission).subscribe((res: any) => {
        if (res.statuscode === 2001 || res.statuscode === 0) {
          if (res.data === undefined) {
            Swal.fire(
              'No Record Found?',
              res.message,
              'info'
            )
            this.createIC(false);
          } else {
            this.data = JSON.parse(res.data);
            // //console.log(this.data);
            this.createIC(true);
            this.showData = true;
          }
        }
      });
    }
  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }

  createIC(valType: boolean) {
    this.getMainArr.clear();

    let noDataObj: any = {
      "RAZOR": {
        "DC": '',
        "CC": '',
        "NB": ''
      },
      "CASHFREE": {
        "DC": '',
        "CC": '',
        "NB": ''
      }
    };

    let mainObj = valType === true ? this.data : noDataObj;
    // //console.log(mainObj);

    for (const iterat in mainObj) {
      let ele = mainObj[iterat];
      let newUsergroup: FormArray = this.fb.array([]);
      let mainUsergroup: FormGroup = this.fb.group({
        name: [iterat],
        arr: newUsergroup
      });
      for (const key in ele) {
        const element = ele[key];
        newUsergroup.push(this.fb.group({
          name: [key, []],
          value: [element, [Validators.required, Validators.min(0)]],
        }));

      }
      this.getMainArr.push(mainUsergroup)
    }

  }

  getPayType(val: any) {

    let tp = [];
    tp = this._CommonService.getPaymentType();

    let sd: any = tp.find(f => f.name == val);
    // //console.log(sd);
    return sd.value;
  }

  onSubmit() {
    // //console.log(this.getMainArr);
    // //console.log(this.getMainArr.value);
    let formVal: any = this.getMainArr.value;
    let obj: any = {};

    formVal.map((val: any) => {
      let objDt: any = {};

      val.arr.map((m: any) => {
        objDt[m.name] = m.value
      })
      obj[val.name] = objDt;
    })
    // //console.log(obj);

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('type', '10');
    formdata.append('userid', this.userId);
    formdata.append('commission', JSON.stringify(obj));
    this._auth.postdata(formdata, config.updatecommission).subscribe((res: any) => {
      // //console.log(res.data);
      if (res.statuscode === 200) {
        Swal.fire(
          'Commission Updated.',
          res.message,
          'success'
        )
      }
    });
  }
}
