import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-paytmcom',
  templateUrl: './paytmcom.component.html',
  styleUrls: ['./paytmcom.component.css']
})
export class PaytmcomComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any;

  showData: boolean = false;
  userId: any;

  mainForm: FormGroup;

  constructor(private _auth: ApiService) {
    this.mainForm = new FormGroup({
      value: new FormControl(null, [Validators.required, Validators.min(0)])
    });
  }
  get getVal() {
    return this.mainForm.controls['value'] as FormControl;
  }

  ngOnInit(): void { }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  selectEvent(event: any) {
    this.showData = false;

    if (event !== undefined) {
      this.userId = event.userid;
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('userid', this.userId);
      formdata.append('type', '3');
      this._auth.postdata(formdata, config.getcommission).subscribe((res: any) => {
        if (res.statuscode === 2001 || res.statuscode === 0) {
          this.showData = true;
          if (res.data === undefined) {
            Swal.fire(
              'No Record Found?',
              res.message,
              'info'
            )
            this.createForm(false);
          } else {
            this.data = JSON.parse(res.data);

            this.createForm(true);
          }
        }
      });
    }
  }
  createForm(val: boolean) {

    if (val) {
      this.getVal.patchValue(this.data['value'])
    } else {
      this.getVal.patchValue('')
    }
  }
  onSubmit() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('type', '3');
    formdata.append('userid', this.userId);
    formdata.append('commission', JSON.stringify(this.mainForm.value));
    this._auth.postdata(formdata, config.updatecommission).subscribe((res: any) => {
      if (res.statuscode === 200) {
        Swal.fire(
          'Commission Updated.',
          res.message,
          'success'
        )
      }
    });
  }
}
