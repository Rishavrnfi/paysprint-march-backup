import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';

@Component({
  selector: 'app-other-comm',
  templateUrl: './other-comm.component.html',
  styleUrls: ['./other-comm.component.css']
})
export class OtherCommComponent implements OnInit {

  userListDts: any;

  constructor(private CommonService: CommonService) { }

  ngOnInit(): void {
    // this.CommonService.setUserListLSFn();
  }

}
