import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherCommComponent } from './other-comm.component';

describe('OtherCommComponent', () => {
  let component: OtherCommComponent;
  let fixture: ComponentFixture<OtherCommComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherCommComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherCommComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
