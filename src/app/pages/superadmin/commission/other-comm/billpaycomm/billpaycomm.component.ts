import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng-select2';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-billpaycomm',
  templateUrl: './billpaycomm.component.html',
  styleUrls: ['./billpaycomm.component.css']
})
export class BillpaycommComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any;


  showData: boolean = false;
  // data: any;
  userId: any;

  mainForm: FormGroup;

  constructor(private _auth: ApiService, private fb: FormBuilder, private _CommonService: CommonService) {
    this.mainForm = this.fb.group({
      mainArr: new FormArray([])
    });
  }
  get getMainArr() {
    // return this.mainForm.get('mainArr') as FormArray;
    return ((this.mainForm as FormGroup).controls['mainArr'] as FormArray)
  }
  getArr(arr: any, i: any): any {
    return (<FormArray>arr.controls['arr'])
  }
  ngOnInit(): void {
  }

  selectEvent(event: any) {
    // do something with selected item
    this.showData = false;

    if (event !== undefined) {
      this.userId = event.userid;
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('userid', this.userId);
      formdata.append('type', '1');
      this._auth.postdata(formdata, config.getcommission).subscribe((res: any) => {
        if (res.statuscode === 2001 || res.statuscode === 0) {
          this.showData = true;
          if (res.data === undefined) {
            Swal.fire(
              'No Record Found?',
              res.message,
              'info'
            )
            this.createForm(false);
          } else {
            this.data = JSON.parse(res.data);
            //console.log(JSON.stringify(this.data));

            this.createForm(true);
          }
        }
      });
    }
  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }

  createForm(valType: boolean) {
    this.getMainArr.clear();

    let noDataObj: any = {

      "mobi": {
        "value": ""
      },
      "ss": {
        "value": ""
      },
      "twd": {
        "value": ""
      }
    };

    let mainObj = valType === true ? this.data : noDataObj;
    // //console.log(mainObj);

    for (const iterat in mainObj) {
      let ele = mainObj[iterat];
      let newUsergroup: FormArray = this.fb.array([]);
      let mainUsergroup: FormGroup = this.fb.group({
        name: [iterat],
        arr: newUsergroup
      });
      for (const key in ele) {
        const element = ele[key];
        newUsergroup.push(this.fb.group({
          name: [key, []],
          value: [element, [Validators.required, Validators.min(0)]],
        }));

      }
      this.getMainArr.push(mainUsergroup)
    }
  }
  onSubmit() {
    let formVal: any = this.getMainArr.value;
    let obj: any = {};

    formVal.map((val: any) => {
      let objDt: any = {};

      val.arr.map((m: any) => {
        objDt[m.name] = m.value
      })
      obj[val.name] = objDt;
    })

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('type', '1');
    formdata.append('userid', this.userId);
    formdata.append('commission', JSON.stringify(obj));
    this._auth.postdata(formdata, config.updatecommission).subscribe((res: any) => {
      if (res.statuscode === 200) {
        Swal.fire(
          'Commission Updated.',
          res.message,
          'success'
        )
      }
    });
  }
}