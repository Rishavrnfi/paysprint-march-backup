import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommRechargeComponent } from './comm-recharge.component';

describe('CommRechargeComponent', () => {
  let component: CommRechargeComponent;
  let fixture: ComponentFixture<CommRechargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommRechargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommRechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
