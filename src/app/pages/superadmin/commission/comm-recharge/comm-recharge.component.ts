import { Component, OnDestroy, OnInit, } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-comm-recharge',
  templateUrl: './comm-recharge.component.html',
  styleUrls: ['./comm-recharge.component.css']
})
export class CommRechargeComponent implements OnInit, OnDestroy {
  keyword = 'userdetails';
  userDtLst: any;
  data: any;


  showData: boolean = false;

  public formData: FormGroup;
  // formArr = new FormArray([]);
  userId: any;
  oprLst: any;
  haveData!: boolean;

  callEvent: any;

  constructor(private fb: FormBuilder, private _auth: ApiService) {
    this.formData = this.fb.group({
      mainGroup: new FormArray([]),
    });
  }
  get getMain() {
    // return this.formData.get('mainGroup') as FormArray;
    return ((this.formData as FormGroup).controls['mainGroup'] as FormArray)

  }

  getArr(arr: any, i: any): any {
    return (<FormArray>arr.controls['arr'])
  }
  ngOnInit(): void {
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getOprtRechLst).subscribe((res: any) => {
      this.oprLst = res.data;
      // //console.log(this.oprLst);

    });
  }
  // setVal(event: any) {
  //   const charCode = (event.which) ? event.which : event.keyCode;
  //   if ((charCode == 38 || charCode == 40) || (charCode > 47 && charCode < 58) || (charCode > 95 && charCode < 106)) {
  //     this.getMain.controls.forEach((element: any) => {
  //       element.controls['value'].patchValue(event.target.value)
  //     });
  //   }
  // }


  selectEvent(event: any) {
    // do something with selected item
    this.callEvent = event;
    this.showData = false;
    this.haveData = false;



    if (event !== undefined) {
      this.userId = event.userid;
      // //console.log(this.userId);
      let newArray: any = [];
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('userid', this.userId);
      formdata.append('type', '4');
      this._auth.postdata(formdata, config.getcommission).subscribe((res: any) => {
        if (res.statuscode === 2001 || res.statuscode === 0) {
          this.showData = true;
          if (res.data !== undefined) {
            this.haveData = true;
            this.data = JSON.parse(res.data);
            // //console.log(this.data);

          } else {
            this.haveData = false;
          }
          this.updateLocalStorage();
        }
      });
    }
  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }

  updateLocalStorage() {
    localStorage.removeItem('oprList');

    let commDt = this.data;
    let testObj = {
      "IC": {
        "is_fixed": '0',
        "data": {
          "value": ''
        }
      },
      "JRI": {
        "is_fixed": '0',
        "data": {
          "value": ''
        }
      },
      "SS": {
        "is_fixed": '0',
        "data": {
          "value": ''
        }
      }
    }

    let localDt: any[] = [];
    let obj = this.haveData === true ? commDt : testObj;
    //console.log(obj);

    for (const key in obj) {
      // //console.log(obj[key]);
      // //console.log(obj[key]['is_fixed']);
      // //console.log(obj[key].is_fixed);
      let mainObj: any = {
        name: key,
        value: '',
        is_fixed: obj[key]['is_fixed'],
        dtArr: [],
      }
      // //console.log(mainObj);
      let fixVal = obj[key]['is_fixed'];

      mainObj.is_fixed = fixVal;
      mainObj.value = fixVal === '0' ? obj[key]['data']['value'] : '';
      let mainKey = obj[key]

      this.oprLst.map((m: any, ind: any) => {
        let dlt = { "id": m.id, "name": m.name, "value": fixVal === '1' ? mainKey.data[m.id] : '' }
        mainObj.dtArr.push(dlt);
      })
      localDt.push(mainObj);
    }
    //console.log('Local Store', localDt);

    let encode: any = EncodeDecode(JSON.stringify(localDt), 'n');
    localStorage.setItem("oprList", encode);
    this.createForm();

  }
  createForm() {

    this.getMain.clear();
    let statData: any = localStorage.getItem('oprList');
    let decode: any = EncodeDecode('n', statData);
    statData = JSON.parse(decode);
    statData.forEach((element: any) => {

      let newUsergroup = this.updateForm(element);

      let mainUsergroup: FormGroup = this.fb.group({
        name: [element.name],
        sameVal: [null],
        is_fixed: this.fb.control(element.is_fixed === '1' ? false : true),
        arr: newUsergroup
      });
      mainUsergroup.get('is_fixed')?.valueChanges.subscribe((value: boolean) => {
        let upArr: FormArray = mainUsergroup.controls['arr'] as FormArray;
        upArr.clear();
        if (value) {
          upArr.push(this.fb.group({
            id: ['value', []],
            name: ['value', []],
            value: [element.value, [Validators.required, Validators.min(0)]],
          }));
        } else {
          element.dtArr.forEach((ele: any) => {
            upArr.push(this.fb.group({
              id: [ele.id, []],
              name: [ele.name, []],
              value: [ele.value, [Validators.required, Validators.min(0)]],
            }));
          });
        }
      })

      mainUsergroup.get('sameVal')?.valueChanges.subscribe((value: any) => {
        let upArr: any = (mainUsergroup.controls['arr'] as FormArray).controls;
        upArr.forEach((element: FormGroup) => {
          element.get('value')?.patchValue(value);
        });
      })
      this.getMain.push(mainUsergroup);
      // //console.log(this.getMain);
      // //console.log(this.getMain.value);

    });
  }
  updateForm(element: any) {
    let newUsergroup = this.fb.array([]);
    if (element.is_fixed === '0') {
      newUsergroup.push(this.fb.group({
        id: ['value', []],
        name: ['value', []],
        value: [element.value, [Validators.required, Validators.min(0)]],
      }));
    } else {
      element.dtArr.forEach((ele: any) => {
        newUsergroup.push(this.fb.group({
          id: [ele.id, []],
          name: [ele.name, []],
          value: [ele.value, [Validators.required, Validators.min(0)]],
        }));
      });
    }
    return newUsergroup;
  }

  onSubmit() {

    let formValur = this.getMain.value;
    // //console.log(formValur);

    let obj: any = {};

    formValur.forEach((ele: any) => {
      let subData: any = {};
      let mainObj = {
        is_fixed: ele.is_fixed === false ? '1' : '0',
        data: subData
      }
      ele.arr.forEach((element: any) => {
        subData[element.id] = element.value;
      });
      obj[ele.name] = mainObj;
    });
    // //console.log(obj);

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('type', '4');
    formdata.append('userid', this.userId);
    formdata.append('commission', JSON.stringify(obj));
    this._auth.postdata(formdata, config.updatecommission).subscribe((res: any) => {
      // //console.log(res.data);
      if (res.statuscode === 200) {
        this.selectEvent(this.userId)

        Swal.fire(
          'Commission Updated.',
          res.message,
          'success'
        )
      }
    });
  }
  ngOnDestroy() {
    localStorage.removeItem('oprList');
  }

}
