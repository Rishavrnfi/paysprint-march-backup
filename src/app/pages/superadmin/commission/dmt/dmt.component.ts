import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dmt',
  templateUrl: './dmt.component.html',
  styleUrls: ['./dmt.component.css']
})
export class DmtComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any;

  mainForm: FormGroup;
  formArr = new FormArray([]);
  userId: any;
  constructor(private _auth: ApiService) {
    this.mainForm = new FormGroup({
      'formArr': this.formArr
    });

  }

  ngOnInit(): void {

  }

  get main() {
    return ((this.mainForm as FormGroup).get('formArr') as FormGroup);
  }

  get mainFar() {
    return ((this.mainForm as FormGroup).controls['formArr'] as FormArray).controls;
  }


  selectEvent(event: any) {
    // do something with selected item
    if (event !== undefined) {
      this.userId = event.userid;
      //console.log(this.userId);
      let newArray: any = [];
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('userid', this.userId);
      formdata.append('type', '5');
      this._auth.postdata(formdata, config.getcommission).subscribe((res: any) => {
        if (res.statuscode === 2001 || res.statuscode === 0) {
          if (res.data === undefined) {
            Swal.fire(
              'No Record Found?',
              res.message,
              'info'
            )

            // this.aepsData = this.fb.group({
            //   mainGroup: [],
            // });

            const data = {
              "fino": {
                "penny": '',
                "imps": {
                  "slot1": '',
                  "slot2": '',
                  "slot3": '',
                  "slot4": '',
                  "slot5": ''
                },
                "neft": {
                  "slot1": '',
                  "slot2": '',
                  "slot3": '',
                  "slot4": '',
                  "slot5": ''
                }
              },
              "airtel": {
                "penny": '',
                "imps": {
                  "slot1": '',
                  "slot2": '',
                  "slot3": '',
                  "slot4": '',
                  "slot5": ''
                },
                "neft": {
                  "slot1": '',
                  "slot2": '',
                  "slot3": '',
                  "slot4": '',
                  "slot5": ''
                }
              },
              "paytm": {
                "penny": '',
                "imps": {
                  "slot1": '',
                  "slot2": '',
                  "slot3": '',
                  "slot4": '',
                  "slot5": ''
                },
                "neft": {
                  "slot1": '',
                  "slot2": '',
                  "slot3": '',
                  "slot4": '',
                  "slot5": ''
                }
              }
            }
            this.createForm(data);
          } else {
            // this.data = JSON.parse(res.data);
            // //console.log(res.data);
            // //console.log(JSON.parse(res.data));
            this.data = JSON.parse(res.data);
            this.createForm(this.data);
          }
        }
        //console.log(JSON.parse(res.data));
      });
    }
  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  createForm(data: any) {
    this.formArr = new FormArray([]);
    this.mainForm = new FormGroup({
      'formArr': this.formArr
    });

    for (const key in data) {

      let innerFormArr = new FormArray([]);
      for (const keys in data[key]) {
        if (keys != 'penny') {
          innerFormArr.push(new FormGroup({
            'groupSlotName': new FormControl(keys),
            'groupSlotFixAmount': new FormControl({ value: '', disabled: true }),
            'groupSlotFixAmountToggle': new FormControl(),
            'slot1': new FormControl(parseInt(data[key][keys].slot1), [Validators.required, Validators.min(1)]),
            'slot2': new FormControl(parseInt(data[key][keys].slot2), [Validators.required, Validators.min(1)]),
            'slot3': new FormControl(parseInt(data[key][keys].slot3), [Validators.required, Validators.min(1)]),
            'slot4': new FormControl(parseInt(data[key][keys].slot4), [Validators.required, Validators.min(1)]),
            'slot5': new FormControl(parseInt(data[key][keys].slot5), [Validators.required, Validators.min(1)]),
          }),
          );
        }
      }

      this.formArr.push(new FormGroup({
        groupName: new FormControl(key),
        groupPennyDrop: new FormControl(parseInt(data[key].penny), [Validators.required, Validators.min(1)]),
        groupSlotArr: innerFormArr
      }));

    }
    //console.log(this.mainFar);
  }

  getArr(arr: any, i: any): any {
    return (<FormArray>arr.controls['groupSlotArr'])
  }

  getArrs(arr: any, i: any): any {
    return ((arr.controls['groupSlotArr'] as FormArray).controls[i] as FormGroup)
  }

  onSubmit() {
    //console.log(this.mainForm.value);
    // //console.log(this.formArr.controls);
    let value = this.formArr.value;
    // let obj: any = {};


    var rv: any = {};
    let mapped = value.map((m: any) => {
      rv[m.groupName] = { 'penny': m.groupPennyDrop };

      for (const key in m.groupSlotArr) {
        // //console.log(key);
        const element = m.groupSlotArr[key];
        let slapObj: any = {};
        for (const keys in element) {
          if (keys !== 'groupSlotFixAmountToggle' && keys !== 'groupSlotName' && keys !== 'groupSlotFixAmount') {
            slapObj[keys] = element[keys];
          }
        }
        rv[m.groupName] = { ...rv[m.groupName], [element.groupSlotName]: slapObj };
      }
    });
    //console.log(rv);

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('type', '5');
    formdata.append('userid', this.userId);
    formdata.append('commission', JSON.stringify(rv));
    this._auth.postdata(formdata, config.updatecommission).subscribe((res: any) => {
      //console.log(res.data);
      if (res.statuscode === 200) {
        Swal.fire(
          'Commission Updated.',
          res.message,
          'success'
        )
      }
    });
  }

  setAmount(event: any, arr: any, i: any) {
    var code = event.keyCode || event.which;
    if (!(code == 16 || code == 9)) {
      //console.log('Tab pressed');

      //console.log(arr, i);
      arr.patchValue({
        "slot1": event.target.value,
        "slot2": event.target.value,
        "slot3": event.target.value,
        "slot4": event.target.value,
        "slot5": event.target.value,
      });
      //console.log(event.target.value);
    }
  }

  toggleAmountInput(event: any, arr: any, i: any, test: any) {
    //console.log(test);
    //console.log(event.currentTarget.checked);
    if (event.currentTarget.checked === true) {
      arr.get('groupSlotFixAmount')?.enable();
      arr.controls["groupSlotFixAmount"].setValidators([Validators.required, Validators.min(1)]);
      arr.controls["groupSlotFixAmount"].updateValueAndValidity();
    } else {

      // //console.log(event);
      arr.controls["groupSlotFixAmount"].clearValidators();
      arr.controls["groupSlotFixAmount"].updateValueAndValidity();
      arr.get('groupSlotFixAmount').patchValue('')
      arr.get('groupSlotFixAmount')?.disable();
      // arr.patchValue({
      //   "slot1": arr.get('groupSlotFixAmount')?.value,
      //   "slot2": arr.get('groupSlotFixAmount')?.value,
      //   "slot3": arr.get('groupSlotFixAmount')?.value,
      //   "slot4": arr.get('groupSlotFixAmount')?.value,
      //   "slot5": arr.get('groupSlotFixAmount')?.value,
      // });
    }
  }

}