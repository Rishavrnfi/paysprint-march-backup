import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-recharge',
  templateUrl: './add-recharge.component.html',
  styleUrls: ['./add-recharge.component.css']
})
export class AddRechargeComponent implements OnInit {
  catName : any;
  categorydata : any;
  searchdata :any ;
  type :any;
  addOprRecharge: any = FormGroup;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route: Router
  ) {
    this.addOprRecharge = this.fb.group({
      api: ['', [Validators.required]],
      op_id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      comm: ['', [Validators.required,Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      category: ['', [Validators.required]],
      type: ['', [Validators.required]],
      status: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata,config.getOprtRechLst).subscribe((res: any) => {    
      this.categorydata = res.data;
     let newArray: any = [];  
     let uniqueObject: any = {}; 
     for (let i in res.data) { 
       let objTitle = res.data[i]['category']; 
       uniqueObject[objTitle] = res.data[i];
     }  
     for (let j in uniqueObject) {
       newArray.push({ 'catName': uniqueObject[j]['category']});
     }
     this.catName =newArray
     //console.log(newArray);
     
    })
 
  }
  gettype(val: any) { 
    var  marvelHeroes =  this.categorydata.filter(function(res : any) { 
      return res.category == val;
     }); 
  //  //console.log(marvelHeroes)
   let newArray1: any = [];  
   let uniqueObject1: any = {}; 
   for (let i in marvelHeroes) { 
     let objTitle = marvelHeroes[i]['type']; 
     uniqueObject1[objTitle] = marvelHeroes[i];
   }  
   for (let j in uniqueObject1) {
     newArray1.push({ 'typename': uniqueObject1[j]['type']});
   }   
   //console.log(newArray1[0]);
   this.type =newArray1[0].typename;
  }

  onAddOperatorRech() {
    if (!this.addOprRecharge.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('api', this.addOprRecharge.get('api').value);
      formdata.append('op_id', this.addOprRecharge.get('op_id').value);
      formdata.append('name', this.addOprRecharge.get('name').value);
      formdata.append('category', this.addOprRecharge.get('category').value);
      formdata.append('comm', this.addOprRecharge.get('comm').value);
      formdata.append('type', this.addOprRecharge.get('type').value);
      formdata.append('status', this.addOprRecharge.get('status').value);
      this._auth.postdata(formdata, config.addOprtRech).subscribe((res: any) => {
        if (res.statuscode == 200) {
          Swal.fire({
            title: res.message,
            icon: 'success'
          });
          this.addOprRecharge.reset();  
        }else{
          Swal.fire({
            icon: 'error',
            title: res.message,
          })
        }
      })
    }
  }
  get f() { return this.addOprRecharge.controls; }
}
