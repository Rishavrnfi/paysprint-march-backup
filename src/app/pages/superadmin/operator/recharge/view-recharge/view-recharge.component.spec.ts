import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRechargeComponent } from './view-recharge.component';

describe('ViewRechargeComponent', () => {
  let component: ViewRechargeComponent;
  let fixture: ComponentFixture<ViewRechargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewRechargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRechargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
