import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-recharge',
  templateUrl: './view-recharge.component.html',
  styleUrls: ['./view-recharge.component.css']
})
export class ViewRechargeComponent implements OnInit {
  operatorRechList: any; 
  editOptrRechForm: any = FormGroup;
  userID: any;
  catName : any;
  categorydata : any;
  searchdata :any ; 
  type :any;
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal : CustomeModalService
  ) {

  }
  ngOnInit(): void {
    this.editOptrRechForm = this.fb.group({
      api: ['', [Validators.required]],
      op_id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      category: ['', [Validators.required]],
      type: ['', [Validators.required]],
      status: ['', [Validators.required]],
      comm: ['', [Validators.required,Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      operatorid: ['', [Validators.required]],
    });
    //this.listOperatorBill();
    this.getCategory();
  }
  onEditSingleOprtRech(data :any,details : any) {
    this.modal.open(data); 
    this.editOptrRechForm.reset();   
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('operatorid', details.id);
    this._auth.postdata(formdata, config.getOprtRech).subscribe((res: any) => {  
      this.editOptrRechForm.controls["api"].setValue(res.data.api);
      this.editOptrRechForm.controls["op_id"].setValue(res.data.op_id);
      this.editOptrRechForm.controls["name"].setValue(res.data.name);
      this.editOptrRechForm.controls["category"].setValue(res.data.category);
      this.editOptrRechForm.controls["status"].setValue(res.data.status);
      this.editOptrRechForm.controls["comm"].setValue(res.data.comm);
      this.editOptrRechForm.controls["operatorid"].setValue(res.data.id); 
      this.type= res.data.type;
    });

  }
  onSaveEditSingleOprtBill() { 
    if (!this.editOptrRechForm.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('api', this.editOptrRechForm.get('api').value);
      formdata.append('op_id', this.editOptrRechForm.get('op_id').value);
      formdata.append('name', this.editOptrRechForm.get('name').value);
      formdata.append('category', this.editOptrRechForm.get('category').value);
      formdata.append('type', this.editOptrRechForm.get('type').value);
      formdata.append('status', this.editOptrRechForm.get('status').value);
      formdata.append('comm', this.editOptrRechForm.get('comm').value);
      formdata.append('operatorid', this.editOptrRechForm.get('operatorid').value);
      this._auth.postdata(formdata, config.updateOprtRech).subscribe((res: any) => {
        if (res.statuscode == 200) { 
            this.modal.close(); 
            Swal.fire({
              title: res.message, 
              icon: 'success'
            });
            this.editOptrRechForm.reset();
            const formdata = new FormData();
            formdata.append('token', config.tokenauth);
            this._auth.postdata(formdata, config.getOprtRechLst).subscribe((res: any) => {
              this.operatorRechList = res.data; 
              this.dtTrigger.next();
            });
          
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message, 
          }) 
        } 
      })
    }
  }

  // listOperatorBill() {

  //   const formdata = new FormData();
  //   formdata.append('token', config.tokenauth);
  //   this._auth.postdata(formdata, config.getOprtRechLst).subscribe((res: any) => {
  //     this.operatorRechList = res.data;
  //     setTimeout(() => {
  //       $('#operatorRList').DataTable({
  //         pagingType: 'full_numbers',
  //         pageLength: 10,
  //         processing: true,
  //         lengthMenu: [10, 25, 50, 100]
  //       });
  //     }, 1);
  //   });
  // }
  
  gettype(val: any) { 
    var  marvelHeroes =  this.categorydata.filter(function(res : any) { 
      return res.category == val;
     }); 
  //  //console.log(marvelHeroes)
   let newArray1: any = [];  
   let uniqueObject1: any = {}; 
   for (let i in marvelHeroes) { 
     let objTitle = marvelHeroes[i]['type']; 
     uniqueObject1[objTitle] = marvelHeroes[i];
   }  
   for (let j in uniqueObject1) {
     newArray1.push({ 'typename': uniqueObject1[j]['type']});
   }   
   ////console.log(newArray1[0]);
   this.type =newArray1[0].typename;
  }

  getCategory(){
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata,config.getOprtRechLst).subscribe((res: any) => {    
      this.operatorRechList = res.data;
      setTimeout(() => {
        $('#operatorRList').DataTable({
          pagingType: 'full_numbers',
          pageLength: 10,
          language: {
            paginate: { "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', 
                        "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                        "first":'GHF', "last" :"GHFH"},
            
            search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            searchPlaceholder: "Search...",
            lengthMenu: "Results :  _MENU_",
          },
          processing: true,
          lengthMenu: [10, 25, 50, 100]
        });
      }, 1);
     let newArray: any = [];  
     let uniqueObject: any = {}; 
     for (let i in res.data) { 
       let objTitle = res.data[i]['category']; 
       uniqueObject[objTitle] = res.data[i];
     }  
     for (let j in uniqueObject) {
       newArray.push({ 'catName': uniqueObject[j]['category']});
     }
     this.catName =newArray
     ////console.log(newArray);
     
    })
  }
   
  get f() { return this.editOptrRechForm.controls; }
}
