import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-bill',
  templateUrl: './add-bill.component.html',
  styleUrls: ['./add-bill.component.css']
})
export class AddBillComponent implements OnInit {

  addOperatorBill: any = FormGroup;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route: Router
  ) {
    this.addOperatorBill = this.fb.group({
      name: ['', [Validators.required]],
      category: ['', [Validators.required]],
      viewbill: ['', [Validators.required]],
      op_id: ['', [Validators.required]],
      ss_id: ['', [Validators.required]],
      twd_id: ['', [Validators.required]],
      regex: ['', [Validators.required]],
      displayname: ['', [Validators.required]],
      ad1_d_name: [''],
      ad1_regex: [''],
      ad1_name: [''],
      ad2_d_name: [''],
      ad2_regex: [''],
      ad2_name: [''],
      ad3_d_name: [''],
      ad3_regex: [''],
      ad3_name: [''],
      min_limit: [''],
      max_limit: [''],
      status: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  onAddOperatorBill() {
    if (!this.addOperatorBill.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('api', '0');
      formdata.append('op_id', this.addOperatorBill.get('op_id').value);
      formdata.append('ss_id', this.addOperatorBill.get('ss_id').value);
      formdata.append('twd_id', this.addOperatorBill.get('twd_id').value);
      formdata.append('name', this.addOperatorBill.get('name').value);
      formdata.append('category', this.addOperatorBill.get('category').value);
      formdata.append('viewbill', this.addOperatorBill.get('viewbill').value);
      formdata.append('regex', this.addOperatorBill.get('regex').value);
      formdata.append('displayname', this.addOperatorBill.get('displayname').value);
      formdata.append('ad1_d_name', this.addOperatorBill.get('ad1_d_name').value);
      formdata.append('ad1_regex', this.addOperatorBill.get('ad1_regex').value);
      formdata.append('ad1_name', this.addOperatorBill.get('ad1_name').value);
      formdata.append('ad2_d_name', this.addOperatorBill.get('ad2_d_name').value);
      formdata.append('ad2_regex', this.addOperatorBill.get('ad2_regex').value);
      formdata.append('ad2_name', this.addOperatorBill.get('ad2_name').value);
      formdata.append('ad3_d_name', this.addOperatorBill.get('ad3_d_name').value);
      formdata.append('ad3_regex', this.addOperatorBill.get('ad3_regex').value);
      formdata.append('ad3_name', this.addOperatorBill.get('ad3_name').value);
      formdata.append('status', this.addOperatorBill.get('status').value);
      formdata.append('min_limit', this.addOperatorBill.get('min_limit').value);
      formdata.append('max_limit', this.addOperatorBill.get('max_limit').value);
      this._auth.postdata(formdata, config.addOprtBill).subscribe((res: any) => {
        if (res.statuscode == 200) {
              Swal.fire({ 
                title: res.message,
                icon: 'success'
              });
              this.addOperatorBill.reset();  
        }else{
          Swal.fire({
            icon: 'error',
            title: res.message
          }) 
        }
      })
    }
  }
  get f() { return this.addOperatorBill.controls; }
}
