import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOperatorBillComponent } from './view-operator-bill.component';

describe('ViewOperatorBillComponent', () => {
  let component: ViewOperatorBillComponent;
  let fixture: ComponentFixture<ViewOperatorBillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewOperatorBillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOperatorBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
