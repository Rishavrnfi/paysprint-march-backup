import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';
import { DatatableServiceService } from 'src/app/_helpers/common/custome-datatable/datatable-service.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-view-operator-bill',
  templateUrl: './view-operator-bill.component.html',
  styleUrls: ['./view-operator-bill.component.css']
})
export class ViewOperatorBillComponent implements OnInit {


  formdata: any;

  operatorBillList: any;
  editOperatorBill: any = FormGroup;
  userID: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: any = DataTableDirective;
  dtOptions: DataTables.Settings = {};
  isDtInitialized: boolean = false;
  dtTrigger: any = new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal: CustomeModalService,
    private datat: DatatableServiceService,
    private http: HttpClient
  ) {
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
    }
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: { "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', 
                    "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                    "first":'GHF', "last" :"GHFH"},
        
        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      pageLength: 10,
      processing: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
    };
    this.editOperatorBill = this.fb.group({
      api: [null, [Validators.required]],
      op_id: [null, [Validators.required]],
      ss_id: [null, [Validators.required]],
      twd_id: [null, [Validators.required]],
      name: [null, [Validators.required]],
      category: [null, [Validators.required]],
      viewbill: [null, [Validators.required]],
      regex: [null, [Validators.required]],
      displayname: [null, [Validators.required]],
      ad1_d_name: [null],
      ad1_regex: [null],
      ad1_name: [null],
      ad2_d_name: [null],
      ad2_regex: [null],
      ad2_name: [null],
      ad3_d_name: [null],
      ad3_regex: [null],
      ad3_name: [null],
      status: [null, [Validators.required]],
      min_limit: [null],
      max_limit: [null],
      operatorid: [null, [Validators.required]],
    });

    // this.listOperatorBill();
    let headers = new HttpHeaders({
      'Authkey': config.Authkey,
      'Authtoken': this._auth.isLoggedIn(),
      "Content-Type": "application/json"
    })

    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.getOprtBillLst, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
          if (res.statuscode == 200) {
            ////console.log(res.data);
            that.operatorBillList = res.data;
            // this.viewtable = true;
            ////console.log(res.data);
            callback({
              recordsTotal: res.recordsTotal,
              recordsFiltered: res.recordsFiltered,
              data: []
            });
          }
        });
      },
    };
  }
  onEditSingleOprtBill(data: any, details: any) {
    const formdata = new FormData();
    formdata.append('operatorid', details.id);
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getOprtBill).subscribe((res: any) => {
      if (res.statuscode == 200) {
        this.modal.open(data);
        this.editOperatorBill.setValue({
          "operatorid": res.data.id,
          "name": res.data.name,
          "api": res.data.api,
          "category": res.data.category,
          "viewbill": res.data.viewbill,
          "op_id": res.data.op_id,
          "ss_id": res.data.ss_id,
          "twd_id": res.data.twd_id,
          "regex": res.data.regex,
          "displayname": res.data.displayname,
          "ad1_d_name": res.data.ad1_d_name,
          "ad1_regex": res.data.ad1_regex,
          "ad1_name": res.data.ad1_name,
          "ad2_d_name": res.data.ad2_d_name,
          "ad2_regex": res.data.ad2_regex,
          "ad2_name": res.data.ad2_name,
          "ad3_d_name": res.data.ad3_d_name,
          "ad3_regex": res.data.ad3_regex,
          "ad3_name": res.data.ad3_name,
          "min_limit": res.data.min_limit,
          "max_limit": res.data.max_limit,
          "status": res.data.status
        })
      }
    })

  }
  onSaveEditSingleOprtBill() {
    if (!this.editOperatorBill.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('api', '0');
      formdata.append('op_id', this.editOperatorBill.get('op_id').value);
      formdata.append('ss_id', this.editOperatorBill.get('ss_id').value);
      formdata.append('twd_id', this.editOperatorBill.get('twd_id').value);
      formdata.append('name', this.editOperatorBill.get('name').value);
      formdata.append('category', this.editOperatorBill.get('category').value);
      formdata.append('viewbill', this.editOperatorBill.get('viewbill').value);
      formdata.append('regex', this.editOperatorBill.get('regex').value);
      formdata.append('displayname', this.editOperatorBill.get('displayname').value);
      formdata.append('ad1_d_name', this.editOperatorBill.get('ad1_d_name').value);
      formdata.append('ad1_regex', this.editOperatorBill.get('ad1_regex').value);
      formdata.append('ad1_name', this.editOperatorBill.get('ad1_name').value);
      formdata.append('ad2_d_name', this.editOperatorBill.get('ad2_d_name').value);
      formdata.append('ad2_regex', this.editOperatorBill.get('ad2_regex').value);
      formdata.append('ad2_name', this.editOperatorBill.get('ad2_name').value);
      formdata.append('ad3_d_name', this.editOperatorBill.get('ad3_d_name').value);
      formdata.append('ad3_regex', this.editOperatorBill.get('ad3_regex').value);
      formdata.append('ad3_name', this.editOperatorBill.get('ad3_name').value);
      formdata.append('status', this.editOperatorBill.get('status').value);
      formdata.append('min_limit', this.editOperatorBill.get('min_limit').value);
      formdata.append('max_limit', this.editOperatorBill.get('max_limit').value);
      formdata.append('operatorid', this.editOperatorBill.get('operatorid').value);
      this._auth.postdata(formdata, config.updateOprtBill).subscribe((res: any) => {
        if (res.statuscode == 200) {
          this.modal.close();
          this.editOperatorBill.reset(); 
          Swal.fire({
            title: res.message,
            icon: 'success'
          }); 
          this.rerender();

        } else {
          Swal.fire({
            icon: 'error',
            title: res.message
          })
        }
      })
    }
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => { 
      dtInstance.draw(); 
      this.dtTrigger.next();
    });
  }

 

  get f() { return this.editOperatorBill.controls; }
}