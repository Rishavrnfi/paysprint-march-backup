import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import Swal from 'sweetalert2';
declare let $: any;

@Component({
  selector: 'app-list-company-bank',
  templateUrl: './list-company-bank.component.html',
  styleUrls: ['./list-company-bank.component.css']
})

export class ListCompanyBankComponent implements OnInit {
  CompanyBankList: any;
  editCompanyBank: any = FormGroup;
  userID: any;
  activestatus: any;
  transfer_allowed: any;
  is_slip: any;
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal: CustomeModalService
  ) {
    this.listCompanyBank();
    this.editCompanyBank = this.fb.group({
      name: ['', [Validators.required]],
      accountno: ['', [Validators.required]],
      balance: ['', [Validators.required]],
      ifsc: ['', [Validators.required]],
      branch: ['', [Validators.required]],
      transfer_allowed: ['', [Validators.required]],
      is_slip: ['', [Validators.required]],
      activestatus: ['', [Validators.required]]
    });
  }

  ngOnInit(): void { }
  async listCompanyBank() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
      this.CompanyBankList = res.data;
      setTimeout(() => {
        $('#CompanyList').DataTable({
          pagingType: 'full_numbers',
          pageLength: 10,
          language: {
            paginate: { "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', 
                        "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                        "first":'GHF', "last" :"GHFH"},
            
            search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            searchPlaceholder: "Search...",
            lengthMenu: "Results :  _MENU_",
          },
          processing: true,
          lengthMenu: [10, 25, 50, 100]
        });
      }, 1);
    });
  }


  editcompanybank(data: any, details: any) {
    this.modal.open(data);
    //console.log(details.id);
    const formdata = new FormData();
    formdata.append('bankid', details.id);
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getcompanybank).subscribe((res: any) => {
      this.userID = res.data.id;
      this.editCompanyBank.controls["name"].setValue(res.data.name);
      this.editCompanyBank.controls["accountno"].setValue(res.data.accno);
      this.editCompanyBank.controls["balance"].setValue(res.data.balance);
      this.editCompanyBank.controls["ifsc"].setValue(res.data.ifsc);
      this.editCompanyBank.controls["branch"].setValue(res.data.branch);
      this.editCompanyBank.controls["transfer_allowed"].setValue(res.data.is_transfer_allowed);
      this.activestatus = res.data.status;
      this.is_slip = res.data.is_slip;
      this.transfer_allowed = res.data.is_transfer_allowed;
    })
  }
  edit_company_bank() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('name', this.editCompanyBank.get('name').value);
    formdata.append('accno', this.editCompanyBank.get('accountno').value);
    formdata.append('balance', this.editCompanyBank.get('balance').value);
    formdata.append('ifsc', this.editCompanyBank.get('ifsc').value);
    formdata.append('branch', this.editCompanyBank.get('branch').value);
    formdata.append('is_transfer_allowed', this.editCompanyBank.get('transfer_allowed').value);
    formdata.append('is_slip', this.editCompanyBank.get('is_slip').value);
    formdata.append('bankid', this.userID);
    formdata.append('status', this.editCompanyBank.get('activestatus').value);
    this._auth.postdata(formdata, config.editcompanybank).subscribe((res: any) => {
      if (res.statuscode == 200) {
        this.listCompanyBank = res.data;
        this.modal.close();
        this.editCompanyBank.reset();
        Swal.fire({
          title: res.message,
          icon: 'success'
        });
        const formdata = new FormData();
        formdata.append('token', config.tokenauth);
        this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
          this.CompanyBankList = res.data;
          this.dtTrigger.next();
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: res.message,
        })
      }
    })
  }

  get f() { return this.editCompanyBank.controls; }
}
