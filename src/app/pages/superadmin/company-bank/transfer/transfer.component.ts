import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Select2OptionData } from 'ng-select2';  
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter} from 'rxjs/operators';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';
import { LoaderService } from 'src/app/_helpers/common/loader.service';
import Swal from 'sweetalert2';
type State = {id: number, name: string};
 


@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit { 
   
  addtransfer: any = FormGroup;
  exampleData: Array<Select2OptionData> = [];
  tobankList:Array<Select2OptionData> = [];
  banklist : any;

  from_bank : any;
  amount:any;
  carData?:string;
  getNumberOnChange:any; 
  isshow:boolean=false; 
  
  public codeValue?: string;
  public states: State[]=[];
  public model: any; 

  
 
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route: Router,
    private loader:LoaderService,
    private convrtAmt : CommonService,
    private http: HttpClient
  ) { 
    this.addtransfer = this.fb.group({
      from_bank: ['', [Validators.required]],
      to_bank: ['', [Validators.required]],
      amount: ['', [Validators.required,Validators.pattern(/^[0-9]+(\.[0-9]{1,2})?$/)]],
      remarks: ['', [Validators.required]] 
    });
  }

  ngOnInit(): void {
    
    let newArray: any = []; 
    let tobankListArray:any = [];
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
      let  data1 = res.data.filter((dt: any) => {
        return dt.is_transfer_allowed == 1;
      })
      let  tobankList = res.data.filter((dt: any) => {
        return dt.is_value_update == 1;
      })
      for (let i in data1) {
        let objTitle = data1[i]['name'];
        let objTitle1 = data1[i]['id']; 
        let accountno = data1[i]['accno'];
        let branch = data1[i]['branch'];  
        let final = objTitle + " | " + accountno + " | " + branch;
        newArray.push({ id: objTitle1, 'text': final });
      }
      this.exampleData = newArray;
      //console.log(tobankList);
       for (let i in tobankList) {
        let objName = tobankList[i]['name'];
        let objid = tobankList[i]['id']; 
        let account = tobankList[i]['accno'];
        let branchname = tobankList[i]['branch'];  
        let finalData = objName + " | " + account + " | " + branchname;
        tobankListArray.push({ id: objid, 'text': finalData });
      }
      this.tobankList = tobankListArray;
    });
  }

  formatter = (state: State) => state.name;

  search = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    filter(term => term.length >= 4),
    map( term =>this.states.filter(state => new RegExp(term, 'mi').test(state.name)).slice(0, 10) )
  )
   
           
  loaddata(){
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
      this.states = res.value.data; 
      })
    
  }
  OnaddTransfer(){ 
    if (!this.addtransfer.valid) {
      return;
    } else{
    if (this.addtransfer.value.from_bank === this.addtransfer.value.to_bank) {
      Swal.fire('Please Select Diffrent bank!!');
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth); 
      formdata.append('from_bank', this.addtransfer.get('from_bank').value);
      formdata.append('to_bank',  this.addtransfer.get('to_bank').value);
      formdata.append('amount',  this.addtransfer.get('amount').value);
      formdata.append('remarks',  this.addtransfer.get('remarks').value);
      this._auth.postdata(formdata, config.addBankTransfer).subscribe((res: any) => {
        if (res.statuscode == 200) { 
                Swal.fire({ 
                  title: res.message,
                  icon: 'success'
                
                });
              this.addtransfer.reset();
              this.loader.loaderEvent.emit(false);
              this.isshow = false;
            
        }else{
          Swal.fire({
            icon: 'error',
            title: res.message
           
          });
        }
      })
    } 
  } 
  }
  onKey(val:any){  
    this.amount = this.convrtAmt.convertNumberToWords(val.target.value);
    if(!this.amount){
      this.isshow = false;
    }else{
      this.isshow = true;
    }
   
    //console.log(this.amount)
  }
  get f() { return this.addtransfer.controls; }
}
