import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-company-bank',
  templateUrl: './add-company-bank.component.html',
  styleUrls: ['./add-company-bank.component.css']
})
export class AddCompanyBankComponent implements OnInit {
  addCompanyBank :any = FormGroup; 
  activestatus:any;
  transfer_allowed:any;
  is_slip:any;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route:Router
  ) {
    this.addCompanyBank = this.fb.group({
      name      :['', [Validators.required]],
      accountno :['',[Validators.required]],
      balance   :['',[Validators.required]],
      ifsc      :['',[Validators.required]],
      branch    :['',[Validators.required]],
      transfer_allowed  :['',[Validators.required]],
      is_slip              :['',[Validators.required]],
      activestatus      :['',[Validators.required]]
    });
   }

  ngOnInit(): void {
  }

  async add_company_bank(){
    if (!this.addCompanyBank.valid){
      return;
    }else{ 
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name',this.addCompanyBank.get('name').value);
      formdata.append('accno',this.addCompanyBank.get('accountno').value);
      formdata.append('balance',this.addCompanyBank.get('balance').value);
      formdata.append('ifsc',this.addCompanyBank.get('ifsc').value);
      formdata.append('branch',this.addCompanyBank.get('branch').value);
      formdata.append('is_transfer_allowed',this.addCompanyBank.get('transfer_allowed').value);
      formdata.append('is_slip',this.addCompanyBank.get('is_slip').value); 
      formdata.append('status',this.addCompanyBank.get('activestatus').value);  
      this._auth.postdata(formdata,config.addcompanybank).subscribe((res: any) => { 
        if (res.statuscode == 200) {
          Swal.fire({
            title: res.message, 
            icon: 'success'
          }); 
          this.addCompanyBank.reset();
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message, 
          })
        }
      })
    }
 }
 get f() { return this.addCompanyBank.controls;}
}
