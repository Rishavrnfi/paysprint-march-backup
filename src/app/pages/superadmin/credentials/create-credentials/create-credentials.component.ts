import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';

@Component({
  selector: 'app-create-credentials',
  templateUrl: './create-credentials.component.html',
  styleUrls: ['./create-credentials.component.css']
})
export class CreateCredentialsComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any = [];
  data: any;

  @Input() userDataId: any = '';
  upUserId: any;
  updatedData: any = [];
  showErrorMesg: any = null;
  showData = false;
  constructor(private _auth: ApiService) { }

  ngOnInit(): void {
    // this.getuser();
    if (this.userDataId !== '') {
      this.upUserId = this.userDataId;
      this.selectEvent(this.upUserId);
    }
  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  selectEvent(event: any) {
    this.updatedData = [];
    this.upUserId = typeof event === 'object' ? event.userid : event;
    //console.log(this.upUserId)
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.upUserId);
    this._auth.postdata(formdata, config.getcredentials).subscribe((res: any) => {
      let udData = res.data;
      for (const key in udData) {
        const element = udData[key];
        let cust = {
          'name': key,
          'value': element
        }
        this.updatedData.push(cust)
      }
      this.showData = true;
      ////console.log(res);
    })

  }

}
