import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CredentialInitComponent } from './credential-init.component';

describe('CredentialInitComponent', () => {
  let component: CredentialInitComponent;
  let fixture: ComponentFixture<CredentialInitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CredentialInitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialInitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
