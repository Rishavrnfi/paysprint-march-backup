import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { ValidatUrl } from 'src/app/_helpers/common/custom-validator/validat-url';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-credential-init',
  templateUrl: './credential-init.component.html',
  styleUrls: ['./credential-init.component.css']
})
export class CredentialInitComponent implements OnInit {
  keyword = 'userdetails';
  userDtLst: any;
  data: any;
  userid: any;
  showData: boolean = false;
  getdata: any;
  form: FormGroup = new FormGroup({});
  userData: any;
  usertype: any;
  constructor(private _auth: ApiService,) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      cburl: new FormControl(null, [Validators.required, ValidatUrl.startWith(['http', 'https'])]),
    })

    this.usertype = this._auth.Getsessiondata().usertype;
    if (this.usertype == 5) {
      this.selectEvent({userid:'5'});
    }
  }


  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  selectEvent(event: any) {

    const formdata = new FormData();
    formdata.append('token', config.tokenauth); 
      formdata.append('userid', event.userid);
 
    this.userData = event;
    this.userid = event.userid;
    this._auth.postdata(formdata, config.getcredential).subscribe((res: any) => {
      if (res.statuscode === 200) {
        this.getdata = res;
        this.showData = true;
        //console.log(res)
        if (res.is_callback === false) {
          this.form.patchValue({ "cburl": res.data.callback_url })
        } else if (res.is_callback === true) {
          this.form.patchValue({ "cburl": null })
        }
      }
    })
  }

  authrizeFn() {
    Swal.fire({
      title: 'Are you sure to Generate  This Authorised Key?', 
      showCancelButton: true,
      icon: 'info',
      confirmButtonText: `Save`, 
    }).then((result) => {
      if (result.isConfirmed) {
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.userid);
    formdata.append('type', 'authorisedkey');
    this._auth.postdata(formdata, config.updateinitcredentials).subscribe((res: any) => {
      if (res.statuscode === 200) {
        this.selectEvent(this.userData);
      }
    });
  } else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }
  })
  }
  callBackFn() {
    Swal.fire({
      title: 'Are you sure to save this callback url?', 
      showCancelButton: true,
      icon: 'info',
      confirmButtonText: `Save`, 
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        let formdata: any = new FormData();
        formdata.append('token', config.tokenauth);
        formdata.append('userid', this.userid);
        formdata.append('type', 'callback_url');
        formdata.append('callback_url', this.form.get('cburl')?.value);
        this._auth.postdata(formdata, config.updateinitcredentials).subscribe((res: any) => {
          if (res.statuscode === 200) {
            Swal.fire('Saved!', '', 'success');
            this.selectEvent(this.userData);
          }else{
            Swal.fire(res.statuscode);
          }
        }); 
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
     
    
  }
}
