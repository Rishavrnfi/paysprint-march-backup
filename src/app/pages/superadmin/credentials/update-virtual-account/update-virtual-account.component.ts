import { Route } from '@angular/compiler/src/core';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng-select2';
import { Options } from 'select2';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-virtual-account',
  templateUrl: './update-virtual-account.component.html',
  styleUrls: ['./update-virtual-account.component.css']
})
export class UpdateVirtualAccountComponent implements OnInit {

  keyword = 'userdetails';
  userDtLst: any;
  data: any;


  @Input() userId: any = '';
  updateVa: any = FormGroup;
  activestatus: any;
  exampleData: Array<Select2OptionData> = [];
  public exampleDataMul: Array<Select2OptionData> = [];
  splits!: any[];
  options!: { width: string; multiple: boolean; tags: boolean; };
  lastVal: any;
  _value!: string[];
  newArray: [] = [];
  va: any;
  showErrorMesg: any = null;
  showData = false;
  vaId: any;
  limit: any;
  Onuserid: any;
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal: CustomeModalService
  ) { }

  ngOnInit(): void {
    // this.getuser();
    this.updateVa = this.fb.group({
      activestatus: ['', [Validators.required]],
      from_account: ['', [Validators.required]]
    });
    this.options = {
      width: '320',
      multiple: true,
      tags: true
    };

    // var data = ["78945", "45666"];
    // //console.log(JSON.stringify(data));


  }
  get value(): string[] {
    return this._value;
  }
  set value(value: string[]) {
    //console.log('Set value: ' + value);
    this._value = value;
  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  // getuser() {
  //   let newArray: any = [];
  //   let uniqueObject: any = {};
  //   let formdata: any = new FormData();
  //   formdata.append('token', config.tokenauth);
  //   formdata.append('Authkey', config.Authkey);
  //   this._auth.postdata(formdata, config.getuserlist).subscribe((res: any) => {
  //     // this.userLst = res.data;
  //     for (let i in res.data) {
  //       let userid = res.data[i]['userid'];
  //       let objTitle = res.data[i]['name'];
  //       let objTittle3 = res.data[i]['username'];
  //       let firmname = res.data[i]['firmname'];
  //       let final = objTitle + " | " + objTittle3 + " | " + firmname;
  //       newArray.push({ id: userid, 'text': final });

  //     }
  //     this.exampleData = newArray;
  //     // //console.log(newArray);
  //   });

  // }
  selectEvent(event: any) {

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', event.userid);
    this.Onuserid = event.userid;
    this._auth.postdata(formdata, config.getVirtualaccount).subscribe((res: any) => {
      this.va = res.data;
      this.showData = true;
      //console.log(res)
    })
  }
  // getUserData(val?: any) {
  //   //console.log(val)
  //   if (val === undefined) {
  //   } else {
  //     const formdata = new FormData();
  //     formdata.append('token', config.tokenauth);
  //     formdata.append('userid', val);
  //     this.Onuserid = val;
  //     this._auth.postdata(formdata, config.getVirtualaccount).subscribe((res: any) => {
  //       this.va = res.data;
  //       this.showData = true;
  //       //console.log(res)
  //     })
  //   }
  // }

  update_Va(data: any, details: any) {
    this.modal.open(data);
    this.updateVa.reset();
    let from_account = details.from_account;
    this.vaId = details.id;
    this.limit = details.limit;
    this.activestatus = details.status;
    ////console.log(details);status

    this.splits = from_account.split(',')
    let sdsk: any[] = []

    this.splits.map((m: any) => {

      sdsk.push({
        id: m,
        text: m
      })
    })
    this.options = {
      width: '320',
      multiple: true,
      tags: true
    };
    this.exampleDataMul = sdsk;

    let sdsksd: any[] = []
    this.exampleDataMul.map((d: any) => {
      // //console.log(d.id);
      sdsksd.push((d.id).toString())
    })
    this._value = sdsksd;
  }
  // this.exampleDataMul = newArray;


  OnUpdate_Va() {

    const formdata = new FormData();
    let sdsd: string = '';
    for (const key in this._value) {
      const element = this._value[key];
      ////console.log(element);
      sdsd += (key === '0' ? element : ',' + element);
    }
    formdata.append('token', config.tokenauth);
    formdata.append('account_number', sdsd);
    formdata.append('status', this.updateVa.get('activestatus').value);
    formdata.append('limit', this.limit);
    formdata.append('vaid', this.vaId);
    formdata.append('userid', this.Onuserid);
    this._auth.postdata(formdata, config.updatevirtualaccount).subscribe((res: any) => {
      if (res.statuscode == 200) {
        this.modal.close();
        Swal.fire({
          title: res.message,
          icon: 'success'
        });
        this.updateVa.reset();

      } else {
        Swal.fire({
          icon: 'error',
          title: res.message
        })
      }

    })
  }
}
