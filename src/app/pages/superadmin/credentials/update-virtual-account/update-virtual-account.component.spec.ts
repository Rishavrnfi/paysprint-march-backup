import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVirtualAccountComponent } from './update-virtual-account.component';

describe('UpdateVirtualAccountComponent', () => {
  let component: UpdateVirtualAccountComponent;
  let fixture: ComponentFixture<UpdateVirtualAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateVirtualAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVirtualAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
