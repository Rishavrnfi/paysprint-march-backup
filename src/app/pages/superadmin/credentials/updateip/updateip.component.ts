import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Select2OptionData } from 'ng-select2';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-updateip',
  templateUrl: './updateip.component.html',
  styleUrls: ['./updateip.component.css']
})
export class UpdateipComponent implements OnInit {
  @Input() userId: any = '';

  keyword = 'userdetails';
  userDtLst: any;
  data: any;

  updateIp: any = FormGroup;
  exampleData: Array<Select2OptionData> = [];
  upUserId: any;
  updatedData: any = [];
  showErrorMesg: any = null;
  showData = false;
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private route: Router) { }
  ngOnInit(): void {
    // this.getUsercredentials()
    // this.getuser();
    if (this.userId !== '') {
      this.upUserId = this.userId;
      this.selectEvent(this.upUserId)
    }
    this.updateIp = this.fb.group({
      ip: ['', [Validators.required,]],
      activestatus: ['', [Validators.required]]
    });
  }
  getuser() {
    // let newArray: any = [];
    // let uniqueObject: any = {};
    // let formdata: any = new FormData();
    // formdata.append('token', config.tokenauth);
    // formdata.append('Authkey', config.Authkey);
    // this._auth.postdata(formdata, config.getuserlist).subscribe((res: any) => {
    //   // this.userLst = res.data;
    //   for (let i in res.data) {
    //     let userid = res.data[i]['userid'];
    //     let objTitle = res.data[i]['name'];
    //     let objTittle3 = res.data[i]['username'];
    //     let firmname = res.data[i]['firmname'];
    //     let final = objTitle + " | " + objTittle3 + " | " + firmname;
    //     newArray.push({ id: userid, 'text': final });

    //   }
    //   this.exampleData = newArray;
    //   // //console.log(newArray);
    // });

  }
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
      if (res['data'] == undefined) {
        this.userDtLst = [];
      } else {
        this.userDtLst = res['data'];
      }
    });
  }
  selectEvent(event: any) {

    this.upUserId = typeof event === 'object' ? event.userid : event;
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.upUserId);
    this._auth.postdata(formdata, config.getcredentials).subscribe((res: any) => {
      this.updateIp.setValue({
        "ip": res.data.WHITELISTED_IP,
        "activestatus": res.data.STATUS
      })

      this.updatedData = res.data;
      this.showData = true;
      ////console.log(res.data)
    })
  }
  // getUsercredentials(val?: any) {
  //   //console.log(val)
  //   if (val === undefined) {
  //   } else {
  //     const formdata = new FormData();
  //     formdata.append('token', config.tokenauth);
  //     formdata.append('userid', val);
  //     this._auth.postdata(formdata, config.getcredentials).subscribe((res: any) => {
  //       this.updateIp.setValue({
  //         "ip": res.data.WHITELISTED_IP,
  //         "activestatus": res.data.STATUS
  //       })

  //       this.updatedData = res.data;
  //       this.upUserId = val;
  //       this.showData = true;
  //       ////console.log(res.data)
  //     })
  //   }
  // }

  OnUpdate_ip() {
    if (!this.updateIp.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('allowip', this.updateIp.get('ip').value);
      formdata.append('userid', this.upUserId);
      formdata.append('status', this.updateIp.get('activestatus').value);
      this._auth.postdata(formdata, config.updatecredentials).subscribe((res: any) => {
        if (res.statuscode == 200) {
          Swal.fire({
            title: res.message,
            icon: 'success'
          });
          this.updateIp.reset();
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message
          })
        }
      })
    }
  }
  get f() { return this.updateIp.controls; }
}
