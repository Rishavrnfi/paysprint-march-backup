import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { config } from 'src/app/service/config';
import { ApiService } from 'src/app/service/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs'; 
import { environment } from 'src/environments/environment';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-list-staff',
  templateUrl: './list-staff.component.html',
  styleUrls: ['./list-staff.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ListStaffComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  editStaff: any = FormGroup;
  staffdata: any;
  formdata: any;
  staffID: any; 
  remark :any;
  activestatus:any; 
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal: CustomeModalService,
    private http: HttpClient
  ) { 
    this.editStaff = this.fb.group({
      name: ['', [Validators.required]],   
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo: ['', [Validators.required, Validators.pattern('[6789][0-9]{9}')]], 
      remarks: ['', [Validators.required]],
      activestatus: ['', [Validators.required]]
    });
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
    }
  }

  ngOnInit(): void {
    let headers = new HttpHeaders({
      'Authkey': config.Authkey,
      'Authtoken': this._auth.isLoggedIn(),
      "Content-Type": "application/json"
    }) 
    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "first": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "last": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
        },
        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.liststaff, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
          if (res.statuscode == 200) {
            ////console.log(res.data);
            that.staffdata = res.data;
            // this.viewtable = true;
            ////console.log(res.data);
            callback({
              recordsTotal: res.recordsTotal,
              recordsFiltered: res.recordsFiltered,
              data: []
            });
          }
        });
      },
    };
  }
  //   liststaff() {
  //   const formdata = new FormData();
  //   formdata.append('token', config.tokenauth);
  //   this._auth.postdata(formdata, config.liststaff).subscribe((res: any) => {
  //     this.staffdata = res.data; 
  //     setTimeout(() => { 
  //       $('#editastaffDatatable').DataTable({
  //         pagingType: 'full_numbers',
  //         language: {
  //           paginate: { "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', 
  //                       "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
  //                       "first":'GHF', "last" :"GHFH"}, 
  //           search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
  //           searchPlaceholder: "Search...",
  //           lengthMenu: "Results :  _MENU_",
  //         },
  //         pageLength: 10,
  //         processing: true,
  //         lengthMenu: [10, 25, 50, 100],
  //        });
  //     }, 1);
  //   });
  // }
  editstaff_Model(data :any,details : any) { 
    //console.log(details.id);
    const formdata = new FormData();
    formdata.append('userid', details.id);
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getstaff).subscribe((res: any) => {
      this.staffID = res.data.userid; 
      ////console.log(res.data);
      this.modal.open(data); 
      this.editStaff.reset();
      this.remark=res.data.remarks;
      this.editStaff.controls["name"].setValue(res.data.name);  
      this.editStaff.controls["email"].setValue(res.data.email);  
      this.editStaff.controls["phoneNo"].setValue(res.data.phone);
      this.editStaff.controls["activestatus"].setValue(res.data.status); 
      this.activestatus = res.data.status;
      // this.editStaff.controls["gender"].setValue(res.data.gender);
      // this.editStaff.controls["activestatus"].setValue(res.data.status);
    })
  }
  Onupdatestaff() {
    if (!this.editStaff.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name', this.editStaff.get('name').value);   
      formdata.append('email', this.editStaff.get('email').value);
      formdata.append('phone', this.editStaff.get('phoneNo').value);  
      formdata.append('remarks', this.editStaff.get('remarks').value);
      formdata.append('userid', this.staffID);
      formdata.append('status', this.editStaff.get('activestatus').value);
      this._auth.postdata(formdata, config.editstaff).subscribe((res: any) => {
        if (res.statuscode == 200) { 
          this.modal.close(); 
          Swal.fire({ 
            title: res.message,
            icon: 'success'
          });
          this.editStaff.reset();
          const formdata = new FormData();
          formdata.append('token', config.tokenauth);
          this._auth.postdata(formdata, config.liststaff).subscribe((res: any) => {
            this.staffdata = res.data; 
            this.dtTrigger.next();
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message  
          }) 
        }
      })
    }
  } 
 
  get f() { return this.editStaff.controls; }
}