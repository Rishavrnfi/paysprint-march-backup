import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-staff',
  templateUrl: './add.staff.component.html',
  styleUrls: ['./add.staff.component.css']
})
export class StaffComponent implements OnInit {
  addStaff :any = FormGroup; 
  activestatus:any;
  gender:any;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route:Router
  ) { 
      this.addStaff = this.fb.group({
        name        : ['',  [Validators.required,Validators.pattern('[a-zA-Z][a-zA-Z ]+')]],   
        email      :['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        phoneNo    :['',[Validators.required,Validators.pattern('[6789][0-9]{9}')]], 
        activestatus:['',[Validators.required]]
      });
    }

  ngOnInit(): void {
    ////console.log( )
  }
 


  async add_staff(){
    if (!this.addStaff.valid){
      return;
    }else{
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name',this.addStaff.get('name').value);   
      formdata.append('email',this.addStaff.get('email').value);
      formdata.append('phone',this.addStaff.get('phoneNo').value);  
      formdata.append('status',this.addStaff.get('activestatus').value);  
      this._auth.postdata(formdata,config.addstaff).subscribe((res: any) => { 
        if (res.statuscode == 200) {
          Swal.fire({ 
            title: res.message,
            icon: 'success'
          });
        this.addStaff.reset();
            
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message 
          }) 
        }
      })
    }
  }

   
  get f() { return this.addStaff.controls;}
}
