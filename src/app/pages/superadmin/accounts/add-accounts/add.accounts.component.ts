import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-accounts',
  templateUrl: './add.accounts.component.html',
  styleUrls: ['./add.accounts.component.css']
})
export class AccountsComponent implements OnInit {
  addAccounts :any = FormGroup; 
  submitted = false;
  activestatus:any; 
  admintype:any;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route:Router
  ) { 
    this.addAccounts = this.fb.group({
      name: ['', [Validators.required]],  
      email      :['',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo    :['',[Validators.required,Validators.pattern('[6789][0-9]{9}')]],  
      activestatus:['',[Validators.required]],
      admintype:['',[Validators.required]]
    });
  }

  ngOnInit(): void {
  }
  async add_accounts(){
    this.submitted = true;
    if (this.addAccounts.invalid){
      return;
    }else{
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name',this.addAccounts.get('name').value); 
      formdata.append('email',this.addAccounts.get('email').value);
      formdata.append('phone',this.addAccounts.get('phoneNo').value);  
      formdata.append('status',this.addAccounts.get('activestatus').value);  
      formdata.append('is_approver',this.addAccounts.get('admintype').value);  
      this._auth.postdata(formdata,config.addAccounts).subscribe((res: any) => { 
        if (res.statuscode == 200) {
          this.addAccounts.reset(); 
          Swal.fire({
            title: 'Hurray!!',
            text: res.message,
            icon: 'success'
          });
          
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message,
            text: 'Something went wrong!' 
          }) 
        }
      })
    }
  }
  get f() { return this.addAccounts.controls;}
}
