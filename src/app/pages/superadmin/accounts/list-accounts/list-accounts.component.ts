import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service'
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataTableDirective } from 'angular-datatables';
declare let $: any;

class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-list-accounts',
  templateUrl: './list-accounts.component.html',
  styleUrls: ['./list-accounts.component.css']
})
export class ListAccountsComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtElement: any =  DataTableDirective;
  formdata: any;
  AccountList: any;
  editAccounts: any = FormGroup;
  userID: any;
  activestatus: any;
  remark: any;
  admintype: any;
  dtTrigger: Subject<any> = new Subject<any>();
  constructor(
    private _auth: ApiService,
    private fb: FormBuilder,
    private modal: CustomeModalService,
    private http: HttpClient
  ) {
    // this.listAccounts();
    this.editAccounts = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      phoneNo: ['', [Validators.required, Validators.pattern('[6789][0-9]{9}')]],
      username: ['', [Validators.required]],
      remarks: ['', [Validators.required]],
      activestatus: ['', [Validators.required]],
      admintype: ['', [Validators.required]]
    });
    this.formdata = {
      'token': config.tokenauth,
    }
  }

  ngOnInit(): void {
    let headers = new HttpHeaders({
      'Authkey': config.Authkey,
      'Authtoken': this._auth.isLoggedIn(),
      "Content-Type": "application/json"
    }) 
    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "first": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "last": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
        },

        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.accountList, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
          if (res.statuscode == 200) {
            ////console.log(res.data);
            that.AccountList = res.data;
            // this.viewtable = true;
            ////console.log(res.data);
            callback({
              recordsTotal: res.recordsTotal,
              recordsFiltered: res.recordsFiltered,
              data: []
            });
          }
        });
      },
    };
  }
 
  async editaccount(data: any, details: any) {
    this.modal.open(data);
    this.editAccounts.reset();
    const formdata = new FormData();
    formdata.append('userid', details.id);
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getaccount).subscribe((res: any) => {
      //console.log(res.data.userid);
      this.userID = res.data.userid;
      this.editAccounts.controls["name"].setValue(res.data.name);
      this.editAccounts.controls["email"].setValue(res.data.email);
      this.editAccounts.controls["phoneNo"].setValue(res.data.phone);
      this.editAccounts.controls["username"].setValue(res.data.username);
      this.activestatus = res.data.status;
      this.remark = res.data.remarks;
      this.admintype = res.data.is_approver;
    })

  }
  async edit_accounts() {
    if (!this.editAccounts.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('name', this.editAccounts.get('name').value);
      formdata.append('email', this.editAccounts.get('email').value);
      formdata.append('phone', this.editAccounts.get('phoneNo').value);
      formdata.append('username', this.editAccounts.get('username').value);
      formdata.append('remarks', this.editAccounts.get('remarks').value);
      formdata.append('userid', this.userID);
      formdata.append('status', this.editAccounts.get('activestatus').value);
      formdata.append('is_approver', this.editAccounts.get('admintype').value);
      this._auth.postdata(formdata, config.editAccount).subscribe((res: any) => {
        if (res.statuscode == 200) {
          this.modal.close();
          this.editAccounts.reset(); 
          Swal.fire({
            title: 'Hurray!!',
            text: res.message,
            icon: 'success'
          });
          this.rerender();
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message,
            text: 'Something went wrong!'
          })
        }
      })
    }
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => { 
      dtInstance.draw(); 
      this.dtTrigger.next();
    });
  }
  get f() { return this.editAccounts.controls; }
}
