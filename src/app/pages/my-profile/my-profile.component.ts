import { Component, OnInit } from '@angular/core';
import { filter } from 'jszip';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { EncodeDecode } from 'src/app/_helpers/encode-decode';
declare function myTp(): any;





@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {
  userData: any; 
  details:any;
  CompanyDetails:any; 
  vaaccount : any;
  public userdetails: any;
  constructor(
    private _auth: ApiService,
  ) { 
    this.details = this._auth.Getsessiondata();  
  }

  ngOnInit(): void {
    myTp();
    const encode:any = EncodeDecode('n',localStorage.getItem('LoginDetails'));
    const _permissionlist:any = JSON.parse(encode);
    this.userdetails = _permissionlist.usertype;  
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getProfileDtl)
      .subscribe((res: any) => {
        if (res.statuscode == 200) {
          this.userData = res.data;
          this.vaaccount  =res.data.va_account; 
          ////console.log(this.vaaccount);
        }
      }); 
    this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
        
       this.CompanyDetails = res.data.filter((dt: any) => {
        return dt.status == 1;
      }) 
      
      setTimeout(() => {
        $('#cmpnyapideatils').DataTable({
          pagingType: 'full_numbers',
          lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
          processing: true,
        });
      }, 1);  
      ////console.log(res.data); 
    });
  }

   
    

}
