import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';
import { ApiService } from 'src/app/service/api.service';
@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.css']
})
export class OnboardingComponent implements OnInit {
  formDt: FormGroup;
  _expression: any = 1;
  isPanValid: boolean = false;
  imageSrc: any;
  panFiles: any;
  aadhaarFrontFls: any;
  aadhaarBackFls: any;
  constructor(private fb: FormBuilder,private _auth:ApiService,private http: HttpClient) {
    this.formDt = this.fb.group({});
    this.verifyUrl();
  }

  ngOnInit(): void {
    this.formDt = this.fb.group({
      mobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[0-9]{10}')]],
      pan: [{ value: '', disabled: true }, [Validators.required, Validators.minLength(10), Validators.pattern('[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}')]],
      name: [{ value: '', disabled: true },],
      dob: [{ value: '', disabled: true },],
      fname: [{ value: '', disabled: true },],
      panFile: ['', [Validators.required]],
      aadhaarFrontFl: ['', [Validators.required]],
      aadhaarBackFl: ['', [Validators.required]],
      hpanFile: ['', [Validators.required]],
      haadhaarFrontFl: ['', [Validators.required]],
      haadhaarBackFl: ['', [Validators.required]]
    });
  }

    verifyUrl(){
      const formdata = new FormData();
          formdata.append('lat','28.95'); 
          formdata.append('lng','89.45'); 
            this._auth.postdata(formdata,config.onboardingverifyUrl).subscribe((res: any) => { 
              // if (res.stage == 1) {
              //   this.formDt.controls['pan'].enable();
              // }
              //console.log(res)
            }) 
    }
  
  verifyPan(val: any) {
    if (val.match('[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}')) {
      //console.log('ok', val);
      this.isPanValid = true;
      this.formDt.patchValue({
        "name": 'Navrose Singh ',
        "dob": '01/07/1992',
        "fname": 'Amarjit Singh',
      })
    } else {
      this.isPanValid = false;
    }
  }

  onFileSelect(event: any, name: string) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      //console.log(file);
      this.readURL(file, name)
    }
  }
  submit() {
    //console.log(this.formDt.value);
  }

  get f() { return this.formDt.controls; }


  readURL(file: any, name: any) {
    const reader = new FileReader();

    // if (event.target.files && event.target.files.length) {
    // const [file] = event.target.files;
    reader.readAsDataURL(file);

    reader.onload = () => {
      switch (name) {
        case 'panFile':
          //console.log(file.name);
          
          this.formDt.patchValue({ "hpanFile": file.name });
          const fd1 = reader.result as string;
          this.panFiles = fd1;
          //console.log(this.formDt.value);
          
          break;
        case 'aadhaarFrontFl':
          // this.formDt.patchValue({ "aadhaarFrontFl": file.name });
          const fd2 = reader.result as string;
          this.aadhaarFrontFls = fd2;
          break;
        case 'aadhaarBackFl':
          // this.formDt.patchValue({ "aadhaarBackFl": file.name });
          const fd3 = reader.result as string;
          this.aadhaarBackFls = fd3;
          break;

        default:
          break;
      }

    };

    // }
    // if (input.files && input.files[0]) {
    //   var reader = new FileReader();

    //   reader.onload = function (e) {
    //     $('#imageResult').attr('src', event.target.files[0];
    //   };
    //   reader.readAsDataURL(input.files[0]);
    // }
  }

 

 

}
