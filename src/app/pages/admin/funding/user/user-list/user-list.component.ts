import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  minDate!: Date;
  maxDate!: Date;
  min: any = '';
  max: any = '';
  userAutoComData: any;
  userKeyword = 'userdetails';
  isLoadingResult: boolean = false;
  prevVal: any;
  userdt: any;
  showUserDtl: boolean = false;
  showActionType: any;
  form_recv: FormGroup = new FormGroup({});
  bankAutoComData: any = [];
  bankKeyword = 'name';
  CompanyBankDtl: any;
  userId: any;

  form_debiter: FormGroup = new FormGroup({});
  currentBal: any;
  historyDt: any = null;
  requestLst: any[] = [];
  rejectedLst: any[] = [];
  creditLst: any[] = [];
  recievingLst: any[] = [];

  constructor(private _auth: ApiService, private datePipe: DatePipe) {
    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate());
  }

  ngOnInit(): void {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
      this.CompanyBankDtl = res.data;
      this.bankAutoComData = this.CompanyBankDtl;
      // this.CompanyDetails = res.data.filter((dt: any) => {
      //   return dt.status == 1;
      // })
    });
  }


  selectEvent(item: any) {
    // do something with selected item
    //console.log(item);
    this.showUserDtl = true;
    this.userdt = item;
    this.prevVal = item.userdetails
    this.userId = item.userid;
    this.currentBal = item.balance;
  }
  getServerResponse(val: any) {
    if (val != this.prevVal) {
      this.prevVal = val;
      this.InitFn();
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      this.isLoadingResult = true;
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this._auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        this.isLoadingResult = false;
        if (res['data'] == undefined) {
          this.userAutoComData = [];
        } else {
          this.userAutoComData = res['data'];
        }
      });
    }

  }
  onChangeSearch(val: any) {
  }

  searchCleared() {
    this.userAutoComData = [];
    this.InitFn();
  }

  putRecvFn() {
    this.showActionType = 1;
  }
  setBankId(item: any) {
    this.form_recv.patchValue({
      "bankid": item.id
    })
  }
  bankSearchCleared() {
    this.form_recv.patchValue({
      "bankid": null
    })
  }
  recvSubmit() {
    const formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.userId);
    formdata.append('amount', this.form_recv.get('amount')?.value);
    formdata.append('charges', this.form_recv.get('charges')?.value);
    formdata.append('bankid', this.form_recv.get('bankid')?.value);
    formdata.append('remarks', this.form_recv.get('remark')?.value);
    this._auth.postdata(formdata, config.adminReceiving).subscribe((res: any) => {
      if (res.statuscode == 200) {
        // Swal.fire(res.message);
        Swal.fire(res.message);
        this.formInit();
      }
    });
  }

  debitFundFn() {
    this.showActionType = 2;
  }

  debitSubmit() {
    const formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('debitorid', this.userId);
    formdata.append('amount', this.form_debiter.get('amount')?.value);
    formdata.append('is_credit_decrease', this.form_debiter.get('is_credit_decrease')?.value);
    formdata.append('otp', this.form_debiter.get('otp')?.value);
    formdata.append('remarks', this.form_debiter.get('remark')?.value);
    this._auth.postdata(formdata, config.adminDebit).subscribe((res: any) => {
      if (res.statuscode == 200) {
        // Swal.fire(res.message);
        Swal.fire(res.message);
        this.formInit();
      }
    });
  }

  historyFn() {
    this.showActionType = 3;
  }

  hisSearch() {
    // this.datePipe.transform(this.min, 'yyyy-MM-dd')
    // this.datePipe.transform(this.max, 'yyyy-MM-dd')
    this.histInit();
    const formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('userid', this.userId);
    formdata.append('startdate', this.datePipe.transform(this.min, 'yyyy-MM-dd'));
    formdata.append('enddate', this.datePipe.transform(this.max, 'yyyy-MM-dd'));
    // formdata.append('bankid', '');
    // formdata.append('amount', '');
    // formdata.append('refnumber', '');
    this._auth.postdata(formdata, config.adminHistory).subscribe((res: any) => {
      if (res.statuscode == 200) {
        Swal.fire(res.message);
        this.historyDt = res.data;
        this.requestLst = this.historyDt['request'] !== undefined && this.historyDt['request'] !== null ? this.historyDt['request'] : [];
        this.rejectedLst = this.historyDt['rejected'] !== undefined && this.historyDt['rejected'] !== null ? this.historyDt['rejected'] : [];
        this.creditLst = this.historyDt['credit'] !== undefined && this.historyDt['credit'] !== null ? this.historyDt['credit'] : [];
        this.recievingLst = this.historyDt['recieving'] !== undefined && this.historyDt['recieving'] !== null ? this.historyDt['recieving'] : [];

      }
    });
  }

  formInit() {
    this.showActionType = 0;

    this.form_recv = new FormGroup({});
    this.form_recv = new FormGroup({
      bankname: new FormControl('', Validators.required),
      bankid: new FormControl('', Validators.required),
      amount: new FormControl('', [Validators.required, Validators.min(0)]),
      charges: new FormControl('', [Validators.required, Validators.min(0)]),
      remark: new FormControl('', Validators.required),
    })

    this.form_debiter = new FormGroup({});
    this.form_debiter = new FormGroup({
      amount: new FormControl('', [Validators.required, Validators.min(0), this.setMinValidator.bind(this)]),
      is_credit_decrease: new FormControl('0'),
      otp: new FormControl('', [Validators.required]),
      remark: new FormControl('', Validators.required),
    })

    this.min = null;
    this.max = null;

    this.histInit()
  }
  histInit() {
    this.historyDt = null;
    this.requestLst = [];
    this.rejectedLst = [];
    this.creditLst = [];
    this.recievingLst = [];
  }
  setMinValidator(control: FormControl): { [key: string]: any } | null {
    let bal = +this.currentBal;
    if (control?.value && control?.value >= bal) {
      return { 'enterValidAmount': true }
    }
    return null;
  };

  InitFn() {
    this.showUserDtl = false;
    this.userdt = '';


    this.showActionType = null;
    this.form_recv.reset('');



    this.formInit();
  }


}
