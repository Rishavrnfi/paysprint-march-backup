import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service';

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.css']
})
export class AuthorizeComponent implements OnInit {
  authrizorLst: any;
  reqLst: any;
  singleApproverAction = false;
  detailDataObj: any;
  formTypeOf = 'authorizer';
  constructor(
    private _auth: ApiService,
    private _CommonModule: CommonService
  ) { }

  ngOnInit(): void {
   // //console.log('Ap Com');
    this.getPendingAuthOrizeLst();
    this.reqLst = this._CommonModule.getRequestType();
    ////console.log(this.reqLst);

  }

  getPendingAuthOrizeLst() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getPendingAuthoLst)
      .subscribe((res: any) => {
        this.authrizorLst = res.data;
        setTimeout(() => {
          $('#apvrLst').DataTable({
            pagingType: 'full_numbers',
            language: {
              paginate: { "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', 
                          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                          "first":'GHF', "last" :"GHFH"}, 
              search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
              searchPlaceholder: "Search...",
              lengthMenu: "Results :  _MENU_",
            },
            pageLength: 10,
            processing: true,
            lengthMenu: [10, 25, 50, 100]
          });
        }, 1);
      });
  }
  onSingleAuthAction(data: any) {

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('reqid', data.reqid);
    this._auth.postdata(formdata, config.getPendingAutho).subscribe((res: any) => {
      this.detailDataObj = res.data;
      this.singleApproverAction = true;
    });
  }
  
  clickFromChild() {
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this._auth.postdata(formdata, config.getPendingAuthoLst)
      .subscribe((res: any) => {
        this.detailDataObj = res.data;
        this.singleApproverAction = false;
      });
  }
}
