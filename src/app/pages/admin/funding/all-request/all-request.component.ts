import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { CommonService } from 'src/app/_helpers/common/common.service'; 
import { CustConfg } from 'src/app/_helpers/common/custom-datepicker/ngx-datePicker-CustConfg';
import { CustomeModalService } from 'src/app/_helpers/common/custome-modal/custome-modal.service';
import { environment } from 'src/environments/environment.prod';
class DataTablesResponse {
  data: any[] = [];
  draw: any;
  recordsFiltered: any;
  recordsTotal: any;
}
@Component({
  selector: 'app-all-request',
  templateUrl: './all-request.component.html',
  styleUrls: ['./all-request.component.css']
})
export class AllRequestComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement!: DataTableDirective;
  minDate!: Date;
  maxDate!: Date;
  dtOptions: DataTables.Settings = {};

  public viewtable: boolean = false;
  public viewtbl: boolean = false;
  formdata: any;
  form: FormGroup = new FormGroup({});
  allRequestLst: any;
  //Creditor----

  userAutoComDataCrd: any;
  userKeywordCrd = 'userdetails';
  prevValCrd: any = '';
  //----

  //Bank ID----

  userAutoComDataBid: any;
  userKeywordBid = 'name';
  prevValBid: any = '';
  //----
  usertype: any;
  requestTypeLst: any = [];
  bsCustConfg = CustConfg;
  constructor(
    private http: HttpClient,
    private auth: ApiService,
    private datepipe: DatePipe,
    private _CommonModule: CommonService,
    private modal: CustomeModalService
  ) {

    const DateValue = new Date();
    this.minDate = new Date(1950, 1, 1);
    this.maxDate = new Date(DateValue.getFullYear(), DateValue.getMonth(), DateValue.getDate());
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      selectdate: new FormControl('', [Validators.required]),
      creditor: new FormControl(''),
      bankid: new FormControl(''),
      status: new FormControl(''),
      type: new FormControl(''),
      is_exceptional: new FormControl(''),
    })
    let headers = new HttpHeaders({
      'Authkey': 'MWQyMmUzNWY4YjhlNjY2NWJjM2EzZjY0NjNhZWM0ZTk=',
      'Authtoken': this.auth.isLoggedIn(),
      "Content-Type": "application/json"
    })

    const that = this;
    let options = { headers: headers };
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        paginate: {
          "previous": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
          "next": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
          "first": 'GHF', "last": "GHFH"
        },

        search: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
        searchPlaceholder: "Search...",
        lengthMenu: "Results :  _MENU_",
      },
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
      serverSide: true,
      processing: true,
      columnDefs: [
        // {
        //   "targets": [2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        //   "orderable": false,
        // },
      ],
      ajax: (dataTablesParameters: any, callback) => {
        if (this.viewtbl) {
          that.http.post<DataTablesResponse>(environment.apiBaseUrl + config.fundingAllrequest, Object.assign(dataTablesParameters, this.formdata), options).subscribe((res: any) => {
            if (res.statuscode == 200) {
              that.allRequestLst = res.data ?? [];
              this.viewtable = true;
              callback({
                recordsTotal: res.recordsTotal,
                recordsFiltered: res.recordsFiltered,
                data: []
              });
            }
          });
        }
      },
      //columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
    };
    this.usertype = this.auth.Getsessiondata().usertype;
    this.requestTypeLst = this._CommonModule.getRequestType1();
  }

  radiocheck(event: any) {
    if (event.target.value) {
      this.form.get('searchvalue')?.enable();
    }
  }
  ngOnDestroy(): void {
    // We remove the last function in the global ext search array so we do not add the fn each time the component is drawn
    // /!\ This is not the ideal solution as other components may add other search function in this array, so be careful when
    // handling this global variable
    $.fn['dataTable'].ext.search.pop();
  }
  OnCallAllReq(): void {
    this.viewtbl = true;

    let startdate: any = this.transform(this.form.get('selectdate')?.value[0]);
    let enddate: any = this.transform(this.form.get('selectdate')?.value[1]);
    this.formdata = {
      'token': 'e090c25187ee2b3f9f1f8a02747356641',
      'startdate': startdate,
      'enddate': enddate,
      'searchby': this.prevValCrd,
      'bankid': this.prevValBid,
      'status': this.form.get('status')?.value,
      'type': this.form.get('type')?.value,
      'is_exceptional': this.form.get('is_exceptional')?.value
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }
  transform(date: any) {
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }



  //Creditor------------
  getServerResponseCrd(val: any) {
    if (val != this.prevValCrd) {
      this.prevValCrd = val;
      // fetch remote data from here
      // And reassign the 'data' which is binded to 'data' property.
      //console.log(typeof val);
      let formdata: any = new FormData();
      formdata.append('token', config.tokenauth);
      formdata.append('Authkey', config.Authkey);
      formdata.append('search', val);
      this.auth.postdata(formdata, config.getuserBySearch).subscribe((res: any) => {
        this.userAutoComDataCrd = res['data'] ?? [];
        // if (res['data'] == undefined) {
        //   this.userAutoComDataCrd = [];
        // } else {
        //   this.userAutoComDataCrd = res['data'];
        // }
      });
    }

  }
  selectEventCrd(item: any) {
    // do something with selected item
    //console.log(item);
    this.prevValCrd = item.userid
  }
  searchClearedCrd() {
    //console.log('searchCleared');
    this.prevValCrd = '';
  }



  //Bank ID------------
  getServerResponseBid(val: any) {
    console.log(val);

    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    //console.log(typeof val);
    let formdata: any = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('Authkey', config.Authkey);
    formdata.append('search', val);
    this.auth.postdata(formdata, config.Companybanklist).subscribe((res: any) => {
      this.userAutoComDataBid = res['data'] ?? [];
      // if (res['data'] == undefined) {
      //   this.userAutoComDataBid = [];
      // } else {
      //   this.userAutoComDataBid = res['data'];
      // }
    });

  }
  selectEventBid(item: any) {
    // do something with selected item
    //console.log(item);
    this.prevValBid = item.id
  }
  searchClearedBid() {
    //console.log('searchCleared');
    this.prevValBid = '';
  }
  get f() {
    return this.form.controls
  }
}
