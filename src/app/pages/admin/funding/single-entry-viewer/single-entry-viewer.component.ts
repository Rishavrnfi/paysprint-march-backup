import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { LoaderService } from 'src/app/_helpers/common/loader.service';
import { LoadingServiceService } from 'src/app/service/loading-service.service';
import { CommonService } from 'src/app/_helpers/common/common.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-single-entry-viewer',
  templateUrl: './single-entry-viewer.component.html',
  styleUrls: ['./single-entry-viewer.component.css']
})
export class SingleEntryViewerComponent implements OnInit {
  @Input() detailData: any;
  @Input() formType: any;
  @Output() backtoApprover = new EventEmitter<string>();
  @Output() backtoAuthorizer = new EventEmitter<string>();
  reqLst: any;
  updateDtlForm: any = FormGroup;
  chargesReq = false;
  cashBackReq = false;
  preCharges: any;
  preCashBack: any;
  isshow:boolean=false;
  constructor(private _CommonModule: CommonService, private _auth: ApiService, private fb: FormBuilder, private loader:LoaderService) { }

  ngOnInit(): void {
     //console.log(this.detailData);
    this.updateDtlForm = this.fb.group({
      otp: [null, [Validators.required]],
      charges: [null],
      cash_back: [null],
      remarks: [null, [Validators.required]],
      exceptional: [0],
    });

    this.reqLst = this._CommonModule.getRequestType();
    this.preCharges = this.detailData.request.charges;
    this.preCashBack = this.detailData.request.cash_back;
    //console.log(this.detailData.request.charges);

  }
  goBakc() {
    this.resetForm();
    if (this.formType === 'approver') {
      this.backtoApprover.emit();
    }
    if (this.formType === 'authorizer') {
      this.backtoAuthorizer.emit();
    }
  }
  onSubmit(val: any) {
    if (val === 'btnApprove') {
      if (this.updateDtlForm.get('charges').value === '' || this.updateDtlForm.get('charges').value === null) {
        this.chargesReq = true;
        return;
      }
      if (this.updateDtlForm.get('cash_back').value === '' || this.updateDtlForm.get('cash_back').value === null) {
        this.cashBackReq = true;
        return;
      }
      this.onDtlApprove();
    }
    if (val === 'btnReject') {
      this.onDtlReject();
    }
  }
  onDtlApprove() {
    var approveUrlType = '';
    if (this.formType === 'approver') {
      approveUrlType = config.pendingApproverApprove;
    }
    if (this.formType === 'authorizer') {
      approveUrlType = config.pendingAuthoAuthorize;
    }


    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('reqid', this.detailData.request.reqid);
    formdata.append('otp', this.updateDtlForm.get('otp').value);
    formdata.append('charges', this.updateDtlForm.get('charges').value);
    formdata.append('cash_back', this.updateDtlForm.get('cash_back').value);
    formdata.append('remarks', this.updateDtlForm.get('remarks').value);
    formdata.append('markasexceptional', this.updateDtlForm.get('exceptional').value);
    this._auth.postdata(formdata, approveUrlType)
      .subscribe((res: any) => {
        if (res.statuscode == 200) {
          // Swal.fire(res.message);
          Swal.fire(res.message).then((result) => {
            if (result.isConfirmed) { 
              this.goBakc();
              this.loader.loaderEvent.emit(false);  
            }
          })
        }
        if (res.statuscode == 404) {
          Swal.fire(res.message);
          // Swal.fire(res.message).then((result) => {
          //   // if (result.isConfirmed) {
          //   //   this.goBakc();
          //   // }
          // })
        }

      });
  }
  onDtlReject() {
    //console.log(this.updateDtlForm.value);
    var approveUrlType = '';
    if (this.formType === 'approver') {
      approveUrlType = config.pendingapproverReject;
    }
    if (this.formType === 'authorizer') {
      approveUrlType = config.pendingauthoReject;
    }

    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    formdata.append('reqid', this.detailData.request.reqid);
    formdata.append('otp', this.updateDtlForm.get('otp').value);
    formdata.append('remarks', this.updateDtlForm.get('remarks').value);
    this._auth.postdata(formdata, approveUrlType)
      .subscribe((res: any) => {
        if (res.statuscode == 200) {
          // Swal.fire(res.message);
          Swal.fire(res.message).then((result) => {
            if (result.isConfirmed) {
              this.goBakc();
              this.loader.loaderEvent.emit(false);
            }
          })
        }
        if (res.statuscode == 404) {
          Swal.fire(res.message);
          // Swal.fire(res.message).then((result) => {
          //   // if (result.isConfirmed) {
          //   //   this.goBakc();
          //   // }
          // })
        }
      });
  }

  resetForm() {
    this.updateDtlForm.reset();
  }
  get f() { return this.updateDtlForm.controls; }
}
