import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleEntryViewerComponent } from './single-entry-viewer.component';

describe('SingleEntryViewerComponent', () => {
  let component: SingleEntryViewerComponent;
  let fixture: ComponentFixture<SingleEntryViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleEntryViewerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleEntryViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
