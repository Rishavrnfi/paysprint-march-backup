import { Component, OnInit, Version } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import Swal from 'sweetalert2';
import { EncodeDecode } from 'src/app/_helpers/encode-decode'
import { ConnectionService } from 'ng-connection-service'; 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  otp = {
    input1: '',
    input2: '',
    input3: '',
    input4: '',
  }
  timeLeft: number = 60;
  showModal: boolean = true;
  maxlength: any;
  totxt: any;
  alert: boolean = false;
  loading = true;
  error = '';
  message = '';
  submitted = false;
  logindata: any = FormBuilder;  
  longitute: string = '';
  latitute: string = '';
  isSubmitted: boolean = false;
  checkPassword: any = {
    username: '',
    password: ''
  };
  resendOtpData:any;
  status = 'ONLINE'; //initializing as online by default
  isConnected = true;
  constructor(
    private route: Router,
    private fb: FormBuilder,
    private _auth: ApiService,
    private connectionService: ConnectionService
  ) {
    this.logindata = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

  }

  ngOnInit(): void {
    this.getLocation();
    this.showModal = false;
    this._auth.isLoggedIn();
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        Swal.fire({
          title: 'You Are Online!!', 
          icon: 'success'
        }); 
      } else {
        Swal.fire({
          icon: 'error',
          title:'Check Internet Connection! No Internet',
        })   
      }
    });
  }


  login() {
    if (!this.logindata.valid) {
      return;
    } else {
      // //console.log([this.latitute,this.longitute]);
      if (this.latitute == "" && this.longitute == "") {
        Swal.fire({
          title: "Please allow the Location access! and You May Not Connected To Internet",
          icon: 'error', 
          confirmButtonText: `Okay`, 
          confirmButtonColor: '#3085d6',
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            window.location.reload();
          } 
        }) 
      } else {
        //this.submitted = true; 
        const formdata = new FormData();
        formdata.append('username', this.logindata.get('username').value);
        formdata.append('password', this.logindata.get('password').value);
        formdata.append('token', config.tokenauth);
        formdata.append('latitude', this.latitute);
        formdata.append('longitude', this.longitute);
        //this.loading = true;
        this._auth.postdata(formdata, config.login).subscribe((res: any) => {
          if (res.response == 2001) {
            Swal.fire({
              title: res.message, 
              icon: 'success'
            });
          } else if (res.firstlogin == 0) {
            this.checkPassword = {
              username: this.logindata.get('username').value,
              password: this.logindata.get('password').value
            };
            let decode: any = EncodeDecode(JSON.stringify(this.checkPassword), 'n');
            localStorage.setItem('Verifydetail', decode);
            this.route.navigate(['change-password']);
          } else if (res.twostep == 1) {  
            this.showModal = true; 
            this.logindata.disable()
          } else if (res.response == 200) {
            let decode: any = EncodeDecode(JSON.stringify(res), 'n');
            localStorage.setItem('LoginDetails', decode); 
            this.route.navigate(['dashboard']);
            //  this._auth.reloadPage();
          } else if (res.status === 2002) {
            Swal.fire({
              icon: 'error',
              title: res.message
            }) 
          } else {
            Swal.fire({
              icon: 'error',
              title: res.message
            });
          }
        } 
        )
      }
    }

  }
  getLocation() {
    this._auth.getLocationService().then(resp => {
      this.longitute = resp.lng;
      this.latitute = resp.lat;

    })
  }
  forgot_pwd() {
    this.route.navigate(['forgot-password']);
  }
  moveFocus() {
    const otpList = this.otp.input1 + '' + this.otp.input2 + '' + this.otp.input3 + '' + this.otp.input4;
    if (otpList.length == 4) {
      const formdata = new FormData();
      formdata.append('username', this.logindata.get('username').value);
      formdata.append('password', this.logindata.get('password').value);
      formdata.append('token', config.tokenauth);
      formdata.append('latitude', this.latitute);
      formdata.append('longitude', this.longitute);
      formdata.append('otp', otpList);
      this._auth.postdata(formdata, config.verify).subscribe((res: any) => {
        if (res.response == 200) {
          let decode: any = EncodeDecode(JSON.stringify(res), 'n');
          localStorage.setItem('LoginDetails', decode);
          this.route.navigate(['dashboard']);
        } else {
          this.otp = {
            input1: '',
            input2: '',
            input3: '',
            input4: '',
          }
          Swal.fire(res.message)
        }
      })
    }
  }
  resend_otp() {
    if (this.latitute == "" && this.longitute == "") {
      Swal.fire({
        title: 'Please allow the Location access!', 
        icon: 'error'
      });  
    } else {
      const formdata = new FormData(); 
      formdata.append('username', this.logindata.get('username').value);
      formdata.append('password', this.logindata.get('password').value);
      formdata.append('token', config.tokenauth);
      formdata.append('latitude', this.latitute);
      formdata.append('longitude', this.longitute);
      this._auth.postdata(formdata, config.resendotp).subscribe((res: any) => {
        if (res.response == 200) {
          Swal.fire({
            icon: 'success',
            title: res.message
          }) 
        } else {
          Swal.fire({
            icon: 'error',
            title: res.message
          }) 
        }
      }
      )
    }
  }

  
  get f() { return this.logindata.controls; }
}
