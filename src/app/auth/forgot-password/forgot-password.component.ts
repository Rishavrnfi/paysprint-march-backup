import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit { 
  forgotpassword : any = FormGroup;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route:Router
  ) {
    this.forgotpassword = this.fb.group({
      username: ['', [Validators.required]],
      panNumber: ['',[ Validators.required,Validators.minLength(10),Validators.maxLength(10)]]
    });
   }
  
  ngOnInit(): void {
  }

  Forgot_password(){
    if (!this.forgotpassword.valid){
      return;
      }else{
        const formdata = new FormData();
        formdata.append('username',this.forgotpassword.get('username').value);
        formdata.append('pannumber',this.forgotpassword.get('panNumber').value);
        formdata.append('token', config.tokenauth); 
        this._auth.postdata(formdata,config.ForgotPassword).subscribe((res: any) => { 
          if (res.response == 200) {
            Swal.fire({
              title: res.message, 
              icon: 'success'
            });
            this.route.navigate(['login']);
          }else{
            Swal.fire({
              icon: 'error',
              title: res.message
            }) 
          }
         
        })
      }
  }

  get f() { return this.forgotpassword.controls;}
}
