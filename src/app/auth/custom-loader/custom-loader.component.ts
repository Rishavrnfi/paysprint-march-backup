import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-loader',
  templateUrl: './custom-loader.component.html',
  styleUrls: ['./custom-loader.component.css']
})
export class CustomLoaderComponent implements OnInit {
  isLoading = false;
  constructor() { }

  ngOnInit(): void {
  }
  load() : void {
    this.isLoading = true;
    setTimeout( () => this.isLoading = false, 2000 );
  }
}
