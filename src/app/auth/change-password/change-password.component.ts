import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from "@angular/forms";
import { ApiService } from 'src/app/service/api.service';
import { config } from 'src/app/service/config';
import { EncodeDecode } from '../../_helpers/encode-decode'
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { PasswordReg } from 'src/app/_helpers/common/custom-validator/password-reg';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  showErrorBox: boolean = false;

  changePass: any = FormGroup;
  private sesion_data: any;
  private username: any;
  private password: any;
  constructor(
    private fb: FormBuilder,
    private _auth: ApiService,
    private route: Router
  ) {
    this.changePass = this.fb.group({
      // password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]],
      password: ['', [Validators.required, PasswordReg.patternValidator(/\d/, {
        hasNumber: true
      }),
      // check whether the entered password has upper case letter
      PasswordReg.patternValidator(/[A-Z]/, {
        hasCapitalCase: true
      }),
      // check whether the entered password has a lower case letter
      PasswordReg.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      // check whether the entered password has a special character
      PasswordReg.patternValidator(
        /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
        {
          hasSpecialCharacters: true
        }
      ),
      Validators.minLength(8)]],
      confirmPassword: ['', Validators.required],
    }, {
      validator: PasswordReg.passwordMatchValidator
    });
  }

  ngOnInit(): void {
  this.getverifyDetails();
  }

  Change_password() {
    if (!this.changePass.valid) {
      return;
    } else {
      const formdata = new FormData();
      formdata.append('new_password', this.changePass.get('password').value);
      formdata.append('confirm_password', this.changePass.get('confirmPassword').value);
      formdata.append('token', config.tokenauth);
      formdata.append('username', this.username);
      formdata.append('password', this.password);
      this._auth.postdata(formdata, config.changePass).subscribe((res: any) => {
        if (res.response == 200) {
          Swal.fire({
            title: res.message,
            icon: 'success'
          });
          localStorage.removeItem('Verifydetail');
          this.route.navigate(['login']);

        } else {
          Swal.fire({
            icon: 'error',
            title: res.message
          })
        }

      })
    }
  }
  getverifyDetails() {
    if (typeof (localStorage.getItem('Verifydetail')) !== 'undefined' && localStorage.getItem('Verifydetail') !== '') {
      let decode: any = EncodeDecode('n', localStorage.getItem('Verifydetail'));
      let data: any = JSON.parse(decode);
      this.username = data.username;
      this.password = data.password;
    } else {
      this.route.navigate(['login']);
    }
  }
  get f() { return this.changePass.controls; }

}