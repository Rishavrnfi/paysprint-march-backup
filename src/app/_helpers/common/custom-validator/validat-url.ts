import { analyzeFileForInjectables } from "@angular/compiler";
import { AbstractControl, ValidatorFn } from "@angular/forms";

export class ValidatUrl {

public static startWith(arr: any[]): ValidatorFn {
return (control: AbstractControl): { [key: string]: boolean } | null => {
let valid = false;
arr.forEach(element => {

if (control?.value != null && !control?.value.startsWith(element)) {
valid = true;
}
});

return valid ? { validUrl: true } : null;
};
}
}