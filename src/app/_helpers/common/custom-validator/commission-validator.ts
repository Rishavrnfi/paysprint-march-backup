import { AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

// setup simple regex for white listed characters
const validCharacters = /[^\s\w,.:&\/()+%'`@-]/;


export class CustomValidators extends Validators {

  public static firstMinAmount(min: number = 100): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value !== undefined && (control.value < min)) {
        return { 'firstMinAmountRange': true };
      }
      return null;
    };
  }
  public static lastMaxAmount(max: number = 10000): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value !== undefined && (control.value > max)) {
        return { 'lastMaxAmountRange': true };
      }
      return null;
    };
  }

  public static checkLastRec(): ValidatorFn {
    return (formArray: AbstractControl): ValidationErrors | null => {
      const vl = formArray.get('mainGroup')?.value
      for (let i = 0; i < vl.length; i++) {
        const element = vl[i].arr;
        for (let j = 0; j < element.length; j++) {
          const sub = element[j];
          if (+element[element.length - 1].slab_max === 10000) {
            break;
          }
        }
      }
      return null;
    }
  };
  public static compairAmount(control: FormControl) {
    if (control.value && control.value.length > 0) {
      const matches = control.value.match(validCharacters);
      return matches
        && matches.length ? { 'not_allowed_characters': matches } : null;
    } else {
      return null;
    }
  }

  public static validateMinAndMax(min: string, max: string): ValidatorFn {
    return (formGroup: AbstractControl): ValidationErrors | null => {
      let minVal = (formGroup.get(min) as FormControl)?.value;
      let maxVal = (formGroup.get(max) as FormControl)?.value;
      if (minVal === maxVal) {
        return null;
      }
      if (minVal > maxVal) {
        return { minMaxError: true };
      }
      return null;
    };
  }
  public static is_fixedValue(): ValidatorFn {
    return (formControl: AbstractControl): ValidationErrors | null => {

      let valueVal = formControl.get('value') as FormControl;
      let is_fixedVal = formControl.get('is_fixed') as FormControl;
      // //console.log(valueVal?.value);
      // //console.log(formControl?.value);
      if (is_fixedVal?.value === false && (valueVal?.value > 0.99)) {
        is_fixedVal?.setErrors({ 'trueError': true })
        // return null;
      } else {
        is_fixedVal?.setErrors(null)
      }
      return null;
    };
  }
  static compairRecordValidator(): ValidatorFn {
    return (formArray: AbstractControl): ValidationErrors | null => {
      for (let i = 0; i < formArray.value.length; i++) {
        if (i > 0) {
          const current_min = +formArray.value[i].slab_min;
          const prev_max = +formArray.value[i - 1].slab_max;
          if ((current_min <= prev_max) || (current_min > (prev_max + 1))) {
            return { MinLengthArray: true };
          }
        }
      }
      return null;
    }
  };

  static firstRowMin(fixedAmt: number = 100): ValidatorFn {
    return (formArray: AbstractControl): ValidationErrors | null => {
      let iterations = (((formArray.parent as FormGroup)?.controls['arr'] as FormArray)?.controls[0] as FormGroup)?.controls['slab_min'] as FormControl;
      if (iterations?.value && iterations?.value != fixedAmt) {
        iterations?.setErrors({ 'firstMinAmountRange': true })
      }
      return null;
    }
  };

  static lastRowMax(fixedAmt: number = 10000): ValidatorFn {
    return (formArray: AbstractControl): ValidationErrors | null => {
      let len = ((formArray.parent as FormGroup)?.controls['arr'] as FormArray)?.value.length;
      let iterations = (((formArray.parent as FormGroup)?.controls['arr'] as FormArray)?.controls[len - 1] as FormGroup)?.controls['slab_max'] as FormControl;
      if (iterations?.value && iterations?.value != fixedAmt) {
        ((formArray.parent as FormGroup)?.controls['validCheck'] as FormArray)?.setErrors({ 'lastMaxAmount': true })
      } else {
        ((formArray.parent as FormGroup)?.controls['validCheck'] as FormArray)?.setErrors(null)
      }
      return null;
    }
  };
}