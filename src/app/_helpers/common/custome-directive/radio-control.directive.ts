import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import Swal from 'sweetalert2';

@Directive({
  selector: '[appRadioControl]'
})
export class RadioControlDirective {
  @Input('appRadioControl') isConfirmShow: any;

  constructor(private eleRef: ElementRef, private renderer: Renderer2) {
    this.isConfirmShow === undefined ? false : this.isConfirmShow;
  }

  @HostListener('click', ['$event']) mouseleave(e: Event) {

    let preVal = this.eleRef.nativeElement.closest('.note-content').children[0].value;
    let newVal = this.eleRef.nativeElement.value;


    if (preVal != newVal) {
      if (this.isConfirmShow) {
        Swal.fire({
          title: 'Do you want to save the changes?',
          showDenyButton: true,
          icon: 'warning',
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          denyButtonText: `Don't save`,
          confirmButtonText: 'Yes, Submit it!',
          allowOutsideClick: false,
          allowEscapeKey: false,

        }).then((result) => {
          if (result.isConfirmed) {
            this.eleRef.nativeElement.closest('.note-content').children[0].value = newVal;
            this.eleRef.nativeElement.closest('.note-content').children[0].click();

          } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info');
            this.eleRef.nativeElement.checked = false;
            this.eleRef.nativeElement.closest('.note-content').children[2].querySelectorAll('[value="' + preVal + '"]')[0].checked = true;
          }
        })
      } else {
        this.eleRef.nativeElement.closest('.note-content').children[0].value = newVal;
        this.eleRef.nativeElement.closest('.note-content').children[0].click();
      }


    }

  }
}
