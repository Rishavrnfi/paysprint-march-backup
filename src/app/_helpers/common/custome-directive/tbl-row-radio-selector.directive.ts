import { ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { Directive } from '@angular/core';

@Directive({
  selector: '[appTblRowRadioSelector]'
})
export class TblRowRadioSelectorDirective {
  @Input() defaultRowColor = '#f925354f';
  constructor(private eleRef: ElementRef, private renderer: Renderer2) { }

  @HostListener('click', ['$event']) mouseleave(e: Event) {
    let parent = this.renderer.parentNode(this.eleRef.nativeElement).children;
    for (let index = 0; index < parent.length; index++) {
      const element = parent[index];
      this.renderer.setStyle(element, 'background-color', 'transparent')
    }
    this.renderer.setStyle(this.eleRef.nativeElement, 'background-color', this.defaultRowColor);
    this.eleRef.nativeElement.children[0].children[0].click();
  }
}
