import { Injectable, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DatatableServiceService {
  @ViewChild(DataTableDirective, {static: false})
  dtOptions: DataTables.Settings = {}; 
  dtElement: any =  DataTableDirective; 
  //dtTrigger: Subject<any> = new Subject();
  dtTrigger:any =  new Subject<any>();
  isDtInitialized:boolean = false;
  constructor() { }
  

  reRender() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
      
    };
  }

  rerenderData(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.draw();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  
}
