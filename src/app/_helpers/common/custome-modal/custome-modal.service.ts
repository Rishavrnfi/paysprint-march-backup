import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
providedIn: 'root'
})
export class CustomeModalService {
defSize = 'xl'
constructor(private modalService: NgbModal) { }
open(content: any, setSize?: any) {
this.defSize = (setSize == undefined || setSize == '' || setSize == null ? this.defSize : setSize);
let ngbModalOptions: NgbModalOptions = {
backdrop: 'static',
keyboard: false,
size: this.defSize,
windowClass: 'animated zoomInUp custo-zoomInUp'
};
////console.log(ngbModalOptions);
const modalRef = this.modalService.open(content, ngbModalOptions);
}
close() {
this.modalService.dismissAll();
}

}