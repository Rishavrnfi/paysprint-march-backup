import { TestBed } from '@angular/core/testing';

import { CustomeModalService } from './custome-modal.service';

describe('CustomeModalService', () => {
  let service: CustomeModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomeModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
