import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth.guard';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { LoginComponent } from './auth/login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AccountsComponent } from './pages/superadmin/accounts/add-accounts/add.accounts.component';
import { StaffComponent } from './pages/superadmin/staff/add-staff/add.staff.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ListStaffComponent } from './pages/superadmin/staff/list-staff/list-staff.component';
import { ListAccountsComponent } from './pages/superadmin/accounts/list-accounts/list-accounts.component';
import { AddAsmComponent } from './pages/superadmin/asm/add-asm/add-asm.component';
import { ListAsmComponent } from './pages/superadmin/asm/list-asm/list-asm.component';
import { ListCompanyBankComponent } from './pages/superadmin/company-bank/list-company-bank/list-company-bank.component';
import { AddCompanyBankComponent } from './pages/superadmin/company-bank/add-company-bank/add-company-bank.component';
import { AddBillComponent } from './pages/superadmin/operator/bill/add-bill/add-bill.component';
import { ViewOperatorBillComponent } from './pages/superadmin/operator/bill/view-operator-bill/view-operator-bill.component';
import { AddRechargeComponent } from './pages/superadmin/operator/recharge/add-recharge/add-recharge.component';
import { ViewRechargeComponent } from './pages/superadmin/operator/recharge/view-recharge/view-recharge.component';
import { ApproverComponent } from './pages/admin/funding/approver/approver.component';
import { TransferComponent } from './pages/superadmin/company-bank/transfer/transfer.component';
import { StatementComponent } from './pages/superadmin/company-bank/statement/statement.component';
import { ApiPartnerComponent } from './pages/superadmin/api-partner/add-api-partner/api-partner.component';
import { ViewApiPartnerComponent } from './pages/superadmin/api-partner/view-api-partner/view-api-partner.component'; 
import { AuthorizeComponent } from './pages/admin/funding/authorize/authorize.component';
import { AllRequestComponent } from './pages/admin/funding/all-request/all-request.component'; 
import { OnboardingComponent } from './pages/onboarding/pages/onboarding/onboarding/onboarding.component';
import { RechargeComponent } from './pages/api-partner/recharge/recharge.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { RefundAndCancellationPolicyComponent } from './pages/refund-and-cancellation-policy/refund-and-cancellation-policy.component';
import { GatewayPgComponent } from './pages/api-partner/pg/gateway-pg/gateway-pg.component';
import { GatewayPgResponseComponent } from './pages/api-partner/pg/gateway-pg-response/gateway-pg-response.component';
import { PgRecepitComponent } from './pages/api-partner/pg/pg-recepit/pg-recepit.component';
import { BillPaymentComponent } from './pages/api-partner/bill-payment/bill-payment.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { AddFundRequestComponent } from './pages/superadmin/fund-request/add-fund-request/add-fund-request.component';
import { FundingComponent } from './pages/api-partner/funding/funding.component';
import { OnlineTransferComponent } from './pages/api-partner/funding/online-transfer/online-transfer.component';
import { ExceptioalRequestComponent } from './pages/api-partner/funding/exceptioal-request/exceptioal-request.component';
import { AllFundRequestComponent } from './pages/api-partner/funding/all-fund-request/all-fund-request.component'; 
import { WallettransferComponent } from './pages/api-partner/wallettransfer/wallettransfer.component';
import { SettlementComponent } from './pages/api-partner/settlement/settlement.component';
import { SettlePaymentComponent } from './pages/api-partner/settlement/settle-payment/settle-payment.component';
import { AddBankComponent } from './pages/api-partner/settlement/add-bank/add-bank.component';
import { CommissionComponent } from './pages/superadmin/commission/commission.component'; 
import { CreateCredentialsComponent } from './pages/superadmin/credentials/create-credentials/create-credentials.component';
import { UpdateipComponent } from './pages/superadmin/credentials/updateip/updateip.component';
import { GetCredentialComponent } from './pages/api-partner/credentials/get-credential/get-credential.component'; 
import { CommRechargeComponent } from './pages/superadmin/commission/comm-recharge/comm-recharge.component';
import { PaymentGatewayComponent } from './pages/superadmin/commission/payment-gateway/payment-gateway.component';
import { OtherCommComponent } from './pages/superadmin/commission/other-comm/other-comm.component';
import { PaytmcomComponent } from './pages/superadmin/commission/other-comm/paytmcom/paytmcom.component';
import { MobikcommComponent } from './pages/superadmin/commission/other-comm/mobikcomm/mobikcomm.component';
import { BillpaycommComponent } from './pages/superadmin/commission/other-comm/billpaycomm/billpaycomm.component';
import { AepsComponent } from './pages/superadmin/commission/aeps/aeps.component';
import { DmtComponent } from './pages/superadmin/commission/dmt/dmt.component';
import { CommSettlementComponent } from './pages/superadmin/commission/other-comm/comm-settlement/comm-settlement.component';
import { CommAadhaarComponent } from './pages/superadmin/commission/other-comm/comm-aadhaar/comm-aadhaar.component';
import { CommCashdepositComponent } from './pages/superadmin/commission/other-comm/comm-cashdeposit/comm-cashdeposit.component';
import { CommMiniStatementComponent } from './pages/superadmin/commission/other-comm/comm-mini-statement/comm-mini-statement.component';
import { UpdateVirtualAccountComponent } from './pages/superadmin/credentials/update-virtual-account/update-virtual-account.component';
import { AepsMiniStatementComponent } from './pages/superadmin/statement/aeps-mini-statement/aeps-mini-statement.component'; 
import { DmtStatementComponent } from './pages/superadmin/statement/dmt-statement/dmt-statement.component';
import { AepsSettlementComponent } from './pages/superadmin/statement/aeps-settlement/aeps-settlement.component';
import { PgTransactionComponent } from './pages/superadmin/statement/pg-transaction/pg-transaction.component';
import { RechargeStatementComponent } from './pages/superadmin/statement/recharge-statement/recharge-statement.component';
import { BillPaymentStatementComponent } from './pages/superadmin/statement/bill-payment-statement/bill-payment-statement.component';
import { MerchantOnboardStatementComponent } from './pages/superadmin/statement/merchant-onboard-statement/merchant-onboard-statement.component';
import { AadhaarPayComponent } from './pages/superadmin/statement/aadhaar-pay/aadhaar-pay.component';
import { AepsCashWithdrawalComponent } from './pages/superadmin/statement/aeps-cash-withdrawal/aeps-cash-withdrawal.component';
import { WalletStatementComponent } from './pages/superadmin/statement/wallet-statement/wallet-statement.component';
import { CashDepositStatementComponent } from './pages/superadmin/statement/cash-deposit-statement/cash-deposit-statement.component';
import { CreditLedgerComponent } from './pages/superadmin/statement/credit-ledger/credit-ledger.component';
import { UserPermissionComponent } from './pages/superadmin/permission/user-permission/user-permission.component';
import { StaffPermissionComponent } from './pages/superadmin/permission/staff-permission/staff-permission.component';
import { ControlsComponent } from './pages/superadmin/control/controls/controls.component';
import { UserComponent } from './pages/admin/funding/user/user.component';
import { UserListComponent } from './pages/admin/funding/user/user-list/user-list.component';
import { GetPrefrenceComponent } from './pages/api-partner/credentials/get-prefrence/get-prefrence.component';
import { DebitLedgerComponent } from './pages/superadmin/statement/debit-ledger/debit-ledger.component';
import { AuthComponent } from './auth/auth.component';
import { CredentialInitComponent } from './pages/superadmin/credentials/credential-init/credential-init.component';
import { QueryComponent } from './pages/superadmin/statement/dmt-statement/query/query.component';
import { ProcessComponent } from './pages/superadmin/statement/dmt-statement/process/process.component';
import { ParkedComponent } from './pages/superadmin/statement/dmt-statement/parked/parked.component';
import { TransferToBankComponent } from './pages/superadmin/statement/dmt-statement/transfer-to-bank/transfer-to-bank.component';
import { HoldComponent } from './pages/superadmin/statement/dmt-statement/hold/hold.component';
import { AepsBalanceEnqryComponent } from './pages/superadmin/statement/aeps-balance-enqry/aeps-balance-enqry.component';
 

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    // component: MyProfileComponent,
    redirectTo:'/myprofile',
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'login',
        pathMatch: 'full',
        component: LoginComponent
      },
      {
        path: 'forgot-password',
        pathMatch: 'full',
        component: ForgotPasswordComponent
      },
      {
        path: 'change-password',
        pathMatch: 'full',
        component: ChangePasswordComponent
      },
    ]
  }, 
  {
    path: 'dashboard',
    redirectTo:'/myprofile',
    // component: MyProfileComponent,
    canActivate: [AuthGuard]
  }, 
  {
    path: 'add-staff',
    component: StaffComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-staff',
    component: ListStaffComponent,
    canActivate: [AuthGuard], 
    data: {
      restricted:[5]
    }
  },
  {
    path: 'add-account',
    component: AccountsComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-account',
    component: ListAccountsComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'add-asm',
    component: AddAsmComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-asm',
    component: ListAsmComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-company-bank',
    component: ListCompanyBankComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'add-company-bank',
    component: AddCompanyBankComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'bank-transfer',
    component: TransferComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'bank-statement',
    component: StatementComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'create-apipartner',
    component: ApiPartnerComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-apipartner',
    component: ViewApiPartnerComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'add-operator-bill',
    component: AddBillComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-operator-bill',
    component: ViewOperatorBillComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'add-operator-recharge',
    component: AddRechargeComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'list-operator-recharge',
    component: ViewRechargeComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'user-permission',
    component: UserPermissionComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'create-credentials',
    component: CreateCredentialsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'updateip',
    component: UpdateipComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'getcredential',
    component: GetCredentialComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'gencredential',
    component: CredentialInitComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'getpreference',
    component: GetPrefrenceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'staff-permission',
    component: StaffPermissionComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  // Admin Route Start Here
  {
    path: 'approve-funding',
    component: ApproverComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
    
  },
  {
    path: 'authorize-funding',
    component: AuthorizeComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'allRequest-funding',
    component: AllRequestComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'update-virtual-account',
    component: UpdateVirtualAccountComponent,
    canActivate: [AuthGuard],
    data: {
      restricted:[5]
    }
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'refund-cancellation-policy',
    component: RefundAndCancellationPolicyComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'gateway-pg',
    component: GatewayPgComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'gateway-pg-response',
    component: GatewayPgResponseComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pg-recepit',
    component: PgRecepitComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bill-payment',
    component: BillPaymentComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'myprofile',
    component: MyProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    canActivate: [AuthGuard]
  },
  //--Statements 
  
  {
    path: 'aepsministatement',
    component: AepsMiniStatementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dmt-Statement',
    component: DmtStatementComponent,
    canActivate: [AuthGuard]
  },  
  {
    path: 'Aeps-settlement',
    component: AepsSettlementComponent,
    canActivate: [AuthGuard]
  },  
  {
    path: 'pg-transaction',
    component: PgTransactionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'recharge-Statement',
    component: RechargeStatementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Bill-Payment-Statement',
    component: BillPaymentStatementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'merchant-onboard-Statement',
    component: MerchantOnboardStatementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Aadhaar-Pay',
    component: AadhaarPayComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Aeps-cash-withdrawal',
    component: AepsCashWithdrawalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Aeps-balance-enquiry',
    component: AepsBalanceEnqryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Wallet-Upload',
    component: WalletStatementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Cash-Deposit-Statement',
    component: CashDepositStatementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'credit-ledger',
    component: CreditLedgerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'debit-ledger',  
    component: DebitLedgerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dmt-Statement/query/:id',  
    component: QueryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dmt-Statement/process/:id',  
    component: ProcessComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dmt-Statement/parked',  
    component: ParkedComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dmt-Statement/transfer-to-bank',  
    component: TransferToBankComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dmt-Statement/hold',  
    component: HoldComponent,
    canActivate: [AuthGuard]
  },

  // Onboarding Api
  {
    path: 'recharge', 
    component: RechargeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'onboarding',
    component: OnboardingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Add-Fund',
    component: AddFundRequestComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'wallettransfer',
    component: WallettransferComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'settlement',
    component: SettlementComponent, 
    children: [
      {
        path: '',
        component: SettlePaymentComponent
      },
      {
        path: 'addbank',
        component: AddBankComponent
      }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'Controls',
    component: ControlsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'funding',
    component: FundingComponent,
    children: [
      {
        path: '',
        component: OnlineTransferComponent
      },
      {
        path: 'excepReq',
        component: ExceptioalRequestComponent
      },
      {
        path: 'fundrequest/get',
        component: AllFundRequestComponent
      },
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'commission',
    component: CommissionComponent,
    data: {
      restricted:[5]
    },
    
    children: [
      {
        path: 'aeps',
        component: AepsComponent
      },
      {
        path: 'dmt',
        component: DmtComponent
      },
      {
        path: 'recharge',
        component: CommRechargeComponent
      },
      {
        path: 'paymentgateway',
        component: PaymentGatewayComponent
      },
      {
        path: 'othercomm',
        component: OtherCommComponent,
        children: [
          {
            path: '',
            component: PaytmcomComponent
          },
          {
            path: 'mobikcomm',
            component: MobikcommComponent
          },
          {
            path: 'billpaycomm',
            component: BillpaycommComponent
          },
          {
            path: 'settlement',
            component: CommSettlementComponent
          },
          {
            path: 'aadhaar',
            component: CommAadhaarComponent
          },
          {
            path: 'cashdeposit',
            component: CommCashdepositComponent
          },
          {
            path: 'ministatement',
            component: CommMiniStatementComponent
          },
        ]
      },
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: 'userlist',
        component: UserListComponent
      }
        ],
        canActivate: [AuthGuard] 
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
