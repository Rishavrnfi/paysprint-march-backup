import { Location } from '@angular/common';
import { Component, Renderer2 } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { ApiService } from './service/api.service';
import { LoaderService } from './_helpers/common/loader.service';
import * as $ from 'jquery';
declare function mySideBarInitFn(): any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  checkLogin: any = false;
  name = "";
  paramnew: any = '';
  app: any;
  showLoadeer: boolean = false;
  constructor(private api: ApiService, private router: Router, private loader: LoaderService, private renderer: Renderer2) {

    let count = 0;
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] === '/login' ||
          event['url'] === '/forgot-password' ||
          event['url'] === '/change-password' ||
          event['url'] === '/gateway-pg-response') {
          this.checkLogin = false;
          //document.body.classList.remove("main");
          document.body.classList.add("login");
          count = 0;
        } else {
          this.checkLogin = true;
          ////console.log(this.checkLogin);
          if (count === 0) {
            setTimeout(() => {
              mySideBarInitFn();

            }, 1);
            count++;
          }
          document.body.classList.remove("login");
          //document.body.classList.add("main");
        }
      }
    });

  }
  ngOnInit() {
    this.loader.loaderEvent.subscribe((data: any) => {
      this.showLoadeer = data;
    });

  }

}

