import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { CommonModule, DatePipe, HashLocationStrategy } from '@angular/common';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { OtpVerificationDirective } from './directives/otp-verification.directive';
import { StaffComponent } from './pages/superadmin/staff/add-staff/add.staff.component';
import { AccountsComponent } from './pages/superadmin/accounts/add-accounts/add.accounts.component';
import { ListAccountsComponent } from './pages/superadmin/accounts/list-accounts/list-accounts.component';
import { ListStaffComponent } from './pages/superadmin/staff/list-staff/list-staff.component';
import { AddAsmComponent } from './pages/superadmin/asm/add-asm/add-asm.component';
import { ListAsmComponent } from './pages/superadmin/asm/list-asm/list-asm.component';
import { AddCompanyBankComponent } from './pages/superadmin/company-bank/add-company-bank/add-company-bank.component';
import { ListCompanyBankComponent } from './pages/superadmin/company-bank/list-company-bank/list-company-bank.component';
import { AddBillComponent } from './pages/superadmin/operator/bill/add-bill/add-bill.component';
import { ViewOperatorBillComponent } from './pages/superadmin/operator/bill/view-operator-bill/view-operator-bill.component';
import { AddRechargeComponent } from './pages/superadmin/operator/recharge/add-recharge/add-recharge.component';
import { ViewRechargeComponent } from './pages/superadmin/operator/recharge/view-recharge/view-recharge.component';
import { AdminHeaderComponent } from './header/admin-header/admin-header.component';
import { RetailerHeaderComponent } from './header/retailer-header/retailer-header.component';
import { HttpRequestInterceptor } from './http-request.interceptor';
import { AccountHeaderComponent } from './header/account-header/account-header.component';
import { LoaderComponent } from './auth/loader/loader.component';
import { ApproverComponent } from './pages/admin/funding/approver/approver.component';
import { SuperAdminComponent } from './header/super-admin/super-admin.component';
import { StatementComponent } from './pages/superadmin/company-bank/statement/statement.component';
import { ApiPartnerComponent } from './pages/superadmin/api-partner/add-api-partner/api-partner.component';
import { TransferComponent } from './pages/superadmin/company-bank/transfer/transfer.component';
import { ViewApiPartnerComponent } from './pages/superadmin/api-partner/view-api-partner/view-api-partner.component';
import { UserPermissionComponent } from './pages/superadmin/permission/user-permission/user-permission.component';
import { ReplacePipe } from './_helpers/common/custom-pipes/replace.pipe';
import { ShowNullResultPipe } from './_helpers/common/custom-pipes/show-null-result.pipe';
import { AllRequestComponent } from './pages/admin/funding/all-request/all-request.component';
import { AuthorizeComponent } from './pages/admin/funding/authorize/authorize.component';
import { SingleEntryViewerComponent } from './pages/admin/funding/single-entry-viewer/single-entry-viewer.component'; 
import { StaffPermissionComponent } from './pages/superadmin/permission/staff-permission/staff-permission.component';
import { OnboardingComponent } from './pages/onboarding/pages/onboarding/onboarding/onboarding.component';
import { RechargeComponent } from './pages/api-partner/recharge/recharge.component';
import { CustomLoaderComponent } from './auth/custom-loader/custom-loader.component';  
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { RefundAndCancellationPolicyComponent } from './pages/refund-and-cancellation-policy/refund-and-cancellation-policy.component';
import { GatewayPgComponent } from './pages/api-partner/pg/gateway-pg/gateway-pg.component';
import { GatewayPgResponseComponent } from './pages/api-partner/pg/gateway-pg-response/gateway-pg-response.component';
import { PgRecepitComponent } from './pages/api-partner/pg/pg-recepit/pg-recepit.component';
import { BillPaymentComponent } from './pages/api-partner/bill-payment/bill-payment.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { NgSelect2Module } from 'ng-select2';
import { AddFundRequestComponent } from './pages/superadmin/fund-request/add-fund-request/add-fund-request.component'; 
import { FundingComponent } from './pages/api-partner/funding/funding.component';
import { OnlineTransferComponent } from './pages/api-partner/funding/online-transfer/online-transfer.component';
import { ExceptioalRequestComponent } from './pages/api-partner/funding/exceptioal-request/exceptioal-request.component';
import { AllFundRequestComponent } from './pages/api-partner/funding/all-fund-request/all-fund-request.component';
import { GetTypeOfPipe } from './_helpers/common/custom-pipes/get-type-of.pipe';
import { ControlsComponent } from './pages/superadmin/control/controls/controls.component';
import { WallettransferComponent } from './pages/api-partner/wallettransfer/wallettransfer.component';
import { SettlementComponent } from './pages/api-partner/settlement/settlement.component';
import { AddBankComponent } from './pages/api-partner/settlement/add-bank/add-bank.component';
import { SettlePaymentComponent } from './pages/api-partner/settlement/settle-payment/settle-payment.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'; 
import { CommissionComponent } from './pages/superadmin/commission/commission.component';
import { CreateCredentialsComponent } from './pages/superadmin/credentials/create-credentials/create-credentials.component';
import { UpdateipComponent } from './pages/superadmin/credentials/updateip/updateip.component';
import { GetCredentialComponent } from './pages/api-partner/credentials/get-credential/get-credential.component'; 
import { OtherCommComponent } from './pages/superadmin/commission/other-comm/other-comm.component';
import { PaymentGatewayComponent } from './pages/superadmin/commission/payment-gateway/payment-gateway.component';
import { PaytmcomComponent } from './pages/superadmin/commission/other-comm/paytmcom/paytmcom.component';
import { MobikcommComponent } from './pages/superadmin/commission/other-comm/mobikcomm/mobikcomm.component';
import { BillpaycommComponent } from './pages/superadmin/commission/other-comm/billpaycomm/billpaycomm.component';
import { CommRechargeComponent } from './pages/superadmin/commission/comm-recharge/comm-recharge.component';
import { DmtComponent } from './pages/superadmin/commission/dmt/dmt.component';
import { AepsComponent } from './pages/superadmin/commission/aeps/aeps.component';
import { CommSettlementComponent } from './pages/superadmin/commission/other-comm/comm-settlement/comm-settlement.component';
import { CommAadhaarComponent } from './pages/superadmin/commission/other-comm/comm-aadhaar/comm-aadhaar.component';
import { CommCashdepositComponent } from './pages/superadmin/commission/other-comm/comm-cashdeposit/comm-cashdeposit.component';
import { CommMiniStatementComponent } from './pages/superadmin/commission/other-comm/comm-mini-statement/comm-mini-statement.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { UpdateVirtualAccountComponent } from './pages/superadmin/credentials/update-virtual-account/update-virtual-account.component';
import { AepsMiniStatementComponent } from './pages/superadmin/statement/aeps-mini-statement/aeps-mini-statement.component'; 
import { TblRowRadioSelectorDirective } from './_helpers/common/custome-directive/tbl-row-radio-selector.directive';
import { DmtStatementComponent } from './pages/superadmin/statement/dmt-statement/dmt-statement.component';
import { AepsSettlementComponent } from './pages/superadmin/statement/aeps-settlement/aeps-settlement.component';
import { PgTransactionComponent } from './pages/superadmin/statement/pg-transaction/pg-transaction.component';
import { RechargeStatementComponent } from './pages/superadmin/statement/recharge-statement/recharge-statement.component';
import { BillPaymentStatementComponent } from './pages/superadmin/statement/bill-payment-statement/bill-payment-statement.component';
import { MerchantOnboardStatementComponent } from './pages/superadmin/statement/merchant-onboard-statement/merchant-onboard-statement.component';
import { AadhaarPayComponent } from './pages/superadmin/statement/aadhaar-pay/aadhaar-pay.component';
import { AepsCashWithdrawalComponent } from './pages/superadmin/statement/aeps-cash-withdrawal/aeps-cash-withdrawal.component';
import { WalletStatementComponent } from './pages/superadmin/statement/wallet-statement/wallet-statement.component';
import { CashDepositStatementComponent } from './pages/superadmin/statement/cash-deposit-statement/cash-deposit-statement.component';
import { CreditLedgerComponent } from './pages/superadmin/statement/credit-ledger/credit-ledger.component';
import { RadioControlDirective } from './_helpers/common/custome-directive/radio-control.directive';
import { SidebarComponent } from './sidebar/sidebar.component'; 
import { CommonSidebarComponent } from './header/common-sidebar/common-sidebar.component';
import { UserListComponent } from './pages/admin/funding/user/user-list/user-list.component'; 
import { UserComponent } from './pages/admin/funding/user/user.component';
import { GetPrefrenceComponent } from './pages/api-partner/credentials/get-prefrence/get-prefrence.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { DebitLedgerComponent } from './pages/superadmin/statement/debit-ledger/debit-ledger.component';
import { AuthComponent } from './auth/auth.component';
import { CredentialInitComponent } from './pages/superadmin/credentials/credential-init/credential-init.component';
import { QueryComponent } from './pages/superadmin/statement/dmt-statement/query/query.component';
import { ProcessComponent } from './pages/superadmin/statement/dmt-statement/process/process.component';
import { ParkedComponent } from './pages/superadmin/statement/dmt-statement/parked/parked.component';
import { TransferToBankComponent } from './pages/superadmin/statement/dmt-statement/transfer-to-bank/transfer-to-bank.component';
import { HoldComponent } from './pages/superadmin/statement/dmt-statement/hold/hold.component';
import { AepsBalanceEnqryComponent } from './pages/superadmin/statement/aeps-balance-enqry/aeps-balance-enqry.component';
 
 
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    ChangePasswordComponent,
    OtpVerificationDirective,
    StaffComponent,
    AccountsComponent,
    ListAccountsComponent,
    ListStaffComponent,
    AddAsmComponent,
    ListAsmComponent,
    AddCompanyBankComponent,
    ListCompanyBankComponent,
    AddBillComponent,
    ViewOperatorBillComponent,
    AddRechargeComponent,
    ViewRechargeComponent,
    AccountsComponent,
    AdminHeaderComponent,
    RetailerHeaderComponent,
    AccountHeaderComponent,
    LoaderComponent,
    ApproverComponent,
    StatementComponent,
    TransferComponent,
    ApiPartnerComponent,
    SuperAdminComponent,
    ViewApiPartnerComponent,
    UserPermissionComponent,
    ReplacePipe, ShowNullResultPipe,
    AllRequestComponent,
    AuthorizeComponent,
    SingleEntryViewerComponent,
    ApproverComponent,
    StaffPermissionComponent,
    OnboardingComponent,
    RechargeComponent,
    CustomLoaderComponent,
    AboutUsComponent,
    PrivacyPolicyComponent,
    RefundAndCancellationPolicyComponent,
    GatewayPgComponent,
    GatewayPgResponseComponent,
    PgRecepitComponent,
    BillPaymentComponent,
    MyProfileComponent,
    ContactUsComponent,
    AddFundRequestComponent,
    FundingComponent,
    OnlineTransferComponent,
    ExceptioalRequestComponent,
    AllFundRequestComponent,
    GetTypeOfPipe,
    ControlsComponent,
    WallettransferComponent,
    SettlementComponent,
    AddBankComponent,
    SettlePaymentComponent, 
    CommissionComponent,
    DmtComponent,
    AepsComponent,
    CreateCredentialsComponent,
    UpdateipComponent,
    GetCredentialComponent,
    GetPrefrenceComponent,
    OtherCommComponent,
    PaymentGatewayComponent,
    PaytmcomComponent,
    MobikcommComponent,
    BillpaycommComponent,
    CommRechargeComponent,
    CommSettlementComponent,
    CommAadhaarComponent,
    CommCashdepositComponent,
    CommMiniStatementComponent,
    UpdateVirtualAccountComponent,
    AepsMiniStatementComponent , 
    TblRowRadioSelectorDirective,
     DmtStatementComponent,
     AepsSettlementComponent,
     PgTransactionComponent,
     RechargeStatementComponent,
     BillPaymentStatementComponent,
     MerchantOnboardStatementComponent,
     AadhaarPayComponent, 
     AepsCashWithdrawalComponent,
     WalletStatementComponent,
     CashDepositStatementComponent,
     CreditLedgerComponent,
     RadioControlDirective,
     SidebarComponent,
     CommonSidebarComponent,
     UserListComponent, 
     UserComponent, DebitLedgerComponent, AuthComponent, CredentialInitComponent, QueryComponent, ProcessComponent, ParkedComponent, TransferToBankComponent, HoldComponent, AepsBalanceEnqryComponent
  ],
  imports: [
    BrowserModule, 
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    CommonModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgSelect2Module,
    DragDropModule,  
    AutocompleteLibModule,
    BsDatepickerModule.forRoot(), 
  ],
  providers: [
    DatePipe, 
    [
      { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor,HashLocationStrategy, multi: true }
    ]
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
