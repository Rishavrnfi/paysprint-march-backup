import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './service/api.service';
import { LoaderService } from './service/loader.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CustomeModalService } from './_helpers/common/custome-modal/custome-modal.service';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  AuthOnboard : any ;
  private requests: HttpRequest<any>[] = [];
  constructor(private auth: ApiService, private loader: LoaderService, private route: Router, private modal : CustomeModalService ) { }

  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }
    this.loader.isLoading.next(this.requests.length > 0);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.requests.push(req);
    let headers = new HttpHeaders({
      'Authkey': 'MWQyMmUzNWY4YjhlNjY2NWJjM2EzZjY0NjNhZWM0ZTk=',
      'Authtoken': this.auth.isLoggedIn(), 
      //'Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJQQVlTUFJJTlQiLCJ0aW1lc3RhbXAiOjE2MTAxOTU5MzAsInBhcnRuZXJJZCI6IlBTMDAxIiwicHJvZHVjdCI6IldBTExFVCIsInJlcWlkIjoxNjEwMTk1OTMwfQ.3slqFu5g7dCoKOxrHKc6HUB55oIiu26tcKEArHnmmuo'
    })
    this.loader.isLoading.next(true);
    const cloneReq = req.clone({ headers });
    return new Observable(observer => {
      const subscription = next.handle(cloneReq)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              if (event.body.statuscode !== 404) {
                this.removeRequest(req);
                observer.next(event);
              }
              else {
                Swal.fire({
                  icon: 'error',
                  title: event.body.message
                })  
                this.modal.close();
                this.auth.onLogout(); 
                this.route.navigate(['login']);
              }
            }
          },
          err => {
            //alert('error' + err);
            this.removeRequest(req);
            observer.error(err);
          },
          () => {
            this.removeRequest(req);
            observer.complete();
          });
      // remove request from queue when cancelled
      return () => {
        this.removeRequest(req);
        subscription.unsubscribe();
      };
    });
    //const cloneReq = req.clone({headers});
    //return next.handle(cloneReq);
  }
}
