import { Injectable } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
@Injectable({
  providedIn: 'root'
})
export class LoadingServiceService {

  constructor(private spinner: NgxSpinnerService) { 
    this.spinner.show(); 
     setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  
  showSpinner(data :any) {
    if(data == true){
      this.spinner.show();
    }else{
      this.spinner.hide();
    }
    setTimeout(() => {
      this.spinner.hide();
    }, 3000);
  }
}
