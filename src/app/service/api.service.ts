import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { EncodeDecode } from '../_helpers/encode-decode'
import { Router } from '@angular/router'; 
@Injectable({
  providedIn: 'root'
})

export class ApiService {
  heads:any;
  private sesion_data: any = '';
  private onboarding_token: any = '';
  constructor(private http: HttpClient, private router: Router) {
  }
  reloadTo(uri: string) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate([uri]));
  }
  isLoginType(){
    if (localStorage.getItem('LoginDetails') !== null) {
      let encode: any = EncodeDecode('n', localStorage.getItem('LoginDetails'));
      this.sesion_data = JSON.parse(encode);
      return this.sesion_data.usertype;
    } else {
      return false;
    }
  }

  Getsessiondata() {
    if (localStorage.getItem('LoginDetails') !== null) {
      let encode: any = EncodeDecode('n', localStorage.getItem('LoginDetails'));
      this.sesion_data = JSON.parse(encode);
      const jsondata = this.sesion_data;
      ////console.log(jsondata)
      return jsondata;
    } else {
      return false;
    }
  }
  isLoggedIn() {
    if (localStorage.getItem('LoginDetails') !== null) {
      let encode: any = EncodeDecode('n', localStorage.getItem('LoginDetails'));
      this.sesion_data = JSON.parse(encode);
      const jsondata = this.sesion_data.loginsession;
      ////console.log(jsondata)
      return jsondata;
    } else {
      return false;
    }
  }
  OnboardingToken() {
    if (localStorage.getItem('onboarding') !== null) {
      let data = localStorage.getItem('onboarding');
      this.onboarding_token = data;
      return this.onboarding_token;
    } else {
      return false;
    }
  }
  postState(payload: any): Observable<any> {
    return this.http.post<any>(environment.apiExternal, payload)
      .pipe(
        retry(0),
        catchError(this.errorHandl)
      )
  }
  postdata(payload: any, path: string): Observable<any> {
    return this.http.post<any>(environment.apiBaseUrl + path, payload)
      .pipe(
        retry(0),
        catchError(this.errorHandl)
      )
  }
  errorHandl(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //console.log(errorMessage);
    return throwError(errorMessage);
  }
  getLocationService(): Promise<any> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude })
      })
    })
  }

  onLogout() {
    localStorage.removeItem('LoginDetails');
    localStorage.clear();
    this.router.navigate(['/login']); 
  }

  downloadFile(data:any,format:any, filename = "data") {
    let csvData = this.ConvertToCSV(data,format);

    console.log(csvData);

    let blob = new Blob(["\ufeff" + csvData], {
      type: "text/csv;charset=utf-8;"
    });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser =
      navigator.userAgent.indexOf("Safari") != -1 &&
      navigator.userAgent.indexOf("Chrome") == -1;
    if (isSafariBrowser) {
      //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }

  ConvertToCSV(objArray:any, headerList:any) {
    let array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
    let str = "";
    let row = "S.No,";

    for (let index in headerList) {
      row += headerList[index] + ",";
    }
    row = row.slice(0, -1);
    str += row + "\r\n";
    for (let i = 0; i < array.length; i++) {
      let line = i + 1 + "";
      for (let index in headerList) {
        let head = headerList[index];

        // if(is_numric(array[i][head])){
        //   heads = "'"+array[i][head];
        // }else{
        //   heads = array[i][head]
        // }
        if(!isNaN(array[i][head]) && array[i][head] !== '' && array[i][head] != null && array[i][head].length > 9){
          this.heads = "'"+array[i][head];
        }
      else {
        this.heads =array[i][head];
      }   
      //console.log( this.heads);
      
      
        line += "," + this.heads;
      }
      str += line + "\r\n";
    }
    return str;
  }
}
