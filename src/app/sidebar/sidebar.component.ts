import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../service/api.service';
import { config } from '../service/config';
import { LoaderService } from '../_helpers/common/loader.service';
import { EncodeDecode } from '../_helpers/encode-decode';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
 
  list: any;
  public details: any;
  viewData: any;
  userData: any;  
  constructor( private auth: ApiService, private route: Router,private loader:LoaderService) {
    this.getverifyDetails()
    const encode:any = EncodeDecode('n',localStorage.getItem('LoginDetails'));
    const _permissionlist:any = JSON.parse(encode);
    this.list = _permissionlist['permission'];
    ////console.log(this.list)
    this.details = {
      username : _permissionlist.username,
      firmname: _permissionlist.firmname,
      balance: _permissionlist.balance,
      pannumber: _permissionlist.pannumber,
      usertype: _permissionlist.usertype,
      allowfundrequest: _permissionlist.allowfundrequest,
      is_superadmin:  _permissionlist['permission'].is_superadmin,
      is_admin: _permissionlist['permission'].is_admin,

    }
    ////console.log(this.details)

   //this.getwallet(); 
   
  }

  ngOnInit(): void { 
    // this.loader.loaderEvent.subscribe((data: any) => {
    //   ////console.log('hello data enter');
    //   if (!data) { 
    //     this.getwallet();  
    //   }
    // }); 
  }

  logout(){
    this.auth.onLogout();
    this.route.navigate(['login']);
  }

  getverifyDetails() {

    if (typeof (localStorage.getItem('LoginDetails')) !== 'undefined' && localStorage.getItem('LoginDetails') !== '') {
      let decode: any = EncodeDecode('n', localStorage.getItem('LoginDetails'));
      let data: any = JSON.parse(decode);
      this.viewData = data;
    } else {
      this.route.navigate(['login']);
    }
  }
  getwallet(){
    const formdata = new FormData();
    formdata.append('token', config.tokenauth);
    this.auth.postdata(formdata, config.getProfileDtl)
      .subscribe((res: any) => {
        if (res.statuscode == 200) {  
          this.userData = res.data; 
          ////console.log(res.data)
        }
      });
  }

  

}
