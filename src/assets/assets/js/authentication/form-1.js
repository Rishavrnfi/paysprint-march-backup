$(document).ready(function(){
	$("body").on("change",".d-none",function(){
		if($(this).val()==0){
			$(".d-none").val(1);
			$(".d-inline-block").html("Hide Password");
			$("#showpassword").attr("type","text");
		}else{
			$(".d-none").val(0);
			$(".d-inline-block").html("Show Password");
			$("#showpassword").attr("type","password");
		}
	});	
});
